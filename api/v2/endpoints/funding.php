<?php

$response_data = array(
    'api_status' => 400
);

$required_fields =  array(
                        'delete',
                        'funding',
                        'user_funding',
                        'pay',
                        // Sameer functions 
                        'create',
                        'edit',
                        'save_fund_as_draft',
                        'delete_draft_fund', 
                        'insert_challenge',
                        'insert_accepted_challenge',
                        'save_challenge_as_draft',
                        'delete_challenge',
                        'delete_draft_challenge',// till now done
                        'load_user_challenge_fund',
                        'load_challenge_fund',
                        'get_funding_by_id',
                        'get_all_challenges'
                    );

$limit = (!empty($_POST['limit']) && is_numeric($_POST['limit']) && $_POST['limit'] > 0 && $_POST['limit'] <= 50 ? Wo_Secure($_POST['limit']) : 20);
$offset = (!empty($_POST['offset']) && is_numeric($_POST['offset']) && $_POST['offset'] > 0 ? Wo_Secure($_POST['offset']) : 0);

if (!empty($_POST['type']) && in_array($_POST['type'], $required_fields)) {

    if ($_POST['type'] == 'create') {

        if (!empty($_POST['title']) && !empty($_POST['description']) && !empty($_FILES['image']) && !empty($_POST['amount']) && is_numeric($_POST['amount']) && $_POST['amount'] > 0) {
                $post_text = "";
				$beneficiary = "";
				$fund_id = "";
				$funding_admins = "";
				$mention_regex = '/@([A-Za-z0-9_]+)/i';
				
				if (empty($_POST['beneficiary'])) {
                    $error_code    = 4;
                    $error_message = 'Beneficiary is required';
                    $response_data['error'] = array('error_id' => $error_code,'error_message' => $error_message);
                    echo json_encode($response_data);
					exit();
				} else {
					$beneficiary = Wo_Secure($_POST['beneficiary']);
					preg_match_all($mention_regex, $beneficiary, $matches);
					foreach ($matches[1] as $match) {
						$match         = Wo_Secure($match);
						$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
						$match_search  = '@' . $match;
						$match_replace = '@[' . $match_user['user_id'] . ']';
						if (isset($match_user['user_id'])) {
							$beneficiary  = str_replace($match_search, $match_replace, $beneficiary);
							$mentions[] = $match_user['user_id'];
						}
					}
					if (sizeof($mentions) != 1) {
						// $data['status'] = 400;
						// $message = $error_icon . "beneficiary must be 1 person " . "<br>";
                        $error_code    = 11;
                        $error_message = 'beneficiary must be 1 person';
                        $response_data['error'] = array('error_id' => $error_code,'error_message' => $error_message);
                        echo json_encode($response_data);
                        exit();
					}
				}
				if(isset($_POST['funding_admins']) && !empty($_POST['funding_admins'])){	
					$funding_admins = Wo_Secure($_POST['funding_admins']);
					preg_match_all($mention_regex, $funding_admins, $matches);
					foreach ($matches[1] as $match) {
						$match         = Wo_Secure($match);
						$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
						$match_search  = '@' . $match;
						$match_replace = '@[' . $match_user['user_id'] . ']';
						if (isset($match_user['user_id'])) {
							$funding_admins  = str_replace($match_search, $match_replace, $funding_admins);
							$fundingAdmins[] = $match_user['user_id'];
						}
					}
				}

				if (isset($_POST['postTextCategory']) && !empty($_POST['postTextCategory'])) {
					$text = $_POST['postTextCategory'];

					$hashtag_regex = '/(#\[([0-9]+)\])/i';
					preg_match_all($hashtag_regex, $text, $matches);
					$match_i = 0;
					foreach ($matches[1] as $match) {
						$hashkey  = $matches[2][$match_i];
						if (!empty($hashkey)) {
							$db->where('id', $hashkey)->update(T_HASHTAGS, array('trend_use_num' => $db->dec(1)));
						}
						$match_i++;
					}
					$post_text = $post_text . " " . $text;
				}

				// Tagging code end;
				$media = array();
				if (!empty($_FILES['image'])) {
					$fileInfo      = array(
						'file' => $_FILES["image"]["tmp_name"],
						'name' => $_FILES['image']['name'],
						'size' => $_FILES["image"]["size"],
						'type' => $_FILES["image"]["type"],
						'types' => 'jpeg,jpg,png,bmp'
					);
					$media  = Wo_ShareFile($fileInfo);
				}

				$insert_array = array(
					'title' => Wo_Secure($_POST['title']),
					'description'   => Wo_Secure($_POST['description']),
					'amount'   => Wo_Secure($_POST['amount']),
					'time'   => time(),
					'image' => $media['filename'],
					'video' => "",
					'beneficiary' => $beneficiary,
					'funding_admins' => $funding_admins,
					'user_id'  => $wo['user']['id'],
					'hashed_id' => Wo_GenerateKey(15, 15)
				);

				$due_date = Wo_Secure($_POST['due_date']);
				if ($due_date) {
					$insert_array['due_date'] = $due_date;
				}


				if (!empty($media) && !empty($media['filename'])) {
					// $data['message'] = $insert_array;
					// echo json_encode($data);
					// exit();
					$fund_id = $db->insert(T_FUNDING, $insert_array);

					if (!empty($fund_id)) {
						//multiple options uploads
						// $postPhotos = "";
						// $postVideo = "";
						$mediaFilename = '';
						$post_photo    = '';
						$mediaName     = '';
						$video_thumb   = '';
						$album_name = '';
						$blur = 0;

						if (!empty($_POST['album_name'])) {
							$album_name = $_POST['album_name'];
						}
						if (!isset($_FILES['postPhotos']['name'])) {
							$album_name = '';
						}
						$multi = 0;

						if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) {
							if (count($_FILES['postPhotos']['name']) == 1) {
								if ($_FILES['postPhotos']['size'][0] > $wo['config']['maxUpload']) {
									$invalid_file = 1;
									$data['invalid_file'] = $invalid_file;
								} else if (Wo_IsFileAllowed($_FILES['postPhotos']['name'][0]) == false) {
									$invalid_file = 2;
									$data['invalid_file'] = $invalid_file;
								} else {

									$fileInfo = array(
										'file' => $_FILES["postPhotos"]["tmp_name"][0],
										'name' => $_FILES['postPhotos']['name'][0],
										'size' => $_FILES["postPhotos"]["size"][0],
										'type' => $_FILES["postPhotos"]["type"][0]
									);
									$media    = Wo_ShareFile($fileInfo);
									if (!empty($media)) {
										$image_file = Wo_GetMedia($media['filename']);
										$upload = true;
										if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
											$blur = 1;
										} elseif ($wo['config']['adult_images'] == 1  && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
											$invalid_file = 3;
											$upload = false;
											@unlink($media['filename']);
											Wo_DeleteFromToS3($media['filename']);
										}
										$mediaFilename = $media['filename'];
										$mediaName     = $media['name'];
									}
								}
							} else {
								$multi = 1;
								$data['multi'] = $multi;
							}
						}
						if (isset($_FILES['postPhotos']['name'])) {
							$allowed = array(
								'gif',
								'png',
								'jpg',
								'jpeg'
							);
							for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
								$new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
								if (!in_array(strtolower($new_string['extension']), $allowed)) {
									$errors[] = $error_icon . $wo['lang']['please_check_details'];
								}
							}
						}
						if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
							$headers = get_headers($_POST['postSticker'], 1);
							if (strpos($headers['Content-Type'], 'image/') !== false) {
								$post_data['postSticker'] = $_POST['postSticker'];
							} else {
								$invalid_file = 2;
							}
						} else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
							if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
								$post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
							}
						}

						if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
							if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postVideo"]["tmp_name"],
									'name' => $_FILES['postVideo']['name'],
									'size' => $_FILES["postVideo"]["size"],
									'type' => $_FILES["postVideo"]["type"],
									'types' => 'mp4,m4v,webm,flv,mov,mpeg'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilename = $media['filename'];
									$mediaName     = $media['name'];
									$img_types     = array(
										'image/png',
										'image/jpeg',
										'image/jpg',
										'image/gif'
									);
									if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
										$fileInfo = array(
											'file' => $_FILES["video_thumb"]["tmp_name"],
											'name' => $_FILES['video_thumb']['name'],
											'size' => $_FILES["video_thumb"]["size"],
											'type' => $_FILES["video_thumb"]["type"],
											'types' => 'jpeg,png,jpg,gif',
											'crop' => array(
												'width' => 525,
												'height' => 295
											)
										);
										$media    = Wo_ShareFile($fileInfo);
										if (!empty($media)) {
											$video_thumb = $media['filename'];
										}
									}
								}
							}
						}
						if (isset($_FILES['postMusic']['name']) && empty($mediaFilename)) {
							if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postMusic"]["tmp_name"],
									'name' => $_FILES['postMusic']['name'],
									'size' => $_FILES["postMusic"]["size"],
									'type' => $_FILES["postMusic"]["type"],
									'types' => 'mp3,wav'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilename = $media['filename'];
									$mediaName     = $media['name'];
								}
							}
						}
						$post_data = array(
							'user_id' => Wo_Secure($wo['user']['user_id']),
							'fund_id' => $fund_id,
							'time' => time(),
							'postRecord' => Wo_Secure($_POST['postRecord']),
							'multi_image' => Wo_Secure($multi),
							'multi_image_post' => 0,
							'postText' => Wo_Secure($post_text),
							'album_name' => Wo_Secure($album_name),
							'postFile' => Wo_Secure($mediaFilename, 0),
							'postFileName' => Wo_Secure($mediaName),
							'postFileThumb' => Wo_Secure($video_thumb),
							'blur' => $blur,
							'postPrivacy' => 0,
						);

						$id = Wo_RegisterPost($post_data);

						if (isset($mentions) && is_array($mentions)) {
							foreach ($mentions as $mention) {
								if (empty($wo['no_mention']) || (!empty($wo['no_mention']) && !in_array($mention, $wo['no_mention']))) {
									$notification_data_array = array(
										'recipient_id' => $mention,
										'type' => 'post_mention',
										'page_id' => 0,
										'post_id' => $id,
										'url' => 'index.php?link1=show_fund&id=' . $insert_array['hashed_id']
									);
									Wo_RegisterNotification($notification_data_array);
								}
							}
						}
						if (isset($_FILES['postPhotos']['name'])) {
							if (count($_FILES['postPhotos']['name']) > 0) {
								$data['imageUpload'] = 'photos > 0';

								for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
									$fileInfo = array(
										'file' => $_FILES["postPhotos"]["tmp_name"][$i],
										'name' => $_FILES['postPhotos']['name'][$i],
										'size' => $_FILES["postPhotos"]["size"][$i],
										'type' => $_FILES["postPhotos"]["type"][$i],
										'types' => 'jpg,png,jpeg,gif'
									);
									$file     = Wo_ShareFile($fileInfo, 1);
									$image_file = Wo_GetMedia($file['filename']);
									if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
										$blur = 1;
									} elseif ($wo['config']['adult_images'] == 1 && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
										$invalid_file = 3;
										$errors[] = $error_icon . $wo['lang']['adult_image_file'];
										Wo_DeletePost($id);
										@unlink($file['filename']);
										Wo_DeleteFromToS3($file['filename']);
									}
									if (!empty($file)) {
										$media_album = Wo_RegisterAlbumMedia($id, $file['filename']);
										$post_data['multi_image'] = 0;
										$post_data['multi_image_post'] = 1;
										$post_data['album_name'] = '';
										$post_data['postFile'] = $file['filename'];
										$post_data['postFileName'] = $file['name'];
										$post_data['active'] = 1;
										$new_id = Wo_RegisterPost($post_data);
										$media_album = Wo_RegisterAlbumMedia($new_id, $file['filename'], $id);
										$data['image-uploaded'] = 'uploaded' . $new_id . "media_album" . $media_album;
									}
									$data['i-' . $i] = 'uploaded';
								}
							}
						}
                        $response_data = [
                        'api_status' => 200,
                        'message' => $wo['lang']['funding_created']
                        ];
						// $data['status'] = 200;
						// $data['message'] = $wo['lang']['funding_created'];
						// $data['url'] = Wo_SeoLink('index.php?link1=my_funding');
					} else {
						$data['status'] = 400;
						$message = $error_icon . $wo['lang']['please_check_details'] . "Database Error";
						$data['message'] = $message;
                        $error_code    = 5;
                        $error_message = 'Database Error';
					}
				}
            else{
                $error_code    = 5;
                $error_message = 'file not supported';
            }
        }
        else{
            $error_code    = 6;
            $error_message = 'please check details';
        }
    }
    if ($_POST['type'] == 'save_fund_as_draft') {

		if ($wo['config']['who_upload'] == 'pro' && $wo['user']['is_pro'] == 0 && !Wo_IsAdmin() && !empty($_FILES['image'])) {
			$response_data['message'] =  $wo['lang']['free_plan_upload_pro'];
		} else {
			if (!empty($_POST['title'])  && !empty($_POST['description']) && !empty($_FILES['image']) && !empty($_POST['amount']) && is_numeric($_POST['amount']) && $_POST['amount'] > 0) {

				$post_text = "";
				$beneficiary = "";
				$fund_id = "";
				$funding_admins = "";
				$mention_regex = '/@([A-Za-z0-9_]+)/i';

				if (empty($_POST['beneficiary'])) {
                    $error_code    = 1;
                    $error_message = 'Beneficiary is required';
                    $response_data['error'] = array('error_id' => $error_code,'error_message' => $error_message);
                    echo json_encode($response_data);
					exit();
				} else {
					$beneficiary = Wo_Secure($_POST['beneficiary']);
					preg_match_all($mention_regex, $beneficiary, $matches);
					foreach ($matches[1] as $match) {
						$match         = Wo_Secure($match);
						$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
						$match_search  = '@' . $match;
						$match_replace = '@[' . $match_user['user_id'] . ']';
						if (isset($match_user['user_id'])) {
							$beneficiary  = str_replace($match_search, $match_replace, $beneficiary);
							$mentions[] = $match_user['user_id'];
						}
					}
					$data['beneficiary'] =sizeof($mentions);
					if (sizeof($mentions) != 1) {
                        $error_code = 11;
                        $error_message = "beneficiary must be 1 person";
                        $response_data['error'] = array('error_id' => $error_code,'error_message' => $error_message);
                        echo json_encode($response_data);
						exit();
					}
				}
				if(isset($_POST['funding_admins']) && !empty($_POST['funding_admins'])){	
					$funding_admins = Wo_Secure($_POST['funding_admins']);
					preg_match_all($mention_regex, $funding_admins, $matches);
					foreach ($matches[1] as $match) {
						$match         = Wo_Secure($match);
						$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
						$match_search  = '@' . $match;
						$match_replace = '@[' . $match_user['user_id'] . ']';
						if (isset($match_user['user_id'])) {
							$funding_admins  = str_replace($match_search, $match_replace, $funding_admins);
							$fundingAdmins[] = $match_user['user_id'];
						}
					}
				}

				if (isset($_POST['postTextCategory']) && !empty($_POST['postTextCategory'])) {
					$text = $_POST['postTextCategory'];

					$hashtag_regex = '/(#\[([0-9]+)\])/i';
					preg_match_all($hashtag_regex, $text, $matches);
					$match_i = 0;
					foreach ($matches[1] as $match) {
						$hashkey  = $matches[2][$match_i];
						if (!empty($hashkey)) {
							$db->where('id', $hashkey)->update(T_HASHTAGS, array('trend_use_num' => $db->dec(1)));
						}
						$match_i++;
					}
					$post_text = $post_text . " " . $text;
				}

				// Tagging code end;
				$media = array();
				if (!empty($_FILES['image'])) {
					$fileInfo      = array(
						'file' => $_FILES["image"]["tmp_name"],
						'name' => $_FILES['image']['name'],
						'size' => $_FILES["image"]["size"],
						'type' => $_FILES["image"]["type"],
						'types' => 'jpeg,jpg,png,bmp'
					);
					$media  = Wo_ShareFile($fileInfo);
				}

				$insert_array = array(
					'title' => Wo_Secure($_POST['title']),
					'description'   => Wo_Secure($_POST['description']),
					'amount'   => Wo_Secure($_POST['amount']),
					'time'   => time(),
					'image' => $media['filename'],
					'video' => "",
					'beneficiary' => $beneficiary,
					'funding_admins' => $funding_admins,
					'user_id'  => $wo['user']['id'],
					'hashed_id' => Wo_GenerateKey(15, 15)
				);

				$due_date = Wo_Secure($_POST['due_date']);
				if ($due_date) {
					$insert_array['due_date'] = $due_date;
				}


				if (!empty($media) && !empty($media['filename'])) {
				
					$fund_id = $db->insert(T_FUNDING_DRAFT, $insert_array);

					if (!empty($fund_id)) {
						//multiple options uploads
						// $postPhotos = "";
						// $postVideo = "";
						$mediaFilename = '';
						$post_photo    = '';
						$mediaName     = '';
						$video_thumb   = '';
						$album_name = '';
						$blur = 0;

						if (!empty($_POST['album_name'])) {
							$album_name = $_POST['album_name'];
						}
						if (!isset($_FILES['postPhotos']['name'])) {
							$album_name = '';
						}
						$multi = 0;

						if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) {
							if (count($_FILES['postPhotos']['name']) == 1) {
								if ($_FILES['postPhotos']['size'][0] > $wo['config']['maxUpload']) {
									$invalid_file = 1;
									$data['invalid_file'] = $invalid_file;
								} else if (Wo_IsFileAllowed($_FILES['postPhotos']['name'][0]) == false) {
									$invalid_file = 2;
									$data['invalid_file'] = $invalid_file;
								} else {

									$fileInfo = array(
										'file' => $_FILES["postPhotos"]["tmp_name"][0],
										'name' => $_FILES['postPhotos']['name'][0],
										'size' => $_FILES["postPhotos"]["size"][0],
										'type' => $_FILES["postPhotos"]["type"][0]
									);
									$media    = Wo_ShareFile($fileInfo);
									if (!empty($media)) {
										$image_file = Wo_GetMedia($media['filename']);
										$upload = true;
										if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
											$blur = 1;
										} elseif ($wo['config']['adult_images'] == 1  && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
											$invalid_file = 3;
											$upload = false;
											@unlink($media['filename']);
											Wo_DeleteFromToS3($media['filename']);
										}
										$mediaFilename = $media['filename'];
										$mediaName     = $media['name'];
									}
								}
							} else {
								$multi = 1;
								$data['multi'] = $multi;
							}
						}
						if (isset($_FILES['postPhotos']['name'])) {
							$allowed = array(
								'gif',
								'png',
								'jpg',
								'jpeg'
							);
							for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
								$new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
								if (!in_array(strtolower($new_string['extension']), $allowed)) {
									$errors[] = $error_icon . $wo['lang']['please_check_details'];
								}
							}
						}
						if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
							$headers = get_headers($_POST['postSticker'], 1);
							if (strpos($headers['Content-Type'], 'image/') !== false) {
								$post_data['postSticker'] = $_POST['postSticker'];
							} else {
								$invalid_file = 2;
							}
						} else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
							if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
								$post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
							}
						}

						if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
							if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postVideo"]["tmp_name"],
									'name' => $_FILES['postVideo']['name'],
									'size' => $_FILES["postVideo"]["size"],
									'type' => $_FILES["postVideo"]["type"],
									'types' => 'mp4,m4v,webm,flv,mov,mpeg'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilename = $media['filename'];
									$mediaName     = $media['name'];
									$img_types     = array(
										'image/png',
										'image/jpeg',
										'image/jpg',
										'image/gif'
									);
									if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
										$fileInfo = array(
											'file' => $_FILES["video_thumb"]["tmp_name"],
											'name' => $_FILES['video_thumb']['name'],
											'size' => $_FILES["video_thumb"]["size"],
											'type' => $_FILES["video_thumb"]["type"],
											'types' => 'jpeg,png,jpg,gif',
											'crop' => array(
												'width' => 525,
												'height' => 295
											)
										);
										$media    = Wo_ShareFile($fileInfo);
										if (!empty($media)) {
											$video_thumb = $media['filename'];
										}
									}
								}
							}
						}
						if (isset($_FILES['postMusic']['name']) && empty($mediaFilename)) {
							if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postMusic"]["tmp_name"],
									'name' => $_FILES['postMusic']['name'],
									'size' => $_FILES["postMusic"]["size"],
									'type' => $_FILES["postMusic"]["type"],
									'types' => 'mp3,wav'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilename = $media['filename'];
									$mediaName     = $media['name'];
								}
							}
						}
						$post_data = array(
							'user_id' => Wo_Secure($wo['user']['user_id']),
							'fund_draft_id' => $fund_id,
							'time' => time(),
							'postRecord' => Wo_Secure($_POST['postRecord']),
							'multi_image' => Wo_Secure($multi),
							'multi_image_post' => 0,
							'postText' => Wo_Secure($post_text),
							'album_name' => Wo_Secure($album_name),
							'postFile' => Wo_Secure($mediaFilename, 0),
							'postFileName' => Wo_Secure($mediaName),
							'postFileThumb' => Wo_Secure($video_thumb),
							'blur' => $blur,
							'postPrivacy' => 0,
						);

						$id = Wo_RegisterPost($post_data);

						if (isset($mentions) && is_array($mentions)) {
							foreach ($mentions as $mention) {
								if (empty($wo['no_mention']) || (!empty($wo['no_mention']) && !in_array($mention, $wo['no_mention']))) {
									$notification_data_array = array(
										'recipient_id' => $mention,
										'type' => 'post_mention',
										'page_id' => 0,
										'post_id' => $id,
										'url' => 'index.php?link1=show_fund&id=' . $insert_array['hashed_id']
									);
									Wo_RegisterNotification($notification_data_array);
								}
							}
						}
						if (isset($_FILES['postPhotos']['name'])) {
							if (count($_FILES['postPhotos']['name']) > 0) {
								$data['imageUpload'] = 'photos > 0';
								for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
									$fileInfo = array(
										'file' => $_FILES["postPhotos"]["tmp_name"][$i],
										'name' => $_FILES['postPhotos']['name'][$i],
										'size' => $_FILES["postPhotos"]["size"][$i],
										'type' => $_FILES["postPhotos"]["type"][$i],
										'types' => 'jpg,png,jpeg,gif'
									);
									$file     = Wo_ShareFile($fileInfo, 1);
									$image_file = Wo_GetMedia($file['filename']);
									if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
										$blur = 1;
									} elseif ($wo['config']['adult_images'] == 1 && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
										$invalid_file = 3;
										$errors[] = $error_icon . $wo['lang']['adult_image_file'];
										Wo_DeletePost($id);
										@unlink($file['filename']);
										Wo_DeleteFromToS3($file['filename']);
									}
									if (!empty($file)) {
										$media_album = Wo_RegisterAlbumMedia($id, $file['filename']);
										$post_data['multi_image'] = 0;
										$post_data['multi_image_post'] = 1;
										$post_data['album_name'] = '';
										$post_data['postFile'] = $file['filename'];
										$post_data['postFileName'] = $file['name'];
										$post_data['active'] = 1;
										$new_id = Wo_RegisterPost($post_data);
										$media_album = Wo_RegisterAlbumMedia($new_id, $file['filename'], $id);
										$data['image-uploaded'] = 'uploaded' . $new_id . "media_album" . $media_album;
									}
									$data['i-' . $i] = 'uploaded';
								}
							}
						}
                        $response_data = [
                            'api_status' => 200,
                            'message' => $wo['lang']['funding_draft_created']
                        ];
					} else {

                        $error_code = 2;
                        $error_message = $wo['lang']['please_check_details'];
					}
				} else {
                    $error_code= 3;
                    $error_message = $wo['lang']['file_not_supported']." Image Issue";
					
				}
			} else {
                $error_code= 4;
                $error_message = $wo['lang']['please_check_details'];
			}
		}
	}
    if($_POST['type'] == 'delete_draft_fund'){
        // get id = funding draft id
        if (!empty($_POST['id'])) {
			$id = Wo_Secure($_POST['id']);
			$fund = $db->where('id', $id)->getOne(T_FUNDING_DRAFT);
			if (!empty($fund) || ($wo['user']['user_id'] != $fund->user_id && Wo_IsAdmin() == false)) {

				@Wo_DeleteFromToS3($fund->image);

				if (file_exists($fund->image)) {
					try {
						unlink($fund->image);
					} catch (Exception $e) {
					}
				}

				$db->where('id', $id)->delete(T_FUNDING_DRAFT);
				$raise = $db->where('funding_id', $id)->get(T_FUNDING_RAISE);
				$db->where('funding_id', $id)->delete(T_FUNDING_RAISE);
				$posts = $db->where('fund_draft_id', $id)->get(T_POSTS);
				if (!empty($posts)) {
					foreach ($posts as $key => $value) {
						$db->where('parent_id', $value->id)->delete(T_POSTS);
					}
				}

				$db->where('fund_draft_id', $id)->delete(T_POSTS);
				foreach ($raise as $key => $value) {
					$raise_posts = $db->where('fund_raise_id', $value->id)->get(T_POSTS);
					if (!empty($raise_posts)) {
						foreach ($posts as $key => $value1) {
							$db->where('parent_id', $value1->id)->delete(T_POSTS);
						}
					}
					$db->where('fund_raise_id', $value->id)->delete(T_POSTS);
				}

				$response_data = [
                    'api_status' => 200,
                    'message' => 'Campaign Draft Deleted Succesfully!'
                ];
			} else {
                $error = 1;
				$error_message = $wo['lang']['please_check_details'];
			}
		} else {
            $error = 2;
            $error_message = $wo['lang']['please_check_details'];
		}
    }
    if ($_POST['type'] == 'edit') {
        // type_fund = draft,
        // if type fund is entered draft campaign will be published
        // if not settled than live campaign will be edited
        if(!isset($_POST['type_fund']) && empty($_POST['type_fund'])){
			if (!empty($_POST['title'])  && !empty($_POST['description']) && !empty($_POST['amount']) && is_numeric($_POST['amount']) && $_POST['amount'] > 0) {
				$id = Wo_Secure($_POST['id']);
				$fund = $db->where('id', $id)->getOne(T_FUNDING);
				$post = $db->where('fund_id',$id)->getOne(T_POSTS);
				$posts = $db->where('fund_id',$id)->get(T_POSTS);
				$mention_regex = '/@([A-Za-z0-9_]+)/i';
				// if (!empty($fund) || ($wo['user']['user_id'] != $fund->user_id && Wo_IsAdmin() == false)) {
				if (!empty($fund)) {
					if (empty($_POST['beneficiary'])) {
                        $error_code    = 1;
                        $error_message = 'Beneficiary is required';
                        $response_data['error'] = array('error_id' => $error_code,'error_message' => $error_message);
                        echo json_encode($response_data);
                        exit();
					} else {
						$beneficiary = Wo_Secure($_POST['beneficiary']);
						preg_match_all($mention_regex, $beneficiary, $matches);
						foreach ($matches[1] as $match) {
							$match         = Wo_Secure($match);
							$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
							$match_search  = '@' . $match;
							$match_replace = '@[' . $match_user['user_id'] . ']';
							if (isset($match_user['user_id'])) {
								$beneficiary  = str_replace($match_search, $match_replace, $beneficiary);
								$mentions[] = $match_user['user_id'];
							}
						}
						$data['beneficiaryText'] = $beneficiary;
						if (sizeof($mentions) != 1) {
                            $error_code = 11;
                            $error_message = "beneficiary must be 1 person";
                            $response_data['error'] = array('error_id' => $error_code,'error_message' => $error_message);
                            echo json_encode($response_data);
                            exit();
						}
					}
					if(isset($_POST['funding_admins']) && !empty($_POST['funding_admins'])){	
						$funding_admins = Wo_Secure($_POST['funding_admins']);
						preg_match_all($mention_regex, $funding_admins, $matches);
						foreach ($matches[1] as $match) {
							$match         = Wo_Secure($match);
							$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
							$match_search  = '@' . $match;
							$match_replace = '@[' . $match_user['user_id'] . ']';
							if (isset($match_user['user_id'])) {
								$funding_admins  = str_replace($match_search, $match_replace, $funding_admins);
								$fundingAdmins[] = $match_user['user_id'];
							}
						}
					}
					if (isset($_POST['postTextCategory']) && !empty($_POST['postTextCategory'])) {
						$text = $_POST['postTextCategory'];

						$hashtag_regex = '/(#\[([0-9]+)\])/i';
						preg_match_all($hashtag_regex, $text, $matches);
						$match_i = 0;
						foreach ($matches[1] as $match) {
							$hashkey  = $matches[2][$match_i];
							if (!empty($hashkey)) {
								$db->where('id', $hashkey)->update(T_HASHTAGS, array('trend_use_num' => $db->dec(1)));
							}
							$match_i++;
						}
						$post_text = $post_text . " " . $text;
					}

					$media = array();
					if (!empty($_FILES['image'])) {
						$fileInfo      = array(
							'file' => $_FILES["image"]["tmp_name"],
							'name' => $_FILES['image']['name'],
							'size' => $_FILES["image"]["size"],
							'type' => $_FILES["image"]["type"],
							'types' => 'jpeg,jpg,png,bmp'
						);
						$media  = Wo_ShareFile($fileInfo);
					}
					$fund->title =Wo_Secure($_POST['title']);
					$fund->description =Wo_Secure($_POST['description']);
					$fund->amount =Wo_Secure($_POST['amount']);
					$fund->beneficiary =$beneficiary;
					$fund->funding_admins =$funding_admins;
			
					if(!empty($media) && !empty($media['filename'])){
						$fund->image =$media['filename'];
					}

					$due_date = Wo_Secure($_POST['due_date']);
					if ($due_date) {
						$fund->due_date = $due_date;
					}
					$fundData = json_decode(json_encode($fund), true);

					$updatedFund = $db->where('id', $id)->update(T_FUNDING, $fundData);
					if($updatedFund){
							//multiple options uploads
							// $postPhotos = "";
							// $postVideo = "";
							$mediaFilename = '';
							$post_photo    = '';
							$mediaName     = '';
							$video_thumb   = '';
							$album_name = '';
							$blur = 0;

							if (!empty($_POST['album_name'])) {
								$album_name = $_POST['album_name'];
							}
							if (!isset($_FILES['postPhotos']['name'])) {
								$album_name = '';
							}
							$multi = 0;

							if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) {
								if (count($_FILES['postPhotos']['name']) == 1) {
									if ($_FILES['postPhotos']['size'][0] > $wo['config']['maxUpload']) {
										$invalid_file = 1;
									} else if (Wo_IsFileAllowed($_FILES['postPhotos']['name'][0]) == false) {
										$invalid_file = 2;
									} else {

										$fileInfo = array(
											'file' => $_FILES["postPhotos"]["tmp_name"][0],
											'name' => $_FILES['postPhotos']['name'][0],
											'size' => $_FILES["postPhotos"]["size"][0],
											'type' => $_FILES["postPhotos"]["type"][0]
										);
										$media    = Wo_ShareFile($fileInfo);
										if (!empty($media)) {
											$image_file = Wo_GetMedia($media['filename']);
											$upload = true;
											if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
												$blur = 1;
											} elseif ($wo['config']['adult_images'] == 1  && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
												$invalid_file = 3;
												$upload = false;
												@unlink($media['filename']);
												Wo_DeleteFromToS3($media['filename']);
											}
											$mediaFilename = $media['filename'];
											$mediaName     = $media['name'];
										}
									}
								} else {
									$multi = 1;
									$data['multi'] = $multi;
								}
							}
							if (isset($_FILES['postPhotos']['name'])) {
								$allowed = array(
									'gif',
									'png',
									'jpg',
									'jpeg'
								);
								for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
									$new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
									if (!in_array(strtolower($new_string['extension']), $allowed)) {
										$errors[] = $error_icon . $wo['lang']['please_check_details'];
									}
								}
							}
							if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
								$headers = get_headers($_POST['postSticker'], 1);
								if (strpos($headers['Content-Type'], 'image/') !== false) {
									$post_data['postSticker'] = $_POST['postSticker'];
								} else {
									$invalid_file = 2;
								}
							} else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
								if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
									$post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
								}
							}

							if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
								if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
									$invalid_file = 1;
								} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
									$invalid_file = 2;
								} else {
									$fileInfo = array(
										'file' => $_FILES["postVideo"]["tmp_name"],
										'name' => $_FILES['postVideo']['name'],
										'size' => $_FILES["postVideo"]["size"],
										'type' => $_FILES["postVideo"]["type"],
										'types' => 'mp4,m4v,webm,flv,mov,mpeg'
									);
									$media    = Wo_ShareFile($fileInfo);
									if (!empty($media)) {
										$mediaFilename = $media['filename'];
										$mediaName     = $media['name'];
										$img_types     = array(
											'image/png',
											'image/jpeg',
											'image/jpg',
											'image/gif'
										);
										if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
											$fileInfo = array(
												'file' => $_FILES["video_thumb"]["tmp_name"],
												'name' => $_FILES['video_thumb']['name'],
												'size' => $_FILES["video_thumb"]["size"],
												'type' => $_FILES["video_thumb"]["type"],
												'types' => 'jpeg,png,jpg,gif',
												'crop' => array(
													'width' => 525,
													'height' => 295
												)
											);
											$media   = Wo_ShareFile($fileInfo);
											if (!empty($media)) {
												$video_thumb = $media['filename'];
											}
										}
									}
								}
							}
							if (isset($_FILES['postMusic']['name']) && empty($mediaFilename)) {
								if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
									$invalid_file = 1;
								} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
									$invalid_file = 2;
								} else {
									$fileInfo = array(
										'file' => $_FILES["postMusic"]["tmp_name"],
										'name' => $_FILES['postMusic']['name'],
										'size' => $_FILES["postMusic"]["size"],
										'type' => $_FILES["postMusic"]["type"],
										'types' => 'mp3,wav'
									);
									$media    = Wo_ShareFile($fileInfo);
									if (!empty($media)) {
										$mediaFilename = $media['filename'];
										$mediaName     = $media['name'];
									}
								}
							}
					
							$post->postText = Wo_Secure($post_text);
							if($multi == 1){
								$post->multi_image = Wo_Secure($multi);
							}
							if(!empty($mediaFilename)){
								$post->postFile = Wo_Secure($mediaFilename, 0);
							}
							if(!empty($mediaName)){
								$post->postFileName = Wo_Secure($mediaName);
							}
							if(!empty($video_thumb)){
								$post->postFileThumb = Wo_Secure($video_thumb);
							}
							if($blue == 1){
								$post->blur = $blur;
							}
							if(isset($_POST['postRecord'])){
								$post->postRecord=Wo_Secure($_POST['postRecord']);
							}
							$post_data = json_decode(json_encode($post), true);
							$updatedPost = $db->where('id', $post_data['post_id'])->update(T_POSTS, $post_data);
							$id = $post_data['post_id'];
							unset($post_data['id']);
							unset($post_data['post_id']);
							unset($post_data['parent_id']);
							
							if (isset($_FILES['postPhotos']['name'])) {						
								if (count($_FILES['postPhotos']['name']) > 0) {
									foreach($posts as $imgpost){
										if($imgpost->multi_image != 1){
											Wo_DeletePost($imgpost->id);
										}
									}
									$data['imageUpload'] = 'photos > 0';
									for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
										$fileInfo = array(
											'file' => $_FILES["postPhotos"]["tmp_name"][$i],
											'name' => $_FILES['postPhotos']['name'][$i],
											'size' => $_FILES["postPhotos"]["size"][$i],
											'type' => $_FILES["postPhotos"]["type"][$i],
											'types' => 'jpg,png,jpeg,gif'
										);
										$file     = Wo_ShareFile($fileInfo, 1);
										$image_file = Wo_GetMedia($file['filename']);
										if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
											$blur = 1;
										} elseif ($wo['config']['adult_images'] == 1 && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
											$invalid_file = 3;
											$errors[] = $error_icon . $wo['lang']['adult_image_file'];
											Wo_DeletePost($id);
											@unlink($file['filename']);
											Wo_DeleteFromToS3($file['filename']);
										}
										if (!empty($file)) {
											$media_album = Wo_RegisterAlbumMedia($id, $file['filename']);
											$post_data['multi_image'] = 0;
											$post_data['multi_image_post'] = 1;
											$post_data['album_name'] = '';
											$post_data['postFile'] = $file['filename'];
											$post_data['postFileName'] = $file['name'];
											$post_data['active'] = 1;
											$new_id = Wo_RegisterPost($post_data);
											$media_album = Wo_RegisterAlbumMedia($new_id, $file['filename'], $id);
											$data['image-uploaded'] = 'uploaded' . $new_id . "media_album" . $media_album;
										}
										$data['i-' . $i] = 'uploaded';
									}
								}
							}

						$response_data['api_status'] = 200;
						$response_data['message'] = $wo['lang']['funding_edited'];
					} 
				} else {
                    $error_code = 1;
					$error_message =  $wo['lang']['please_check_details'];
				}
			} else {
                $error_code = 2;
				$error_message = $wo['lang']['please_check_details'];
			}
		}else{
			// publishing saved draft funding here sameer
			if (!empty($_POST['title'])  && !empty($_POST['description']) && !empty($_POST['amount']) && is_numeric($_POST['amount']) && $_POST['amount'] > 0) {
				$id = Wo_Secure($_POST['id']);
				$fund = $db->where('id', $id)->getOne(T_FUNDING_DRAFT);
				$post = $db->where('fund_draft_id',$id)->getOne(T_POSTS);
				$posts = $db->where('fund_draft_id',$id)->get(T_POSTS);
				$mention_regex = '/@([A-Za-z0-9_]+)/i';
				if (!empty($fund)) {
					if (empty($_POST['beneficiary'])) {
                        $error_code    = 1;
                        $error_message = 'Beneficiary is required';
                        $response_data['error'] = array('error_id' => $error_code,'error_message' => $error_message);
                        echo json_encode($response_data);
                        exit();
					} else {
						$beneficiary = Wo_Secure($_POST['beneficiary']);
						preg_match_all($mention_regex, $beneficiary, $matches);
						foreach ($matches[1] as $match) {
							$match         = Wo_Secure($match);
							$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
							$match_search  = '@' . $match;
							$match_replace = '@[' . $match_user['user_id'] . ']';
							if (isset($match_user['user_id'])) {
								$beneficiary  = str_replace($match_search, $match_replace, $beneficiary);
								$mentions[] = $match_user['user_id'];
							}
						}
						$data['beneficiaryText'] = $beneficiary;
						if (sizeof($mentions) != 1) {
                            $error_code = 11;
                            $error_message = "beneficiary must be 1 person";
                            $response_data['error'] = array('error_id' => $error_code,'error_message' => $error_message);
                            echo json_encode($response_data);
                            exit();
						}
					}
					if(isset($_POST['funding_admins']) && !empty($_POST['funding_admins'])){	
						$funding_admins = Wo_Secure($_POST['funding_admins']);
						preg_match_all($mention_regex, $funding_admins, $matches);
						foreach ($matches[1] as $match) {
							$match         = Wo_Secure($match);
							$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
							$match_search  = '@' . $match;
							$match_replace = '@[' . $match_user['user_id'] . ']';
							if (isset($match_user['user_id'])) {
								$funding_admins  = str_replace($match_search, $match_replace, $funding_admins);
								$fundingAdmins[] = $match_user['user_id'];
							} 
						}
					}
					if (isset($_POST['postTextCategory']) && !empty($_POST['postTextCategory'])) {
						$text = $_POST['postTextCategory'];

						$hashtag_regex = '/(#\[([0-9]+)\])/i';
						preg_match_all($hashtag_regex, $text, $matches);
						$match_i = 0;
						foreach ($matches[1] as $match) {
							$hashkey  = $matches[2][$match_i];
							if (!empty($hashkey)) {
								$db->where('id', $hashkey)->update(T_HASHTAGS, array('trend_use_num' => $db->dec(1)));
							}
							$match_i++;
						}
						$post_text = $post_text . " " . $text;
					}

					$media = array();
					if (!empty($_FILES['image'])) {
						$fileInfo      = array(
							'file' => $_FILES["image"]["tmp_name"],
							'name' => $_FILES['image']['name'],
							'size' => $_FILES["image"]["size"],
							'type' => $_FILES["image"]["type"],
							'types' => 'jpeg,jpg,png,bmp'
						);
						$media  = Wo_ShareFile($fileInfo);
					}
					
					
					$fund->time = time();
					$fund->user_id  = $wo['user']['id'];
					$fund->hashed_id =  Wo_GenerateKey(15, 15);
					$fund->title =Wo_Secure($_POST['title']);
					$fund->description =Wo_Secure($_POST['description']);
					$fund->amount =Wo_Secure($_POST['amount']);
					$fund->beneficiary =$beneficiary;
					$fund->funding_admins =$funding_admins;
			
					if(!empty($media) && !empty($media['filename'])){
						$fund->image =$media['filename'];
					}

					$due_date = Wo_Secure($_POST['due_date']);
					if ($due_date) {
						$fund->due_date = $due_date;
					}
					$fundData = json_decode(json_encode($fund), true);

					unset($fundData['id']);
					unset($fundData['type']);
					
					$fund_id = $db->insert(T_FUNDING, $fundData);
					$isdraftUpdated = $db->where('id',$fund->id)->update(T_FUNDING_DRAFT,['status' => 0]);
					if($fund_id && $isdraftUpdated){
							//multiple options uploads

							$mediaFilename = '';
							$post_photo    = '';
							$mediaName     = '';
							$video_thumb   = '';
							$album_name = '';
							$blur = 0;

							if (!empty($_POST['album_name'])) {
								$album_name = $_POST['album_name'];
							}
							if (!isset($_FILES['postPhotos']['name'])) {
								$album_name = '';
							}
							$multi = 0;

							if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) {
								if (count($_FILES['postPhotos']['name']) == 1) {
									if ($_FILES['postPhotos']['size'][0] > $wo['config']['maxUpload']) {
										$invalid_file = 1;
									} else if (Wo_IsFileAllowed($_FILES['postPhotos']['name'][0]) == false) {
										$invalid_file = 2;
									} else {

										$fileInfo = array(
											'file' => $_FILES["postPhotos"]["tmp_name"][0],
											'name' => $_FILES['postPhotos']['name'][0],
											'size' => $_FILES["postPhotos"]["size"][0],
											'type' => $_FILES["postPhotos"]["type"][0]
										);
										$media    = Wo_ShareFile($fileInfo);
										if (!empty($media)) {
											$image_file = Wo_GetMedia($media['filename']);
											$upload = true;
											if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
												$blur = 1;
											} elseif ($wo['config']['adult_images'] == 1  && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
												$invalid_file = 3;
												$upload = false;
												@unlink($media['filename']);
												Wo_DeleteFromToS3($media['filename']);
											}
											$mediaFilename = $media['filename'];
											$mediaName     = $media['name'];
										}
									}
								} else {
									$multi = 1;
									$data['multi'] = $multi;
								}
							}
							if (isset($_FILES['postPhotos']['name'])) {
								$allowed = array(
									'gif',
									'png',
									'jpg',
									'jpeg'
								);
								for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
									$new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
									if (!in_array(strtolower($new_string['extension']), $allowed)) {
										$errors[] = $error_icon . $wo['lang']['please_check_details'];
									}
								}
							}
							if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
								$headers = get_headers($_POST['postSticker'], 1);
								if (strpos($headers['Content-Type'], 'image/') !== false) {
									$post_data['postSticker'] = $_POST['postSticker'];
								} else {
									$invalid_file = 2;
								}
							} else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
								if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
									$post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
								}
							}

							if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
								if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
									$invalid_file = 1;
								} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
									$invalid_file = 2;
								} else {
									$fileInfo = array(
										'file' => $_FILES["postVideo"]["tmp_name"],
										'name' => $_FILES['postVideo']['name'],
										'size' => $_FILES["postVideo"]["size"],
										'type' => $_FILES["postVideo"]["type"],
										'types' => 'mp4,m4v,webm,flv,mov,mpeg'
									);
									$media    = Wo_ShareFile($fileInfo);
									if (!empty($media)) {
										$mediaFilename = $media['filename'];
										$mediaName     = $media['name'];
										$img_types     = array(
											'image/png',
											'image/jpeg',
											'image/jpg',
											'image/gif'
										);
										if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
											$fileInfo = array(
												'file' => $_FILES["video_thumb"]["tmp_name"],
												'name' => $_FILES['video_thumb']['name'],
												'size' => $_FILES["video_thumb"]["size"],
												'type' => $_FILES["video_thumb"]["type"],
												'types' => 'jpeg,png,jpg,gif',
												'crop' => array(
													'width' => 525,
													'height' => 295
												)
											);
											$media   = Wo_ShareFile($fileInfo);
											if (!empty($media)) {
												$video_thumb = $media['filename'];
											}
										}
									}
								}
							}
							if (isset($_FILES['postMusic']['name']) && empty($mediaFilename)) {
								if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
									$invalid_file = 1;
								} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
									$invalid_file = 2;
								} else {
									$fileInfo = array(
										'file' => $_FILES["postMusic"]["tmp_name"],
										'name' => $_FILES['postMusic']['name'],
										'size' => $_FILES["postMusic"]["size"],
										'type' => $_FILES["postMusic"]["type"],
										'types' => 'mp3,wav'
									);
									$media    = Wo_ShareFile($fileInfo);
									if (!empty($media)) {
										$mediaFilename = $media['filename'];
										$mediaName     = $media['name'];
									}
								}
							}
						
							$post->fund_id = $fund_id;
							$post->postText = Wo_Secure($post_text);
							if($multi == 1){
								$post->multi_image = Wo_Secure($multi);
							}
							if(!empty($mediaFilename)){
								$post->postFile = Wo_Secure($mediaFilename, 0);
							}
							if(!empty($mediaName)){
								$post->postFileName = Wo_Secure($mediaName);
							}
							if(!empty($video_thumb)){
								$post->postFileThumb = Wo_Secure($video_thumb);
							}
							if($blue == 1){
								$post->blur = $blur;
							}
							if(isset($_POST['postRecord'])){
								$post->postRecord=Wo_Secure($_POST['postRecord']);
							}
							$post_data = json_decode(json_encode($post), true);
							$updatedPost = $db->where('id', $post_data['post_id'])->update(T_POSTS, $post_data);
							$id = $post_data['post_id'];
							unset($post_data['id']);
							unset($post_data['post_id']);
							unset($post_data['parent_id']);

							if (isset($mentions) && is_array($mentions)) {
								foreach ($mentions as $mention) {
									if (empty($wo['no_mention']) || (!empty($wo['no_mention']) && !in_array($mention, $wo['no_mention']))) {
										$notification_data_array = array(
											'recipient_id' => $mention,
											'type' => 'post_mention',
											'page_id' => 0,
											'post_id' => $id,
											'url' => 'index.php?link1=show_fund&id=' . $insert_array['hashed_id']
										);
										Wo_RegisterNotification($notification_data_array);
									}
								}
							}
							if (isset($_FILES['postPhotos']['name'])) {						
								if (count($_FILES['postPhotos']['name']) > 0) {
									foreach($posts as $imgpost){
										if($imgpost->multi_image != 1){
											Wo_DeletePost($imgpost->id);
										}
									}
									$data['imageUpload'] = 'photos > 0';
									for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
										$fileInfo = array(
											'file' => $_FILES["postPhotos"]["tmp_name"][$i],
											'name' => $_FILES['postPhotos']['name'][$i],
											'size' => $_FILES["postPhotos"]["size"][$i],
											'type' => $_FILES["postPhotos"]["type"][$i],
											'types' => 'jpg,png,jpeg,gif'
										);
										$file     = Wo_ShareFile($fileInfo, 1);
										$image_file = Wo_GetMedia($file['filename']);
										if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
											$blur = 1;
										} elseif ($wo['config']['adult_images'] == 1 && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
											$invalid_file = 3;
											$errors[] = $error_icon . $wo['lang']['adult_image_file'];
											Wo_DeletePost($id);
											@unlink($file['filename']);
											Wo_DeleteFromToS3($file['filename']);
										}
										if (!empty($file)) {
											$media_album = Wo_RegisterAlbumMedia($id, $file['filename']);
											$post_data['multi_image'] = 0;
											$post_data['multi_image_post'] = 1;
											$post_data['album_name'] = '';
											$post_data['postFile'] = $file['filename'];
											$post_data['postFileName'] = $file['name'];
											$post_data['active'] = 1;
											$new_id = Wo_RegisterPost($post_data);
											$media_album = Wo_RegisterAlbumMedia($new_id, $file['filename'], $id);
											$data['image-uploaded'] = 'uploaded' . $new_id . "media_album" . $media_album;
										}
										$data['i-' . $i] = 'uploaded';
									}
								}
							}


						$response_data['api_status'] = 200;
						$response_data['message'] = $wo['lang']['funding_edited'];
						// $data['url'] = $wo['config']['site_url'] . '/show_fund/' . $fund->hashed_id;
					} 
				} else {
                    $error_code = 1;
                    $error_message =  $wo['lang']['please_check_details'];
				}
			} else {
				$data['message'] = $error_icon . $wo['lang']['please_check_details'];
			}
			// publishing saved draft funding here sameer end
		}
    }
		// sameer challenged
        // postText for tagging
    if ($_POST['type'] == 'insert_challenge') {
        $sqlConnect;
        if ($wo['config']['who_upload'] == 'pro' && $wo['user']['is_pro'] == 0 && !Wo_IsAdmin() && !empty($_FILES['image'])) {
            $data['message'] = $error_icon . $wo['lang']['free_plan_upload_pro'];
        } else {
            if (!empty($_POST['title'])  && !empty($_POST['description'])) {

                // Tagging code will be used for challenge not for campgian sameer
                $post_text = "";
                $mention_regex = '/@([A-Za-z0-9_]+)/i';

                $mention_regex = '/@([A-Za-z0-9_]+)/i';
                if(empty($_POST['postText'])){
                    $error_code    = 1;
                    $error_message = 'please tag at least 3 friends';
                    $response_data['error'] = array('error_id' => $error_code,'error_message' => $error_message);
                    echo json_encode($response_data);
					exit();
                }else{
                    $post_text = Wo_Secure($_POST['postText']);
                    preg_match_all($mention_regex, $post_text, $matches);
                    foreach ($matches[1] as $match) {
                        $match         = Wo_Secure($match);
                        $match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
                        $match_search  = '@' . $match;
                        $match_replace = '@[' . $match_user['user_id'] . ']';
                        if (isset($match_user['user_id'])) {
                            $post_text  = str_replace($match_search, $match_replace, $post_text);
                            $mentions[] = $match_user['user_id'];
                        }
                    }
                    if(sizeof($mentions) < 3){     
                        $error_code    = 2;
                        $error_message = 'please tag at least 3 friends';
                        $response_data['error'] = array('error_id' => $error_code,'error_message' => $error_message);
                        echo json_encode($response_data);
                        exit();
                    }
                }
                // Tagging code end;
                $media = array();
                $multi = 0;
                if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) {
                    if (count($_FILES['postPhotos']['name']) == 1) {
                        if ($_FILES['postPhotos']['size'][0] > $wo['config']['maxUpload']) {
                            $invalid_file = 1;
                            $data['invalid_file'] = $invalid_file;
                        } else if (Wo_IsFileAllowed($_FILES['postPhotos']['name'][0]) == false) {
                            $invalid_file = 2;
                            $data['invalid_file'] = $invalid_file;
                        } else {

                            $fileInfo = array(
                                'file' => $_FILES["postPhotos"]["tmp_name"][0],
                                'name' => $_FILES['postPhotos']['name'][0],
                                'size' => $_FILES["postPhotos"]["size"][0],
                                'type' => $_FILES["postPhotos"]["type"][0]
                            );
                            $media    = Wo_ShareFile($fileInfo);
                            if (!empty($media)) {
                                $image_file = Wo_GetMedia($media['filename']);
                                $upload = true;
                                if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
                                    $blur = 1;
                                } elseif ($wo['config']['adult_images'] == 1  && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
                                    $invalid_file = 3;
                                    $upload = false;
                                    @unlink($media['filename']);
                                    Wo_DeleteFromToS3($media['filename']);
                                }
                                $mediaFilename = $media['filename'];
                                $mediaName     = $media['name'];
                            }
                        }
                    } else {
                        $multi = 1;
                        $data['multi'] = $multi;
                    }
                }
                if (isset($_FILES['postPhotos']['name'])) {
                    $allowed = array(
                        'gif',
                        'png',
                        'jpg',
                        'jpeg'
                    );
                    for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
                        $new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
                        if (!in_array(strtolower($new_string['extension']), $allowed)) {
                            $errors[] = $error_icon . $wo['lang']['please_check_details'];
                        }
                    }
                }
                if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
                    $headers = get_headers($_POST['postSticker'], 1);
                    if (strpos($headers['Content-Type'], 'image/') !== false) {
                        $post_data['postSticker'] = $_POST['postSticker'];
                    } else {
                        $invalid_file = 2;
                    }
                } else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
                    if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
                        $post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
                    }
                }
                if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
                    if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
                        $invalid_file = 1;
                    } else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
                        $invalid_file = 2;
                    } else {
                        $fileInfo = array(
                            'file' => $_FILES["postVideo"]["tmp_name"],
                            'name' => $_FILES['postVideo']['name'],
                            'size' => $_FILES["postVideo"]["size"],
                            'type' => $_FILES["postVideo"]["type"],
                            'types' => 'mp4,m4v,webm,flv,mov,mpeg'
                        );
                        $media    = Wo_ShareFile($fileInfo);
                        if (!empty($media)) {
                            $mediaFilename = $media['filename'];
                            $mediaName     = $media['name'];
                            $img_types     = array(
                                'image/png',
                                'image/jpeg',
                                'image/jpg',
                                'image/gif'
                            );
                            if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
                                $fileInfo = array(
                                    'file' => $_FILES["video_thumb"]["tmp_name"],
                                    'name' => $_FILES['video_thumb']['name'],
                                    'size' => $_FILES["video_thumb"]["size"],
                                    'type' => $_FILES["video_thumb"]["type"],
                                    'types' => 'jpeg,png,jpg,gif',
                                    'crop' => array(
                                        'width' => 525,
                                        'height' => 295
                                    )
                                );
                                $media    = Wo_ShareFile($fileInfo);
                                if (!empty($media)) {
                                    $video_thumb = $media['filename'];
                                }
                            }
                        }
                    }
                }
                if (isset($_FILES['postMusic']['name']) && empty($mediaFilename)) {
                    if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
                        $invalid_file = 1;
                    } else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
                        $invalid_file = 2;
                    } else {
                        $fileInfo = array(
                            'file' => $_FILES["postMusic"]["tmp_name"],
                            'name' => $_FILES['postMusic']['name'],
                            'size' => $_FILES["postMusic"]["size"],
                            'type' => $_FILES["postMusic"]["type"],
                            'types' => 'mp3,wav'
                        );
                        $media    = Wo_ShareFile($fileInfo);
                        if (!empty($media)) {
                            $mediaFilename = $media['filename'];
                            $mediaName     = $media['name'];
                        }
                    }
                }

                
                if (!empty($media) && !empty($media['filename'])) {
                    if(isset($_POST['challenge_id']) && !empty($_POST['challenge_id'])){
                        $challenge_id = Wo_Secure($_POST['challenge_id']);
                        // update challenge;
                        $challenge_data = $db->where('id',$challenge_id)->getOne(T_CHALLENGE);
                        // $challenge_data = json_decode(json_encode($challenge_data));
                        $challenge_query     = "UPDATE " . T_CHALLENGE . " SET `last_trend_time` = " . time() . ", `trend_use_num` = " . ($challenge_data->trend_use_num + 1) . " WHERE `id` = " . $challenge_id;
                        $challenge_sql_query = mysqli_query($sqlConnect, $challenge_query);

                    }else{
                
                        // insert Challenge 
                        $week = date('Y-m-d', strtotime(date('Y-m-d') . " +1week"));
                        $insert_challenge = array(
                            'hash' => md5(Wo_Secure($_POST['title'])),
                            'title' => Wo_Secure($_POST['title']),
                            'last_trend_time' => time(),
                            'trend_use_num' => 1,
                            'expire_date' => $week
                        ); 
                    
                        $challenge_id = $db->insert(T_CHALLENGE,$insert_challenge);
                    }
            
                    $insert_array = array(
                        'funding_id' => Wo_Secure($_POST['funding_id']),
                        'challenge_id' => Wo_Secure($challenge_id),
                        'invited_by' => 0,
                        'description'   => Wo_Secure($_POST['description']),
                        'invites' => Wo_Secure($post_text),
                        'time'   => time(),
                        'file' => $media['name'],
                        'fileName' => $media['filename'],
                        'user_id'  => $wo['user']['id'],
                        'status' => 1,
                        'hashed_id' => Wo_GenerateKey(15, 15)
                    );
                    // 'postRecord' => Wo_Secure($_POST['postRecord']),
            
                    $challenge_fund_id = $db->insert(T_CHALLENGE_FUNDING, $insert_array);
            
                    if(!empty($challenge_fund_id)){

                        $taggedUsers = Wo_GetTaggedUsers($post_text);
                        $tagged_users_challenge_fund_id = array();
                        foreach($taggedUsers as $user){
                            $invites_insert_array = array(
                                'funding_id' => Wo_Secure($_POST['funding_id']),
                                'challenge_id' => Wo_Secure($challenge_id),
                                'invited_by' => $wo['user']['id'],
                                'description'   => Wo_Secure($_POST['description']),
                                'invites' => '',
                                'time'   => time(),
                                'file' => '',
                                'fileName' => '',
                                'user_id'  => $user['id'],
                                'status' => 0,
                                'hashed_id' => Wo_GenerateKey(15, 15)
                            );
                        $iddd = $db->insert(T_CHALLENGE_FUNDING,$invites_insert_array);
                        $tagged_users_challenge_fund_id[] = array('user_id' => $user['id'],'cha_fund_id' => $iddd);
                        }
                        $data['mentions'] = array();

                        if (isset($mentions) && is_array($mentions)) {
                            foreach($tagged_users_challenge_fund_id as $val){	
                                foreach ($mentions as $mention) {
                                    if (empty($wo['no_mention']) || (!empty($wo['no_mention']) && !in_array($mention, $wo['no_mention']))) {
                                        if($mention == $val['user_id']){
                                            $notification_data_array = array(
                                                'recipient_id' => $mention,
                                                'type' => 'challenge_invitation',
                                                'page_id' => 0,
                                                'text' => 'You are Invited For Challenge',
                                                'challenge_funding_id' => $val['cha_fund_id'],
                                                'url' => 'index.php?link1=create_challenge&challenge_fund_id=' . $val['cha_fund_id'] .'&id='.Wo_Secure($_POST['funding_id'])
                                            );
                                            Wo_RegisterNotification($notification_data_array);
                                        }
                                    
                                    }
                                }
                            }
                            
                        }
                        $mediaFilename = '';
                        $post_photo    = '';
                        $mediaName     = '';
                        $video_thumb   = '';
                        $album_name = '';
                        $blur = 0;

                        if (!empty($_POST['album_name'])) {
                            $album_name = $_POST['album_name'];
                        }
                        if (!isset($_FILES['postPhotos']['name'])) {
                            $album_name = '';
                        }
                        $multi = 0;

                        if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) {
                            if (count($_FILES['postPhotos']['name']) == 1) {
                                if ($_FILES['postPhotos']['size'][0] > $wo['config']['maxUpload']) {
                                    $invalid_file = 1;
                                    $data['invalid_file'] = $invalid_file;
                                } else if (Wo_IsFileAllowed($_FILES['postPhotos']['name'][0]) == false) {
                                    $invalid_file = 2;
                                    $data['invalid_file'] = $invalid_file;
                                } else {

                                    $fileInfo = array(
                                        'file' => $_FILES["postPhotos"]["tmp_name"][0],
                                        'name' => $_FILES['postPhotos']['name'][0],
                                        'size' => $_FILES["postPhotos"]["size"][0],
                                        'type' => $_FILES["postPhotos"]["type"][0]
                                    );
                                    $media    = Wo_ShareFile($fileInfo);
                                    if (!empty($media)) {
                                        $image_file = Wo_GetMedia($media['filename']);
                                        $upload = true;
                                        if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
                                            $blur = 1;
                                        } elseif ($wo['config']['adult_images'] == 1  && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
                                            $invalid_file = 3;
                                            $upload = false;
                                            @unlink($media['filename']);
                                            Wo_DeleteFromToS3($media['filename']);
                                        }
                                        $mediaFilename = $media['filename'];
                                        $mediaName     = $media['name'];
                                    }
                                }
                            } else {
                                $multi = 1;
                                $data['multi'] = $multi;
                            }
                        }
                        if (isset($_FILES['postPhotos']['name'])) {
                            $allowed = array(
                                'gif',
                                'png',
                                'jpg',
                                'jpeg'
                            );
                            for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
                                $new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
                                if (!in_array(strtolower($new_string['extension']), $allowed)) {
                                    $errors[] = $error_icon . $wo['lang']['please_check_details'];
                                }
                            }
                        }
                        if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
                            $headers = get_headers($_POST['postSticker'], 1);
                            if (strpos($headers['Content-Type'], 'image/') !== false) {
                                $post_data['postSticker'] = $_POST['postSticker'];
                            } else {
                                $invalid_file = 2;
                            }
                        } else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
                            if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
                                $post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
                            }
                        }

                        if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
                            if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
                                $invalid_file = 1;
                            } else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
                                $invalid_file = 2;
                            } else {
                                $fileInfo = array(
                                    'file' => $_FILES["postVideo"]["tmp_name"],
                                    'name' => $_FILES['postVideo']['name'],
                                    'size' => $_FILES["postVideo"]["size"],
                                    'type' => $_FILES["postVideo"]["type"],
                                    'types' => 'mp4,m4v,webm,flv,mov,mpeg'
                                );
                                $media    = Wo_ShareFile($fileInfo);
                                if (!empty($media)) {
                                    $mediaFilename = $media['filename'];
                                    $mediaName     = $media['name'];
                                    $img_types     = array(
                                        'image/png',
                                        'image/jpeg',
                                        'image/jpg',
                                        'image/gif'
                                    );
                                    if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
                                        $fileInfo = array(
                                            'file' => $_FILES["video_thumb"]["tmp_name"],
                                            'name' => $_FILES['video_thumb']['name'],
                                            'size' => $_FILES["video_thumb"]["size"],
                                            'type' => $_FILES["video_thumb"]["type"],
                                            'types' => 'jpeg,png,jpg,gif',
                                            'crop' => array(
                                                'width' => 525,
                                                'height' => 295
                                            )
                                        );
                                        $media    = Wo_ShareFile($fileInfo);
                                        if (!empty($media)) {
                                            $video_thumb = $media['filename'];
                                        }
                                    }
                                }
                            }
                        }
                        if (isset($_FILES['postMusic']['name']) && empty($mediaFilename)) {
                            if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
                                $invalid_file = 1;
                            } else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
                                $invalid_file = 2;
                            } else {
                                $fileInfo = array(
                                    'file' => $_FILES["postMusic"]["tmp_name"],
                                    'name' => $_FILES['postMusic']['name'],
                                    'size' => $_FILES["postMusic"]["size"],
                                    'type' => $_FILES["postMusic"]["type"],
                                    'types' => 'mp3,wav'
                                );
                                $media    = Wo_ShareFile($fileInfo);
                                if (!empty($media)) {
                                    $mediaFilename = $media['filename'];
                                    $mediaName     = $media['name'];
                                }
                            }
                        }
                        $post_data = array(
                            'user_id' => Wo_Secure($wo['user']['user_id']),
                            'challenge_fund_id' => $challenge_fund_id,
                            'time' => time(),
                            'postRecord' => Wo_Secure($_POST['postRecord']),
                            'multi_image' => Wo_Secure($multi),
                            'multi_image_post' => 0,
                            'postText' => '   ',
                            'album_name' => Wo_Secure($album_name),
                            'postFile' => Wo_Secure($mediaFilename, 0),
                            'postFileName' => Wo_Secure($mediaName),
                            'postFileThumb' => Wo_Secure($video_thumb),
                            'blur' => $blur,
                            'postPrivacy' => 0,
                        );

                        $id = Wo_RegisterPost($post_data);
                        if(!empty($id)){
                            if (isset($_FILES['postPhotos']['name'])) {
                                if (count($_FILES['postPhotos']['name']) > 0) {
                                    $data['imageUpload'] = 'photos > 0';
    
                                    for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
                                        $fileInfo = array(
                                            'file' => $_FILES["postPhotos"]["tmp_name"][$i],
                                            'name' => $_FILES['postPhotos']['name'][$i],
                                            'size' => $_FILES["postPhotos"]["size"][$i],
                                            'type' => $_FILES["postPhotos"]["type"][$i],
                                            'types' => 'jpg,png,jpeg,gif'
                                        );
                                        $file     = Wo_ShareFile($fileInfo, 1);
                                        $image_file = Wo_GetMedia($file['filename']);
                                        if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
                                            $blur = 1;
                                        } elseif ($wo['config']['adult_images'] == 1 && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
                                            $invalid_file = 3;
                                            $errors[] = $error_icon . $wo['lang']['adult_image_file'];
                                            Wo_DeletePost($id);
                                            @unlink($file['filename']);
                                            Wo_DeleteFromToS3($file['filename']);
                                        }
                                        if (!empty($file)) {
                                            $media_album = Wo_RegisterAlbumMedia($id, $file['filename']);
                                            $post_data['multi_image'] = 0;
                                            $post_data['multi_image_post'] = 1;
                                            $post_data['album_name'] = '';
                                            $post_data['postFile'] = $file['filename'];
                                            $post_data['postFileName'] = $file['name'];
                                            $post_data['active'] = 1;
                                            $new_id = Wo_RegisterPost($post_data);
                                            $media_album = Wo_RegisterAlbumMedia($new_id, $file['filename'], $id);
                                            $data['image-uploaded'] = 'uploaded' . $new_id . "media_album" . $media_album;
                                        }
                                        $data['i-' . $i] = 'uploaded';
                                    }
                                }
                            }
                            $response_data['api_status'] = 200;
                            $response_data['message'] = $wo['lang']['challenge_created'];
                            // $response_data['url'] = Wo_SeoLink('index.php?link1=my_challenges');
                        }else{
                            $error_code = 3;
                            $error_message = $wo['lang']['please_check_details'] . "Post insert error";
                        }
                        
                    }else{
                        $error_code = 4;
                        $error_message = $error_icon . $wo['lang']['please_check_details'] . "Database Error";

                    }
                }else{
                    $error_code = 5;
                    $error_message =  $wo['lang']['please_check_details'] . " Regarding Media";
                }
            } else {
                $error_code = 5;
                $error_message =  $wo['lang']['please_check_details'];
            }
        }
    }
	// insert accepted challenge
	if ($_POST['type'] == 'insert_accepted_challenge') {
		$sqlConnect;
		if ($wo['config']['who_upload'] == 'pro' && $wo['user']['is_pro'] == 0 && !Wo_IsAdmin() && !empty($_FILES['image'])) {
			$data['message'] = $error_icon . $wo['lang']['free_plan_upload_pro'];
		} else {
			if (!empty($_POST['challenge_id']) && !empty($_POST['challenge_fund_id'])) {

				// Tagging code will be used for challenge not for campgian sameer
				$post_text = "";
				$mention_regex = '/@([A-Za-z0-9_]+)/i';

				$mention_regex = '/@([A-Za-z0-9_]+)/i';
				if(empty($_POST['postText'])){
                    $error_code    = 1;
                    $error_message = 'please tag at least 3 friends';
                    $response_data['error'] = array('error_id' => $error_code,'error_message' => $error_message);
                    echo json_encode($response_data);
					exit();
				}else{
					$post_text = Wo_Secure($_POST['postText']);
					preg_match_all($mention_regex, $post_text, $matches);
					foreach ($matches[1] as $match) {
						$match         = Wo_Secure($match);
						$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
						$match_search  = '@' . $match;
						$match_replace = '@[' . $match_user['user_id'] . ']';
						if (isset($match_user['user_id'])) {
							$post_text  = str_replace($match_search, $match_replace, $post_text);
							$mentions[] = $match_user['user_id'];
						}
					}
					if(sizeof($mentions) < 3){
                        $error_code    = 2;
                        $error_message = 'please tag at least 3 friends';
                        $response_data['error'] = array('error_id' => $error_code,'error_message' => $error_message);
                        echo json_encode($response_data);
                        exit();
					}
				}
				// Tagging code end;
				$media = array();
				$multi = 0;
				if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) {
					if (count($_FILES['postPhotos']['name']) == 1) {
						if ($_FILES['postPhotos']['size'][0] > $wo['config']['maxUpload']) {
							$invalid_file = 1;
							$data['invalid_file'] = $invalid_file;
						} else if (Wo_IsFileAllowed($_FILES['postPhotos']['name'][0]) == false) {
							$invalid_file = 2;
							$data['invalid_file'] = $invalid_file;
						} else {

							$fileInfo = array(
								'file' => $_FILES["postPhotos"]["tmp_name"][0],
								'name' => $_FILES['postPhotos']['name'][0],
								'size' => $_FILES["postPhotos"]["size"][0],
								'type' => $_FILES["postPhotos"]["type"][0]
							);
							$media    = Wo_ShareFile($fileInfo);
							if (!empty($media)) {
								$image_file = Wo_GetMedia($media['filename']);
								$upload = true;
								if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
									$blur = 1;
								} elseif ($wo['config']['adult_images'] == 1  && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
									$invalid_file = 3;
									$upload = false;
									@unlink($media['filename']);
									Wo_DeleteFromToS3($media['filename']);
								}
								$mediaFilename = $media['filename'];
								$mediaName     = $media['name'];
							}
						}
					} else {
						$multi = 1;
						$data['multi'] = $multi;
					}
				}
				if (isset($_FILES['postPhotos']['name'])) {
					$allowed = array(
						'gif',
						'png',
						'jpg',
						'jpeg'
					);
					for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
						$new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
						if (!in_array(strtolower($new_string['extension']), $allowed)) {
							$errors[] = $error_icon . $wo['lang']['please_check_details'];
						}
					}
				}
				if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
					$headers = get_headers($_POST['postSticker'], 1);
					if (strpos($headers['Content-Type'], 'image/') !== false) {
						$post_data['postSticker'] = $_POST['postSticker'];
					} else {
						$invalid_file = 2;
					}
				} else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
					if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
						$post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
					}
				}
				if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
					if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
						$invalid_file = 1;
					} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
						$invalid_file = 2;
					} else {
						$fileInfo = array(
							'file' => $_FILES["postVideo"]["tmp_name"],
							'name' => $_FILES['postVideo']['name'],
							'size' => $_FILES["postVideo"]["size"],
							'type' => $_FILES["postVideo"]["type"],
							'types' => 'mp4,m4v,webm,flv,mov,mpeg'
						);
						$media    = Wo_ShareFile($fileInfo);
						if (!empty($media)) {
							$mediaFilename = $media['filename'];
							$mediaName     = $media['name'];
							$img_types     = array(
								'image/png',
								'image/jpeg',
								'image/jpg',
								'image/gif'
							);
							if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
								$fileInfo = array(
									'file' => $_FILES["video_thumb"]["tmp_name"],
									'name' => $_FILES['video_thumb']['name'],
									'size' => $_FILES["video_thumb"]["size"],
									'type' => $_FILES["video_thumb"]["type"],
									'types' => 'jpeg,png,jpg,gif',
									'crop' => array(
										'width' => 525,
										'height' => 295
									)
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$video_thumb = $media['filename'];
								}
							}
						}
					}
				}
				if (isset($_FILES['postMusic']['name']) && empty($mediaFilename)) {
					if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
						$invalid_file = 1;
					} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
						$invalid_file = 2;
					} else {
						$fileInfo = array(
							'file' => $_FILES["postMusic"]["tmp_name"],
							'name' => $_FILES['postMusic']['name'],
							'size' => $_FILES["postMusic"]["size"],
							'type' => $_FILES["postMusic"]["type"],
							'types' => 'mp3,wav'
						);
						$media    = Wo_ShareFile($fileInfo);
						if (!empty($media)) {
							$mediaFilename = $media['filename'];
							$mediaName     = $media['name'];
						}
					}
				}

				if (!empty($media) && !empty($media['filename'])) {
					if(isset($_POST['challenge_id']) && !empty($_POST['challenge_id'])){
					
						$challenge_id = Wo_Secure($_POST['challenge_id']);
						// update challenge;
						$challenge_data = $db->where('id',$challenge_id)->getOne(T_CHALLENGE);
						// $challenge_data = json_decode(json_encode($challenge_data));
						
						$challenge_query     = "UPDATE " . T_CHALLENGE . " SET `last_trend_time` = " . time() . ", `trend_use_num` = '" . ($challenge_data->trend_use_num + 1) . "' WHERE `id` = '" . $challenge_id ."'";
                		$challenge_sql_query = mysqli_query($sqlConnect, $challenge_query);
				
						
					
					}else{
						// insert Challenge 
						$week = date('Y-m-d', strtotime(date('Y-m-d') . " +1week"));
						$insert_challenge = array(
							'hash' => md5(Wo_Secure($_POST['title'])),
							'title' => Wo_Secure($_POST['title']),
							'last_trend_time' => time(),
							'trend_use_num' => 1,
							'expire_date' => $week
						); 
						$challenge_id = $db->insert(T_CHALLENGE,$insert_challenge);
					}
					if(isset($_POST['challenge_fund_id']) && !empty($_POST['challenge_fund_id'])){
			
						$challenge_fund_id = $_POST['challenge_fund_id'];
						$challenge_fund = $db->where('id',$challenge_fund_id)->getOne(T_CHALLENGE_FUNDING);
						$challenge_fund->invites = Wo_Secure($post_text);
						$challenge_fund->time = time();
						if(!empty($media)){
							$challenge_fund->file =  $media['name'];							
							$challenge_fund->fileName = $media['filename'];
						}
						$challenge_fund->status = 1;
						$challenge_fund = (Array)$challenge_fund;
						
						$result = $db->where('id',$challenge_fund_id)->update(T_CHALLENGE_FUNDING, $challenge_fund);
						if(!$result){
							$challenge_fund_id = false;
						}
					}else{
                        $error_code = 3;
						$error_message = $error_icon . $wo['lang']['please_check_details'] . " Challenge Funding Id id not found";
					}
                    //Challenge Funding Updated for user invited
					if(!empty($challenge_fund_id)){

						$taggedUsers = Wo_GetTaggedUsers($post_text);
						foreach($taggedUsers as $user){
							$invites_insert_array = array(
								'funding_id' => Wo_Secure($_POST['funding_id']),
								'challenge_id' => Wo_Secure($challenge_id),
								'invited_by' => $wo['user']['id'],
								'description'   => Wo_Secure($challenge_fund['description']),
								'invites' => '',
								'time'   => time(),
								'file' => '',
								'fileName' => '',
								'user_id'  => $user['id'],
								'status' => 0,
								'hashed_id' => Wo_GenerateKey(15, 15)
							);
							$iddd = $db->insert(T_CHALLENGE_FUNDING,$invites_insert_array);
							$tagged_users_challenge_fund_id[] = array('user_id' => $user['id'],'cha_fund_id' => $iddd);
						}
						if (isset($mentions) && is_array($mentions)) {
							foreach($tagged_users_challenge_fund_id as $val){	
								foreach ($mentions as $mention) {
									if (empty($wo['no_mention']) || (!empty($wo['no_mention']) && !in_array($mention, $wo['no_mention']))) {
										if($mention == $val['user_id']){
											$notification_data_array = array(
												'recipient_id' => $mention,
												'type' => 'challenge_invitation',
												'page_id' => 0,
												'text' => 'You are Invited For Challenge',
												'challenge_funding_id' => $val['cha_fund_id'],
												'url' => 'index.php?link1=create_challenge&challenge_fund_id=' . $val['cha_fund_id'] .'&id='.Wo_Secure($_POST['funding_id'])
											);
											Wo_RegisterNotification($notification_data_array);
										}
									
									}
								}
							}
						}
						$mediaFilename = '';
						$post_photo    = '';
						$mediaName     = '';
						$video_thumb   = '';
						$album_name = '';
						$blur = 0;

						if (!empty($_POST['album_name'])) {
							$album_name = $_POST['album_name'];
						}
						if (!isset($_FILES['postPhotos']['name'])) {
							$album_name = '';
						}
						$multi = 0;

						if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) {
							if (count($_FILES['postPhotos']['name']) == 1) {
								if ($_FILES['postPhotos']['size'][0] > $wo['config']['maxUpload']) {
									$invalid_file = 1;
									$data['invalid_file'] = $invalid_file;
								} else if (Wo_IsFileAllowed($_FILES['postPhotos']['name'][0]) == false) {
									$invalid_file = 2;
									$data['invalid_file'] = $invalid_file;
								} else {

									$fileInfo = array(
										'file' => $_FILES["postPhotos"]["tmp_name"][0],
										'name' => $_FILES['postPhotos']['name'][0],
										'size' => $_FILES["postPhotos"]["size"][0],
										'type' => $_FILES["postPhotos"]["type"][0]
									);
									$media    = Wo_ShareFile($fileInfo);
									if (!empty($media)) {
										$image_file = Wo_GetMedia($media['filename']);
										$upload = true;
										if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
											$blur = 1;
										} elseif ($wo['config']['adult_images'] == 1  && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
											$invalid_file = 3;
											$upload = false;
											@unlink($media['filename']);
											Wo_DeleteFromToS3($media['filename']);
										}
										$mediaFilename = $media['filename'];
										$mediaName     = $media['name'];
									}
								}
							} else {
								$multi = 1;
								$data['multi'] = $multi;
							}
						}
						if (isset($_FILES['postPhotos']['name'])) {
							$allowed = array(
								'gif',
								'png',
								'jpg',
								'jpeg'
							);
							for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
								$new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
								if (!in_array(strtolower($new_string['extension']), $allowed)) {
									$errors[] = $error_icon . $wo['lang']['please_check_details'];
								}
							}
						}
						if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
							$headers = get_headers($_POST['postSticker'], 1);
							if (strpos($headers['Content-Type'], 'image/') !== false) {
								$post_data['postSticker'] = $_POST['postSticker'];
							} else {
								$invalid_file = 2;
							}
						} else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
							if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
								$post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
							}
						}

						if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
							if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postVideo"]["tmp_name"],
									'name' => $_FILES['postVideo']['name'],
									'size' => $_FILES["postVideo"]["size"],
									'type' => $_FILES["postVideo"]["type"],
									'types' => 'mp4,m4v,webm,flv,mov,mpeg'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilename = $media['filename'];
									$mediaName     = $media['name'];
									$img_types     = array(
										'image/png',
										'image/jpeg',
										'image/jpg',
										'image/gif'
									);
									if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
										$fileInfo = array(
											'file' => $_FILES["video_thumb"]["tmp_name"],
											'name' => $_FILES['video_thumb']['name'],
											'size' => $_FILES["video_thumb"]["size"],
											'type' => $_FILES["video_thumb"]["type"],
											'types' => 'jpeg,png,jpg,gif',
											'crop' => array(
												'width' => 525,
												'height' => 295
											)
										);
										$media    = Wo_ShareFile($fileInfo);
										if (!empty($media)) {
											$video_thumb = $media['filename'];
										}
									}
								}
							}
						}
						if (isset($_FILES['postMusic']['name']) && empty($mediaFilename)) {
							if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postMusic"]["tmp_name"],
									'name' => $_FILES['postMusic']['name'],
									'size' => $_FILES["postMusic"]["size"],
									'type' => $_FILES["postMusic"]["type"],
									'types' => 'mp3,wav'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilename = $media['filename'];
									$mediaName     = $media['name'];
								}
							}
						}
						$post_data = array(
							'user_id' => Wo_Secure($wo['user']['user_id']),
							'challenge_fund_id' => $challenge_fund_id,
							'time' => time(),
							'postRecord' => Wo_Secure($_POST['postRecord']),
							'multi_image' => Wo_Secure($multi),
							'multi_image_post' => 0,
							'postText' => '  ',
							'album_name' => Wo_Secure($album_name),
							'postFile' => Wo_Secure($mediaFilename, 0),
							'postFileName' => Wo_Secure($mediaName),
							'postFileThumb' => Wo_Secure($video_thumb),
							'blur' => $blur,
							'postPrivacy' => 0,
						);
						$id = Wo_RegisterPost($post_data);
						if(!empty($id)){
							if (isset($_FILES['postPhotos']['name'])) {
								if (count($_FILES['postPhotos']['name']) > 0) {
									$data['imageUpload'] = 'photos > 0';
	
									for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
										$fileInfo = array(
											'file' => $_FILES["postPhotos"]["tmp_name"][$i],
											'name' => $_FILES['postPhotos']['name'][$i],
											'size' => $_FILES["postPhotos"]["size"][$i],
											'type' => $_FILES["postPhotos"]["type"][$i],
											'types' => 'jpg,png,jpeg,gif'
										);
										$file     = Wo_ShareFile($fileInfo, 1);
										$image_file = Wo_GetMedia($file['filename']);
										if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
											$blur = 1;
										} elseif ($wo['config']['adult_images'] == 1 && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
											$invalid_file = 3;
											$errors[] = $error_icon . $wo['lang']['adult_image_file'];
											Wo_DeletePost($id);
											@unlink($file['filename']);
											Wo_DeleteFromToS3($file['filename']);
										}
										if (!empty($file)) {
											$media_album = Wo_RegisterAlbumMedia($id, $file['filename']);
											$post_data['multi_image'] = 0;
											$post_data['multi_image_post'] = 1;
											$post_data['album_name'] = '';
											$post_data['postFile'] = $file['filename'];
											$post_data['postFileName'] = $file['name'];
											$post_data['active'] = 1;
											$new_id = Wo_RegisterPost($post_data);
											$media_album = Wo_RegisterAlbumMedia($new_id, $file['filename'], $id);
											$data['image-uploaded'] = 'uploaded' . $new_id . "media_album" . $media_album;
										}
										$data['i-' . $i] = 'uploaded';
									}
								}
							}
							$response_data['status'] = 200;
							$response_data['message'] = $wo['lang']['challenge_created'];
							// $response_data['url'] = Wo_SeoLink('index.php?link1=my_challenges');

						}else{
                            $error_code = 4;
							$error_message =  $wo['lang']['please_check_details'] . " Post Error";
							}
					}else{
                        $error_code = 5;
						$error_message = $wo['lang']['please_check_details'] . " Database Error";
					}
				}else{
					$error_code = 6;
					$error_message = $wo['lang']['please_check_details'] . " Regarding Media";
				}
			} else {
                $error_code = 7;
				$error_message = $error_icon . $wo['lang']['please_check_details'];
			}
		}
	}
	if ($_POST['type'] == 'save_challenge_as_draft') {
		$sqlConnect;
		if ($wo['config']['who_upload'] == 'pro' && $wo['user']['is_pro'] == 0 && !Wo_IsAdmin() && !empty($_FILES['image'])) {
			$data['message'] = $error_icon . $wo['lang']['free_plan_upload_pro'];
		} else {
			if (!empty($_POST['title'])  && !empty($_POST['description'])) {

				// Tagging code will be used for challenge not for campgian sameer
				$post_text = "";
				$mention_regex = '/@([A-Za-z0-9_]+)/i';

				$mention_regex = '/@([A-Za-z0-9_]+)/i';
				if(empty($_POST['postText'])){
					
				}else{
					$post_text = Wo_Secure($_POST['postText']);
					preg_match_all($mention_regex, $post_text, $matches);
					foreach ($matches[1] as $match) {
						$match         = Wo_Secure($match);
						$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
						$match_search  = '@' . $match;
						$match_replace = '@[' . $match_user['user_id'] . ']';
						if (isset($match_user['user_id'])) {
							$post_text  = str_replace($match_search, $match_replace, $post_text);
							$mentions[] = $match_user['user_id'];
						}
					}
					if(sizeof($mentions) < 3){
						$response_data['api_status'] = 400;
						$error_message =$error_icon."Tag at least 3 Friends"."<br>";						
						$error_code  = 1;
                        $response_data['error'] = array('error_id' => $error_code,'error_message' => $error_message);
						echo json_encode($response_data);	
						exit();
					}
				}
				// Tagging code end;
				$media = array();
				if (!empty($_FILES['image'])) {
					$fileInfo      = array(
						'file' => $_FILES["image"]["tmp_name"],
						'name' => $_FILES['image']['name'],
						'size' => $_FILES["image"]["size"],
						'type' => $_FILES["image"]["type"],
						'types' => 'jpeg,jpg,png,bmp'
					);
					$media  = Wo_ShareFile($fileInfo);
				}
				if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
					if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
						$invalid_file = 1;
					} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
						$invalid_file = 2;
					} else {
						$fileInfo = array(
							'file' => $_FILES["postVideo"]["tmp_name"],
							'name' => $_FILES['postVideo']['name'],
							'size' => $_FILES["postVideo"]["size"],
							'type' => $_FILES["postVideo"]["type"],
							'types' => 'mp4,m4v,webm,flv,mov,mpeg'
						);
						$media    = Wo_ShareFile($fileInfo);
						if (!empty($media)) {
							$mediaFilename = $media['filename'];
							$mediaName     = $media['name'];
							$img_types     = array(
								'image/png',
								'image/jpeg',
								'image/jpg',
								'image/gif'
							);
							if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
								$fileInfo = array(
									'file' => $_FILES["video_thumb"]["tmp_name"],
									'name' => $_FILES['video_thumb']['name'],
									'size' => $_FILES["video_thumb"]["size"],
									'type' => $_FILES["video_thumb"]["type"],
									'types' => 'jpeg,png,jpg,gif',
									'crop' => array(
										'width' => 525,
										'height' => 295
									)
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$video_thumb = $media['filename'];
								}
							}
						}
					}
				}
				if (isset($_FILES['postMusic']['name']) && empty($mediaFilename)) {
					if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
						$invalid_file = 1;
					} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
						$invalid_file = 2;
					} else {
						$fileInfo = array(
							'file' => $_FILES["postMusic"]["tmp_name"],
							'name' => $_FILES['postMusic']['name'],
							'size' => $_FILES["postMusic"]["size"],
							'type' => $_FILES["postMusic"]["type"],
							'types' => 'mp3,wav'
						);
						$media    = Wo_ShareFile($fileInfo);
						if (!empty($media)) {
							$mediaFilename = $media['filename'];
							$mediaName     = $media['name'];
						}
					}
				}
				if(isset($_POST['challenge_fund_id']) && !empty($_POST['challenge_fund_id'])){
					$challenge_fund_id = $_POST['challenge_fund_id'];
					$challenge_fund = $db->where('id',$challenge_fund_id)->getOne(T_CHALLENGE_FUNDING);
					$challenge_fund->description = Wo_Secure($_POST['description']);
					$challenge_fund->invites = Wo_Secure($post_text);
					$challenge_fund->time = time();
					if(!empty($media)){
						$challenge_fund->file =  $media['file'];							
						$challenge_fund->fileName = $media['fileName'];
					}
					$challenge_fund->status = 2;
					$challenge_fund = json_decode(json_encode($challenge_fund));
					$result = $db->update(T_CHALLENGE_FUNDING, $challenge_fund);
					if(!$result){
						$challenge_fund_id = false;
					}
				}else{
					$insert_array = array(
						'funding_id' => Wo_Secure($_POST['funding_id']),
						'challenge_id' => Wo_Secure($challenge_id),
						'invited_by' => 0,
						'description'   => Wo_Secure($_POST['description']),
						'invites' => Wo_Secure($post_text),
						'time'   => time(),
						'file' => $media['file'],
						'fileName' => $media['fileName'],
						'user_id'  => $wo['user']['id'],
						'status' => 2,
						'hashed_id' => Wo_GenerateKey(15, 15)
					);
					$challenge_fund_id = $db->insert(T_CHALLENGE_FUNDING, $insert_array);

				}
					// 'postRecord' => Wo_Secure($_POST['postRecord']),
				if(!empty($challenge_fund_id)){
					$response_data['status'] = 200;
					$response_data['message'] = $wo['lang']['challenge_created'];
				
				}else{
					$error_code = 2;
					$error_message = $error_icon . $wo['lang']['please_check_details'] . "Database Error";
				}
				
			} else {
                $error_code = 3;
				$error_message = $wo['lang']['please_check_details'];
			}
		}
	}
    // method get parameter id =  challenge fund id
    if ($_POST['type'] == 'delete_challenge') {
		if (!empty($_POST['id'])) {
			$id = Wo_Secure($_POST['id']);
			$challenge_fund = $db->where('id', $id)->getOne(T_CHALLENGE_FUNDING);
			if (!empty($challenge_fund) || ($wo['user']['user_id'] != $challenge_fund->user_id && Wo_IsAdmin() == false)) {

				@Wo_DeleteFromToS3($fund->file);

				if (file_exists($fund->file)) {
					try {
						unlink($fund->file);
					} catch (Exception $e) {
					}
				}

				$db->where('id', $id)->delete(T_CHALLENGE_FUNDING);
				$posts = $db->where('challenge_fund_id', $id)->get(T_POSTS);
				if (!empty($posts)) {
					foreach ($posts as $key => $value) {
						$db->where('parent_id', $value->id)->delete(T_POSTS);
					}
				}
				$db->where('challenge_fund_id', $id)->delete(T_POSTS);
				$response_data['api_status'] = 200;
				$response_data['message'] = 'Challenge fund deleted succesfuly!';
			} else {
                $error_code = 1;
				$error_message =  $wo['lang']['please_check_details'];
			}
		} else {
            $error_code = 2;
			$error_message = $wo['lang']['please_check_details'];
		}
	}
    // method get parameter id =  challenge fund id
	if ($_POST['type'] == 'delete_draft_challenge') {
		if (!empty($_POST['id'])) {
			$id = Wo_Secure($_POST['id']);
			$fund = $db->where('id', $id)->getOne(T_CHALLENGE_FUNDING);
			if (!empty($fund) || ($wo['user']['user_id'] != $fund->user_id && Wo_IsAdmin() == false)) {

				@Wo_DeleteFromToS3($fund->file);

				if (file_exists($fund->file)) {
					try {
						unlink($fund->file);
					} catch (Exception $e) {
					}
				}

				$db->where('id', $id)->delete(T_CHALLENGE_FUNDING);
				$posts = $db->where('challenge_fund_id', $id)->get(T_POSTS);
				if (!empty($posts)) {
					foreach ($posts as $key => $value) {
						$db->where('parent_id', $value->id)->delete(T_POSTS);
					}
				}

				$db->where('challenge_fund_id', $id)->delete(T_POSTS);

				$response_data['api_status'] = 200;
				$response_data['message'] = 'Challenge fund deleted succesfully!';
			} else {
                $error_code = 1;
				$error_message = $error_icon . $wo['lang']['please_check_details'];
			}
		} else {
            $error_code = 2;
			$error_message = $error_icon . $wo['lang']['please_check_details'];
		}
	}

    if ($_POST['type'] == 'delete') {

        if (!empty($_POST['id'])) {
            $id = Wo_Secure($_POST['id']);
            $fund = $db->where('id',$id)->getOne(T_FUNDING);
            if (!empty($fund) || ($wo['user']['user_id'] != $fund->user_id && Wo_IsAdmin() == false)) {

                @Wo_DeleteFromToS3($fund->image);

                if (file_exists($fund->image)) {
                    try {
                        unlink($fund->image);   
                    }
                    catch (Exception $e) {
                    }
                }

                $db->where('id',$id)->delete(T_FUNDING);
                $raise = $db->where('funding_id',$id)->get(T_FUNDING_RAISE);
                $db->where('funding_id',$id)->delete(T_FUNDING_RAISE);
                $posts = $db->where('fund_id',$id)->get(T_POSTS);
                if (!empty($posts)) {
                    foreach ($posts as $key => $value) {
                        $db->where('parent_id',$value->id)->delete(T_POSTS);
                    }
                }
                    
                $db->where('fund_id',$id)->delete(T_POSTS);
                foreach ($raise as $key => $value) {
                    $raise_posts = $db->where('fund_raise_id',$value->id)->get(T_POSTS);
                    if (!empty($raise_posts)) {
                        foreach ($posts as $key => $value1) {
                            $db->where('parent_id',$value1->id)->delete(T_POSTS);
                        }
                    }
                    $db->where('fund_raise_id',$value->id)->delete(T_POSTS);
                }

                $response_data = array(
                                    'api_status' => 200,
                                    'message' => 'funding deleted'
                                );
            }
            else{
                $error_code    = 7;
                $error_message = 'please check details';
            }
        }
        else{
            $error_code    = 6;
            $error_message = 'id can not be empty';
        }
    }

    	// load user fund by sameer
        // parameter offset and user_id
        // offset = challenge fund id
	if ($_POST['type'] == 'load_user_challenge_fund') {

		if (!empty($_POST['offset']) && is_numeric($_POST['offset']) && $_POST['offset'] > 0) {
			$id = Wo_Secure($_POST['offset']);
			$user_id = $wo['user']['id'];
			if (!empty($_POST['user_id']) && is_numeric($_POST['user_id']) && $_POST['user_id'] > 0) {
				$user_id = Wo_Secure($_POST['user_id']);
			}

			$funding = GetChallengesByUserId($user_id, 9, $id);

			$html = '';
			if (!empty($funding)) {
				foreach ($funding as $key => $wo['challenge_fund']) {
					$html .= Wo_LoadPage('my_challenges/list');
				}
			}
			$response_data['api_status'] = 200;
			$response_data['challenges'] = (array)$funding;
		}
	}
	if ($_POST['type'] == 'load_fund') {

		if (!empty($_POST['offset']) && is_numeric($_POST['offset']) && $_POST['offset'] > 0) {
			$id = Wo_Secure($_POST['offset']);

			$funding = GetFunding(10, $id);

			$html = '';
			if (!empty($funding)) {
				foreach ($funding as $key => $wo['fund']) {
					$html .= Wo_LoadPage('funding/list');
				}
			}
			$response_data['api_status'] = 200;
			$response_data['html'] = $html;
		}
	}

    	// function by sameer
	if ($_POST['type'] == 'load_challenge_fund') {

		if (!empty($_POST['offset']) && is_numeric($_POST['offset']) && $_POST['offset'] > 0) {
			$id = Wo_Secure($_POST['offset']);

			$funding = GetChallenges(10, $id);

			$html = '';
			if (!empty($funding)) {
				foreach ($funding as $key => $wo['challenge_fund']) {
					$html .= Wo_LoadPage('challenges/list');
				}
			}
			$response_data['api_status'] = 200;
			$response_data['html'] = $html;
			$response_data['challenges'] = (Array)$funding;
		}
	}
	//get funding details by sameer
	if ($_POST['type'] == 'get_funding_by_id') {

		if (!empty($_POST['campaign_id']) && is_numeric($_POST['campaign_id']) && $_POST['campaign_id'] > 0) {
			$id = Wo_Secure($_POST['campaign_id']);
			
			
			$fund = GetFundingById($id);
			$html = '';
			if (!empty($fund)) {
	
					
					$beneficiary =   Wo_Markup($fund['beneficiary'],false,false,true,0);
					$html.=
					'
						<input type="hidden" name="fund_campaign_id" value="'.$fund['id'].'" />
						<div class="form-group row text-center">
							<div class="col-md-12 text-center">
								<h6 class="text-center">You have chosen to support the following campaign</h6>
							</div>
							<div class="col-md-12 text-center">
								<label class="control-label">'.$fund['title'].'</label>
								<img src="'.$fund['image'].'" style="width:100%;height:300px;">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label class="control-label">Organizer</label>
								<br>
								<div class="row pull-bottom">
									<div class="inner-circle">
										<i class="icon-circle-blank fa-2x">  R</i>
									</div>
									<div class="postMeta--author-text p-3">
										<span class="user-popover" data-id="'.$fund['user_data']['id'].'" data-type="user">
											<a target="_blank" href="'.$fund['user_data']['url'].'" > '.$fund['user_data']['name'] .'</a>
										</span>
									</div>
								</div>
								
							</div>
							<div class="col-md-6">
								<label class="control-label">Beneficiary</label>
								<br>
								<div class="postMeta--author-text">
									<i class="fa fa-smile-o fa-3x" aria-hidden="true"></i>
									'.$beneficiary.'
								</div>
							</div>
						</div>
					';				
					
					$response_data['message'] = "Data Found";
					$response_data['api_status'] = 200;
					$response_data['html'] = $html;
					$response_data['fund'] = (Array)$fund;
			}else{
                $error_code =1;
                $error_message = "No Data Found";
			}

		}
	}
	// sameer get all challenges get
	if ($_POST['type'] == 'get_all_challenges') {

		$challenges = getAllChallenges();
		$titles = array();	
		foreach($challenges as $challenge){
			$titles[] = array(
				'value'=>$challenge->title,
				'id'=>$challenge->id
			);
		}
					
		$response_data['message'] = "Data Found";
		$response_data['api_status'] = 200;
		$response_data['challenges'] = Array($challenges);
		$response_data['title'] = $titles;
		
	}
	

    if ($_POST['type'] == 'funding') {
        $funding = GetFunding($limit,$offset);
        foreach ($funding as $key => $value) {
            if (!empty($value['user_data'])) {
                foreach ($non_allowed as $key4 => $value4) {
                  unset($funding[$key]['user_data'][$value4]);
                }
            }
            else{
                $funding[$key]['user_data'] = null;
            }
        }
        $response_data = array(
                            'api_status' => 200,
                            'data' => $funding
                        );

    }

    if ($_POST['type'] == 'user_funding') {
        $user_id = $wo['user']['id'];
        if (!empty($_POST['user_id']) && is_numeric($_POST['user_id']) && $_POST['user_id'] > 0) {
            $user_id = Wo_Secure($_POST['user_id']);
        }

        $funding   = GetFundingByUserId($user_id,$limit,$offset); 

        foreach ($funding as $key => $value) {
            if (!empty($value['user_data'])) {
                foreach ($non_allowed as $key4 => $value4) {
                  unset($funding[$key]['user_data'][$value4]);
                }
            }
            else{
                $funding[$key]['user_data'] = null;
            }
        }
        $response_data = array(
                            'api_status' => 200,
                            'data' => $funding
                        );

    }

    if ($_POST['type'] == 'pay') {
        $user_id = $wo['user']['id'];
        if (!empty($_POST['amount']) && is_numeric($_POST['amount']) && $_POST['amount'] > 0 && !empty($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0) {
            $amount = Wo_Secure($_POST['amount']);
            $fund_id = Wo_Secure($_POST['id']);
            $fund = $db->where('id',$fund_id)->getOne(T_FUNDING);
            if (!empty($fund)) {


                $notes = "Donated to ".mb_substr($fund->title, 0, 100, "UTF-8");

                $create_payment_log = mysqli_query($sqlConnect, "INSERT INTO " . T_PAYMENT_TRANSACTIONS . " (`userid`, `kind`, `amount`, `notes`) VALUES ({$wo['user']['user_id']}, 'DONATE', {$amount}, '{$notes}')");

                $admin_com = 0;
                if (!empty($wo['config']['donate_percentage']) && is_numeric($wo['config']['donate_percentage']) && $wo['config']['donate_percentage'] > 0) {
                    $admin_com = ($wo['config']['donate_percentage'] * $amount) / 100;
                    $amount = $amount - $admin_com;
                }
                $user_data = Wo_UserData($fund->user_id);
                $db->where('user_id',$fund->user_id)->update(T_USERS,array('balance' => $user_data['balance'] + $amount));
                $fund_raise_id = $db->insert(T_FUNDING_RAISE,array('user_id' => $wo['user']['user_id'],
                                                  'funding_id' => $fund_id,
                                                  'amount' => $amount,
                                                  'time' => time()));
                $post_data = array(
                    'user_id' => Wo_Secure($wo['user']['user_id']),
                    'fund_raise_id' => $fund_raise_id,
                    'time' => time(),
                    'multi_image_post' => 0
                );

                $id = Wo_RegisterPost($post_data);

                $notification_data_array = array(
                    'recipient_id' => $fund->user_id,
                    'type' => 'fund_donate',
                    'url' => 'index.php?link1=show_fund&id=' . $fund->hashed_id
                );
                Wo_RegisterNotification($notification_data_array);
                $response_data = array(
                                    'api_status' => 200,
                                    'message' => 'donated'
                                );
            }
            else{
                $error_code    = 7;
                $error_message = 'fund not found';
            }
        }
        else{
            $error_code    = 6;
            $error_message = 'amount,id can not be empty';
        }
    }

}
else{
    $error_code    = 4;
    $error_message = 'type can not be empty';
}