<?php
if ($wo['loggedin'] == false) {
    header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
    exit();
}
if (Wo_IsUserComplete($wo['user']['user_id']) === false) {
	header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
    exit();
}
//echo "<pre>";print_r($wo['user']);die;
if ($wo['user']['startup_image'] == 0) {
	$page = 'avatar_startup';
} else if ($wo['user']['start_up_info'] == 0) {
	$page = 'info_startup';
//} else if ($wo['user']['startup_about'] == 0) {
	//$page = 'startup_about';
} else if ($wo['user']['startup_follow'] == 0) {
	$wo['users'] = Wo_GetFemusUsers();
	//echo "<pre>";print_r($wo['users'] );die;
	/*if ($wo['config']['connectivitySystem'] == 1 || empty($wo['users'])) {
		Wo_UpdateUserData($wo['user']['user_id'], array('startup_follow' => 1));
		header("Location: " . Wo_SeoLink('index.php?link1=start-up'));
        exit();
	}*/
	$page = 'follow_startup';
} else if ($wo['user']['startup_thankyou'] == 0) {
	Wo_UpdateUserData($wo['user']['user_id'], array('start_up' => 1, 'startup_thankyou' => 1));
	$page = 'startup_thankyou';
} else {
	header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
    exit();
}
$wo['description'] = $wo['config']['siteDesc'];
$wo['keywords']    = $wo['config']['siteKeywords'];
$wo['page']        = 'start_up';

$wo['title']       = $wo['lang']['lets_go'];
$wo['content']     = Wo_LoadPage('start_up/' . $page);