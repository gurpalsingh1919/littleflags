<?php
// if ($wo['loggedin'] == false || $wo['config']['funding_system'] != 1) {
//   header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
//   exit();
// }

if (is_numeric($_GET['id']) && $_GET['id'] > 0) {
  if(isset($_GET['type'])){
    $wo['fund'] = GetPreviewDraftFundingById($_GET['id']);
     //$wo['fund']['updates']=getCampaignUpdates($_GET['id']);
  }else{
    $wo['fund'] = GetFundingById($_GET['id']);
    //$wo['fund']['updates']=getCampaignUpdates($_GET['id']);
  }
}
else{
  if(isset($_GET['type'])){
    $wo['fund'] = GetPreviewDraftFundingById($_GET['id'],'hash');
    //$wo['fund']['updates']=getCampaignUpdates($_GET['id'],'hash');
  }else{
    $wo['fund'] = GetFundingById($_GET['id'],'hash');
    //$wo['fund']['updates']=getCampaignUpdates($_GET['id'],'hash');
  }
}
//echo "<pre>";print_r($_GET); print_r($wo['fund']);die;
if (!empty($_GET['ref'])) {
    if ($_GET['ref'] == 'se' && !empty($wo['fund']['id'])) {
        $regsiter_recent = Wo_RegsiterRecent($wo['fund']['id'], 'campaign');
    }
}

$wo['description'] = $wo['config']['siteDesc'];
$wo['keywords']    = $wo['config']['siteKeywords'];
$wo['page']        = 'show_fund';
$wo['title']       = $wo['config']['siteTitle'];
//Wo_notifyOrganizer($wo['fund']['id'],1);
//Wo_checkRaisedStatus($wo['fund']['id']);die;
$user_id=$wo['user']['id'];
$isOrganiser = ($user_id == $wo['fund']['user_id']);
$isCampaignAdmin=false;
if(!empty($wo['fund']['funding_admins']))
{
  $isCampaignAdmin=Wo_IsCampaingnAdmin($wo['fund']['funding_admins']);
}
//var_dump($isOrganiser);
//var_dump($isCampaignAdmin);
//die;
$status=array('0','4','8');
if(in_array($wo['fund']['status'], $status) && !Wo_IsAdmin($user_id) && !$isOrganiser && !$isCampaignAdmin)
{
  header("Location: " . Wo_SeoLink('index.php?link1=funding'));
  exit();
}
if(isset($_GET['type'])){
  $wo['content']     = Wo_LoadPage('show_fund/content_draft');
}
else if(isset($_GET['export_donors']))
{
  //echo $_GET['export_donors'];die;
    $array=Wo_ExportDonors($wo['fund']['id']);
    if(count($array) >0)
    {
      download_send_headers("campaign-donors-" . $wo['fund']['id'] . ".csv");
      echo array2csv($array);
      die();  
    }
    else
    {
      $link=Wo_SeoLink('index.php?link1=show_fund&id='.$wo['fund']['hashed_id']);
      header("Location: ". $link);
      exit();
    }
    

    //$wo['content']     = Wo_LoadPage('show_fund/content_draft');
}
else if(!empty($_GET['invite']))
{
  if($wo['fund']['user_id']==$wo['user']['user_id'])
  {
    $wo['content']     = Wo_LoadPage('show_fund/invite-fundraiser');
  }
  else
  {   
      $link=Wo_SeoLink('index.php?link1=show_fund&id='.$wo['fund']['hashed_id']);
      header("Location: ". $link);
      exit();
  }

  
}
else if(!empty($_GET['analytics']))
{
  if($wo['fund']['user_id']==$wo['user']['user_id'])
  {
    $wo['content']     = Wo_LoadPage('show_fund/campaing-analysis');
  }
  else
  {   
      $link=Wo_SeoLink('index.php?link1=show_fund&id='.$wo['fund']['hashed_id']);
      header("Location: ". $link);
      exit();
  }

  
}
else if(!empty($_GET['notify']))
{
  $notify_id=$_GET['notify'];
  $wo['notify']=getNotificationDetails($notify_id);
  if(!empty($wo['notify']) && $wo['notify']->recipient_id==$wo['user']['user_id'])
  {
    $wo['content']     = Wo_LoadPage('show_fund/notify-message');
  }
  else
  {   
      $link=Wo_SeoLink('index.php?link1=show_fund&id='.$wo['fund']['hashed_id']);
      header("Location: ". $link);
      exit();
  }
}
else
{
  $wo['content']     = Wo_LoadPage('show_fund/content');
}
