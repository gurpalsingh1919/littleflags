<?php
if ($wo['loggedin'] == false || $wo['config']['funding_system'] != 1) {
  header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
  exit();
}
//echo "<pre>";print_r($_GET);die;
if(!isset($_GET['type'])){
  
    if (is_numeric($_GET['id']) && $_GET['id'] > 0) {
      $wo['fund'] = GetFundingById($_GET['id']);
    }
    else{
      $wo['fund'] = GetFundingById($_GET['id'],'hash');
    }

}else{
  
    if (is_numeric($_GET['id']) && $_GET['id'] > 0) {
      $wo['fund'] = GetDraftFundingById($_GET['id']);
    }
    else{
      $wo['fund'] = GetDraftFundingById($_GET['id'],'hash');
    }
}
$isAdministrator = false;
if(!empty($wo['fund']['funding_admins'])){
  $administrators = Wo_GetTaggedUsers($wo['fund']['funding_admins']);
  if(!empty($administrators)){
    foreach($administrators as $admin){
      if($wo['user']['id']  == $admin['id']){
        $isAdministrator = true;
      }
    }
  }
}
if (empty($wo['fund']) || ($wo['user']['user_id'] != $wo['fund']['user_id'] && Wo_IsAdmin() == false  &&  $isAdministrator == false)) {
	header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
  exit(); 
}
//echo "<pre>";print_r($wo['fund']);die;
$wo['description'] = $wo['config']['siteDesc'];
$wo['keywords']    = $wo['config']['siteKeywords'];
$wo['page']        = 'edit_fund';
$wo['title']       = $wo['config']['siteTitle'];
$wo['content']     = Wo_LoadPage('edit_fund/content');