<?php
if ($wo['loggedin'] == false || $wo['config']['funding_system'] != 1) {
  header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
  exit();
}


if (isset($_GET['id']) && $_GET['id'] !='') 
{
  
    $wo['fund'] = Wo_GetChallengeFundingById($_GET['id'],'hash');
}
else
{
    header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
    exit();
}

//echo "<pre>";print_r($wo['fund']);die;
$wo['description'] = $wo['config']['siteDesc'];
$wo['keywords']    = $wo['config']['siteKeywords'];
$wo['page']        = 'thank_you_challenge';
$wo['title']       = $wo['config']['siteTitle'];
$wo['content']     = Wo_LoadPage('create_challenge/thankyou_challenge');

