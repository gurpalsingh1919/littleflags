<?php
// Paypal methods
use PayPal\Api\Payer;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Details;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;

header("Content-type: application/json");

// Updated by Sameer
if ($f == 'funding' && $wo['config']['funding_system'] == 1) 
{

	$data['status'] = 400;
	if ($s == 'insert_funding') 
	{
		//echo "<pre>";print_r($_POST);die;
		if ($wo['config']['who_upload'] == 'pro' && $wo['user']['is_pro'] == 0 && !Wo_IsAdmin() && !empty($_FILES['image'])) 
		{
			$data['message'] = $error_icon . $wo['lang']['free_plan_upload_pro'];
		} 
		else 
		{
			if (!empty($_POST['title'])  && !empty($_POST['description']) && !empty($_FILES['image']) && !empty($_POST['amount']) && is_numeric($_POST['amount']) && $_POST['amount']  > 0 ) 
			{
				$flag='0';

				
				$post_text = "";
				$beneficiary = "";
				$fund_id = "";
				$funding_admins = "";
				$page_id='';
				$mention_regex = '/@([A-Za-z0-9_]+)/i';
				$fundraising_for=$_POST['fundraising_for'];
				$falgs=array();
				$bank_name='';
				$bank_account_name='';
				$bank_account_no='';

				if($fundraising_for=='2')
				{
					$beneficiary = Wo_Secure($_POST['beneficiary2']);
				}
				else
				{
					if(empty($_POST['bank_name']))
					{
						$flag ='1';
						$message = $error_icon . $wo['lang']['bank_name_required'];
					}
					if(empty($_POST['bank_account_name']))
					{
						$flag ='1';
						$message = $error_icon . $wo['lang']['bank_account_name_required'];
					}
					if(empty($_POST['bank_account_no']))
					{
						$flag ='1';
						$message = $error_icon . $wo['lang']['bank_account_no_required'];
					}
					if($flag=='1')
					{
						$data['status'] = 400;
						$data['message'] = $message;
						echo json_encode($data);
						exit();
					}
					$bank_name=$_POST['bank_name'];
					$bank_account_name=$_POST['bank_account_name'];
					$bank_account_no=$_POST['bank_account_no'];

					$beneficiary = Wo_Secure($_POST['beneficiary1']);
				}
				if (empty($beneficiary)) 
				{
					$data['status'] = 400;
					$message = $error_icon . $wo['lang']['beneficiary_is_required'] . "<br>";
					$data['message'] = $message;
					echo json_encode($data);
					exit();
				} 
				else 
				{
					//$beneficiary = Wo_Secure($_POST['beneficiary']);
					//$fundraising_for=$_POST['fundraising_for'];
					
					if($fundraising_for=='2')
					{
						preg_match_all($mention_regex, $beneficiary, $matches);
						foreach ($matches[1] as $match) {
							$match         = Wo_Secure($match);
							$match_pages    = Wo_PageData(Wo_PageIdFromPagename($match));
							$match_search  = '@' . $match;
							$match_replace = '@[' . $match_pages['page_id'] . ']';
							if (isset($match_pages['page_id'])) 
							{
								$beneficiary  = str_replace($match_search, $match_replace, $beneficiary);
								$page_id=$match_pages['page_id'];
								$mentions[] = $match_pages['page_id'];
								$special_flag=array('1','2','3','4');
								if(in_array($match_pages['page_id'], $special_flag))
								{
									$falgs['flags_id']=$match_pages['page_id'];
								}
								else
								{
									$falgs['flags_id']='5';
								}
							}
						}
					}
					else
					{
						preg_match_all($mention_regex, $beneficiary, $matches);
						foreach ($matches[1] as $match) {
							$match         = Wo_Secure($match);
							$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
							$match_search  = '@' . $match;
							$match_replace = '@[' . $match_user['user_id'] . ']';
							if (isset($match_user['user_id'])) {
								$beneficiary  = str_replace($match_search, $match_replace, $beneficiary);
								$mentions[] = $match_user['user_id'];
							}
						}
						$falgs['flags_id']='5';
					}
					
					$data['beneficiary'] =sizeof($mentions);
					if (sizeof($mentions) != 1) {
						$data['status'] = 400;
						$message = $error_icon . $wo['lang']['only_one_beneficiary'] . "<br>";
						$data['message'] = $message;
						$data['beneficiary'] =sizeof($mentions);
						echo json_encode($data);
						exit();
					}
				}
				if(isset($_POST['funding_admins']) && !empty($_POST['funding_admins'])){	
					$funding_admins = Wo_Secure($_POST['funding_admins']);
					preg_match_all($mention_regex, $funding_admins, $matches);
					foreach ($matches[1] as $match) {
						$match         = Wo_Secure($match);
						$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
						$match_search  = '@' . $match;
						$match_replace = '@[' . $match_user['user_id'] . ']';
						if (isset($match_user['user_id'])) {
							$funding_admins  = str_replace($match_search, $match_replace, $funding_admins);
							$fundingAdmins[] = $match_user['user_id'];
						}
					}
				}
				$categories=array();
				if (isset($_POST['postTextCategory']) && !empty($_POST['postTextCategory'])) 
				{
					$categories=$_POST['postTextCategory'];
				}

				// Tagging code end;
				$media = array();
				if (!empty($_FILES['image'])) {
					$fileInfo      = array(
						'file' => $_FILES["image"]["tmp_name"],
						'name' => $_FILES['image']['name'],
						'size' => $_FILES["image"]["size"],
						'type' => $_FILES["image"]["type"],
						'types' => 'jpeg,jpg,png,bmp'
					);
					$media  = Wo_ShareFile($fileInfo);
				}

				$insert_array = array(
					'title' => Wo_Secure($_POST['title']),
					'description'   => $_POST['description'],
					'fundraising_for'   => $fundraising_for,
					'amount'   => Wo_Secure($_POST['amount']),
					'time'   => time(),
					'image' => $media['filename'],
					'video' => "",
					'beneficiary' => $beneficiary,
					'funding_admins' => $funding_admins,
					'user_id'  => $wo['user']['id'],
					'hashed_id' => Wo_GenerateKey(15, 15),
					'category'=>json_encode($categories),
					'status' => '0',
					'bank_name'   => $bank_name,
					'bank_account_name'   => $bank_account_name,
					'bank_account_no'   => $bank_account_no
				);

				$due_date = Wo_Secure($_POST['due_date']);
				if ($due_date) {
					$insert_array['due_date'] = $due_date;
				}

				//echo "<pre>";print_r($insert_array);die;
				if (!empty($media) && !empty($media['filename'])) 
				{
					// $data['message'] = $insert_array;
					// echo json_encode($data);
					// exit();
					$fund_id = $db->insert(T_FUNDING, $insert_array);

					if (!empty($fund_id)) 
					{
						Wo_NewCampaignPublish($fund_id);
						$falgs['campaign_id']=$fund_id;
						$falgs['time']=time();
						$db->insert(T_CAMPAIGN_FLAGS, $falgs);
						$mediaFilename = '';
						$post_photo    = '';
						$mediaName     = '';
						$video_thumb   = '';
						$album_name = '';
						$blur = 0;

						if (!empty($_POST['album_name'])) {
							$album_name = $_POST['album_name'];
						}
						if (!isset($_FILES['postPhotos']['name'])) {
							$album_name = '';
						}
						$multi = 0;

						if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) 
						{
							$multi = 1;
							$data['multi'] = $multi;
						}
						if (isset($_FILES['postPhotos']['name'])) {
							$allowed = array(
								'gif',
								'png',
								'jpg',
								'jpeg'
							);
							for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
								$new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
								if (!in_array(strtolower($new_string['extension']), $allowed)) {
									$errors[] = $error_icon . $wo['lang']['please_check_details'];
								}
							}
						}
						if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
							$headers = get_headers($_POST['postSticker'], 1);
							if (strpos($headers['Content-Type'], 'image/') !== false) {
								$post_data['postSticker'] = $_POST['postSticker'];
							} else {
								$invalid_file = 2;
							}
						} else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
							if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
								$post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
							}
						}

						if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
							if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postVideo"]["tmp_name"],
									'name' => $_FILES['postVideo']['name'],
									'size' => $_FILES["postVideo"]["size"],
									'type' => $_FILES["postVideo"]["type"],
									'types' => 'mp4,m4v,webm,flv,mov,mpeg'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilename = $media['filename'];
									$mediaName     = $media['name'];
									$img_types     = array(
										'image/png',
										'image/jpeg',
										'image/jpg',
										'image/gif'
									);
									if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
										$fileInfo = array(
											'file' => $_FILES["video_thumb"]["tmp_name"],
											'name' => $_FILES['video_thumb']['name'],
											'size' => $_FILES["video_thumb"]["size"],
											'type' => $_FILES["video_thumb"]["type"],
											'types' => 'jpeg,png,jpg,gif',
											'crop' => array(
												'width' => 525,
												'height' => 295
											)
										);
										$media    = Wo_ShareFile($fileInfo);
										if (!empty($media)) {
											$video_thumb = $media['filename'];
										}
									}
								}
							}
						}
						if (isset($_FILES['postMusic']['name'])) {
							if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postMusic"]["tmp_name"],
									'name' => $_FILES['postMusic']['name'],
									'size' => $_FILES["postMusic"]["size"],
									'type' => $_FILES["postMusic"]["type"],
									'types' => 'mp3,wav'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilenameMusic = $media['filename'];
									$mediaNameMusic     = $media['name'];
								}
							}
						}
						$post_data = array(
							'user_id' => Wo_Secure($wo['user']['user_id']),
							'fund_id' => $fund_id,
							//'page_id'=>$page_id,
							'time' => time(),
							'postRecord' => Wo_Secure($_POST['postRecord']),
							'multi_image' => Wo_Secure($multi),
							'multi_image_post' => 0,
							'postText' => Wo_Secure($post_text),
							'album_name' => Wo_Secure($album_name),
							'postFile' => Wo_Secure($mediaFilename, 0),
							'postFileName' => Wo_Secure($mediaName),
							'postFileThumb' => Wo_Secure($video_thumb),
							'blur' => $blur,
							'postPrivacy' => 0,
							'active' =>'0'
						);

						$id = Wo_RegisterPost($post_data);
						$db->where('id',$id)->update(T_POSTS, array('page_id' =>$page_id));
						unset($post_data['id']);
						unset($post_data['post_id']);
						unset($post_data['parent_id']);
						if(isset($_FILES['postMusic']['name']) && $id && isset($mediaFilenameMusic) && isset($mediaNameMusic)  ){
							$post_data['multi_image'] = 0;
							$post_data['postFile'] = Wo_Secure($mediaFilenameMusic, 0);
							$post_data['postFileName'] = Wo_Secure($mediaNameMusic);
							$post_data['is_music'] = 1;
							Wo_RegisterPost($post_data);
							unset($post_data['is_music']);
						}
						if (isset($mentions) && is_array($mentions) && $fundraising_for !='2') {
							foreach ($mentions as $mention) {
								if (empty($wo['no_mention']) || (!empty($wo['no_mention']) && !in_array($mention, $wo['no_mention']))) {
									$notification_data_array = array(
										'recipient_id' => $mention,
										'type' => 'post_mention',
										'page_id' => 0,
										'post_id' => $id,
										'url' => 'index.php?link1=show_fund&id=' . $insert_array['hashed_id']
									);
									Wo_RegisterNotification($notification_data_array);
								}
							}
						}
						if (isset($_FILES['postPhotos']['name'])) {
							if (count($_FILES['postPhotos']['name']) > 0) {
								$data['imageUpload'] = 'photos > 0';

								for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
									$fileInfo = array(
										'file' => $_FILES["postPhotos"]["tmp_name"][$i],
										'name' => $_FILES['postPhotos']['name'][$i],
										'size' => $_FILES["postPhotos"]["size"][$i],
										'type' => $_FILES["postPhotos"]["type"][$i],
										'types' => 'jpg,png,jpeg,gif'
									);
									$file     = Wo_ShareFile($fileInfo, 1);
									$image_file = Wo_GetMedia($file['filename']);
									if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
										$blur = 1;
									} elseif ($wo['config']['adult_images'] == 1 && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
										$invalid_file = 3;
										$errors[] = $error_icon . $wo['lang']['adult_image_file'];
										Wo_DeletePost($id);
										@unlink($file['filename']);
										Wo_DeleteFromToS3($file['filename']);
									}
									if (!empty($file)) {
										$media_album = Wo_RegisterAlbumMedia($id, $file['filename']);
										$post_data['multi_image'] = 0;
										$post_data['multi_image_post'] = 1;
										$post_data['album_name'] = '';
										$post_data['postFile'] = $file['filename'];
										$post_data['postFileName'] = $file['name'];
										$post_data['active'] = 1;
										$new_id = Wo_RegisterPost($post_data);
										$media_album = Wo_RegisterAlbumMedia($new_id, $file['filename'], $id);
										$data['image-uploaded'.$i] = 'uploaded' . $new_id . "media_album" . $media_album;
									}
									$data['i-' . $i] = 'uploaded';
								}
							}
						}

						$data['status'] = 200;
						$data['message'] = $wo['lang']['funding_created'];
						//$data['url'] = Wo_SeoLink('index.php?link1=campaign_thankyou&id='. $insert_array['hashed_id']);
						$data['url'] = 'index.php?link1=campaign_thankyou&id=' . $insert_array['hashed_id'];
					} else {
						$data['status'] = 400;
						$message = $error_icon . $wo['lang']['please_check_details'] . "Database Error";
						$data['message'] = $message;
					}
				} 
				else 
				{
					$data['status'] = 400;
					$message = $error_icon . $wo['lang']['file_not_supported'] . "image" . "<br>";
					$data['message'] = $message;
				}
			} 
			else 
			{
				$data['post data'] = $_POST;
				$data['files'] = $_FILES;
				$data['message'] = $error_icon . $wo['lang']['please_check_details'];
			}
		}
	}

	if ($s == 'save_fund_as_draft') 
	{
		//echo "<pre>";print_r($_POST);die;
		if ($wo['config']['who_upload'] == 'pro' && $wo['user']['is_pro'] == 0 && !Wo_IsAdmin() && !empty($_FILES['image'])) 
		{
			$data['message'] = $error_icon . $wo['lang']['free_plan_upload_pro'];
		} 
		else 
		{
			if (!empty($_POST['title'])  && !empty($_POST['description']) && !empty($_FILES['image']) && !empty($_POST['amount']) && is_numeric($_POST['amount']) && $_POST['amount'] > 0) 
			{
				$flag='0';
				$bank_name='';
				$bank_account_name='';
				$bank_account_no='';
				$post_text = "";
				$beneficiary = "";
				$fund_id = "";
				$page_id='';
				$funding_admins = "";
				$mention_regex = '/@([A-Za-z0-9_]+)/i';
				$fundraising_for=$_POST['fundraising_for'];
				if($fundraising_for=='2')
				{
					$beneficiary = Wo_Secure($_POST['beneficiary2']);
				}
				else if($fundraising_for=='1')
				{
					if(empty($_POST['bank_name']))
					{
						$flag ='1';
						$message = $error_icon . $wo['lang']['bank_name_required'];
					}
					if(empty($_POST['bank_account_name']))
					{
						$flag ='1';
						$message = $error_icon . $wo['lang']['bank_account_name_required'];
					}
					if(empty($_POST['bank_account_no']))
					{
						$flag ='1';
						$message = $error_icon . $wo['lang']['bank_account_no_required'];
					}
					if($flag=='1')
					{
						$data['status'] = 400;
						$data['message'] = $message;
						echo json_encode($data);
						exit();
					}

					$bank_name=$_POST['bank_name'];
					$bank_account_name=$_POST['bank_account_name'];
					$bank_account_no=$_POST['bank_account_no'];

					$beneficiary = Wo_Secure($_POST['beneficiary1']);
				}
				if (empty($beneficiary)) {
					$data['status'] = 400;
					$message = $error_icon . "Beneficiary is required" . "<br>";
					$data['message'] = $message;
					echo json_encode($data);
					exit();
				} 
				else 
				{
					//$beneficiary = Wo_Secure($_POST['beneficiary']);
					//$fundraising_for=$_POST['fundraising_for'];
					if($fundraising_for=='2')
					{
						preg_match_all($mention_regex, $beneficiary, $matches);
						foreach ($matches[1] as $match) {
							$match         = Wo_Secure($match);
							$match_pages    = Wo_PageData(Wo_PageIdFromPagename($match));
							$match_search  = '@' . $match;
							$match_replace = '@[' . $match_pages['page_id'] . ']';
							if (isset($match_pages['page_id'])) {
								$beneficiary =str_replace($match_search, $match_replace, $beneficiary);
								$page_id=$match_pages['page_id'];
								$mentions[] = $match_pages['page_id'];

								$special_flag=array('1','2','3','4');
								if(in_array($match_pages['page_id'], $special_flag))
								{
									$falgs['flags_id']=$match_pages['page_id'];
								}
								else
								{
									$falgs['flags_id']='5';
								}

							}
						}
					}
					else
					{
						preg_match_all($mention_regex, $beneficiary, $matches);
						foreach ($matches[1] as $match) {
							$match         = Wo_Secure($match);
							$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
							$match_search  = '@' . $match;
							$match_replace = '@[' . $match_user['user_id'] . ']';
							if (isset($match_user['user_id'])) {
								$beneficiary  = str_replace($match_search, $match_replace, $beneficiary);
								$mentions[] = $match_user['user_id'];
							}
						}
						$falgs['flags_id']='5';
					}
					$data['beneficiary'] =sizeof($mentions);
					if (sizeof($mentions) != 1) {
						$data['status'] = 400;
						$message = $error_icon . "beneficiary must be 1 person " . "<br>";
						$data['message'] = $message;
						$data['beneficiary'] =sizeof($mentions);
						echo json_encode($data);
						exit();
					}
				}
				if(isset($_POST['funding_admins']) && !empty($_POST['funding_admins'])){	
					$funding_admins = Wo_Secure($_POST['funding_admins']);
					preg_match_all($mention_regex, $funding_admins, $matches);
					foreach ($matches[1] as $match) {
						$match         = Wo_Secure($match);
						$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
						$match_search  = '@' . $match;
						$match_replace = '@[' . $match_user['user_id'] . ']';
						if (isset($match_user['user_id'])) {
							$funding_admins  = str_replace($match_search, $match_replace, $funding_admins);
							$fundingAdmins[] = $match_user['user_id'];
						}
					}
				}

				if (isset($_POST['postTextCategory']) && !empty($_POST['postTextCategory'])) {
					$categories=$_POST['postTextCategory'];
				}

				// Tagging code end;
				$media = array();
				if (!empty($_FILES['image'])) {
					$fileInfo      = array(
						'file' => $_FILES["image"]["tmp_name"],
						'name' => $_FILES['image']['name'],
						'size' => $_FILES["image"]["size"],
						'type' => $_FILES["image"]["type"],
						'types' => 'jpeg,jpg,png,bmp'
					);
					$media  = Wo_ShareFile($fileInfo);
				}

				$insert_array = array(
					'title' => Wo_Secure($_POST['title']),
					'description'   => $_POST['description'],
					'fundraising_for'   => $fundraising_for,
					'amount'   => Wo_Secure($_POST['amount']),
					'time'   => time(),
					'image' => $media['filename'],
					'video' => "",
					'beneficiary' => $beneficiary,
					'funding_admins' => $funding_admins,
					'user_id'  => $wo['user']['id'],
					'hashed_id' => Wo_GenerateKey(15, 15),
					'category'=>json_encode($categories),
					'status'=>'4',
					'bank_name'   => $bank_name,
					'bank_account_name'   => $bank_account_name,
					'bank_account_no'   => $bank_account_no
				);
				
				$due_date = Wo_Secure($_POST['due_date']);
				if ($due_date) {
					$insert_array['due_date'] = $due_date;
				}

				
				if (!empty($media) && !empty($media['filename'])) 
				{
				
					$fund_id = $db->insert(T_FUNDING, $insert_array);
					//echo "<pre>";print_r($fund_id);die;
					if (!empty($fund_id)) 
					{
						$falgs['campaign_id']=$fund_id;
						$falgs['time']=time();
						$db->insert(T_CAMPAIGN_FLAGS, $falgs);
						
						$mediaFilename = '';
						$post_photo    = '';
						$mediaName     = '';
						$video_thumb   = '';
						$album_name = '';
						$blur = 0;

						if (!empty($_POST['album_name'])) {
							$album_name = $_POST['album_name'];
						}
						if (!isset($_FILES['postPhotos']['name'])) {
							$album_name = '';
						}
						$multi = 0;

						if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) 
						{
							$multi = 1;
							$data['multi'] = $multi;
						}
						if (isset($_FILES['postPhotos']['name'])) {
							$allowed = array(
								'gif',
								'png',
								'jpg',
								'jpeg'
							);
							for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
								$new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
								if (!in_array(strtolower($new_string['extension']), $allowed)) {
									$errors[] = $error_icon . $wo['lang']['please_check_details'];
								}
							}
						}
						if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
							$headers = get_headers($_POST['postSticker'], 1);
							if (strpos($headers['Content-Type'], 'image/') !== false) {
								$post_data['postSticker'] = $_POST['postSticker'];
							} else {
								$invalid_file = 2;
							}
						} else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
							if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
								$post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
							}
						}

						if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
							if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postVideo"]["tmp_name"],
									'name' => $_FILES['postVideo']['name'],
									'size' => $_FILES["postVideo"]["size"],
									'type' => $_FILES["postVideo"]["type"],
									'types' => 'mp4,m4v,webm,flv,mov,mpeg'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilename = $media['filename'];
									$mediaName     = $media['name'];
									$img_types     = array(
										'image/png',
										'image/jpeg',
										'image/jpg',
										'image/gif'
									);
									if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
										$fileInfo = array(
											'file' => $_FILES["video_thumb"]["tmp_name"],
											'name' => $_FILES['video_thumb']['name'],
											'size' => $_FILES["video_thumb"]["size"],
											'type' => $_FILES["video_thumb"]["type"],
											'types' => 'jpeg,png,jpg,gif',
											'crop' => array(
												'width' => 525,
												'height' => 295
											)
										);
										$media    = Wo_ShareFile($fileInfo);
										if (!empty($media)) {
											$video_thumb = $media['filename'];
										}
									}
								}
							}
						}
						if (isset($_FILES['postMusic']['name'])) {
							if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postMusic"]["tmp_name"],
									'name' => $_FILES['postMusic']['name'],
									'size' => $_FILES["postMusic"]["size"],
									'type' => $_FILES["postMusic"]["type"],
									'types' => 'mp3,wav'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilenameMusic = $media['filename'];
									$mediaNameMusic     = $media['name'];
								}
							}
						}
						$post_data = array(
							'user_id' => Wo_Secure($wo['user']['user_id']),
							'fund_id' => $fund_id,
							'time' => time(),
							'postRecord' => Wo_Secure($_POST['postRecord']),
							'multi_image' => Wo_Secure($multi),
							'multi_image_post' => 0,
							'postText' => "  ".Wo_Secure($post_text),
							'album_name' => Wo_Secure($album_name),
							'postFile' => Wo_Secure($mediaFilename, 0),
							'postFileName' => Wo_Secure($mediaName),
							'postFileThumb' => Wo_Secure($video_thumb),
							'blur' => $blur,
							'postPrivacy' => 0,
							'active' =>'0',
							//'page_id'=>$page_id
						);

						$id = Wo_RegisterPost($post_data);
						$db->where('id',$id)->update(T_POSTS, array('page_id' =>$page_id));
						unset($post_data['id']);
						unset($post_data['post_id']);
						unset($post_data['parent_id']);
						// $data['post_data'] = $post_data;
						// $data['update'] = $updatedPost;
						// $data['id'] = $id;
						// echo json_encode($data);
						// exit();
				
						if($id && isset($mediaFilenameMusic) && isset($mediaNameMusic) && isset($_FILES['postMusic']['name'])  ){

							$post_data['postFile'] = Wo_Secure($mediaFilenameMusic, 0);
							$post_data['postFileName'] = Wo_Secure($mediaNameMusic);
							$post_data['is_music'] = 1;
							Wo_RegisterPost($post_data);
							unset($post_data['is_music']);

						}

						if (isset($mentions) && is_array($mentions) && $fundraising_for !='2') {
							foreach ($mentions as $mention) {
								if (empty($wo['no_mention']) || (!empty($wo['no_mention']) && !in_array($mention, $wo['no_mention']))) {
									$notification_data_array = array(
										'recipient_id' => $mention,
										'type' => 'post_mention',
										'page_id' => 0,
										'post_id' => $id,
										'url' => 'index.php?link1=show_fund&id=' . $insert_array['hashed_id']
									);
									Wo_RegisterNotification($notification_data_array);
								}
							}
						}
						if (isset($_FILES['postPhotos']['name'])) {
							if (count($_FILES['postPhotos']['name']) > 0) {
								$data['imageUpload'] = 'photos > 0';
								for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
									$fileInfo = array(
										'file' => $_FILES["postPhotos"]["tmp_name"][$i],
										'name' => $_FILES['postPhotos']['name'][$i],
										'size' => $_FILES["postPhotos"]["size"][$i],
										'type' => $_FILES["postPhotos"]["type"][$i],
										'types' => 'jpg,png,jpeg,gif'
									);
									$file     = Wo_ShareFile($fileInfo, 1);
									$image_file = Wo_GetMedia($file['filename']);
									if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
										$blur = 1;
									} elseif ($wo['config']['adult_images'] == 1 && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
										$invalid_file = 3;
										$errors[] = $error_icon . $wo['lang']['adult_image_file'];
										Wo_DeletePost($id);
										@unlink($file['filename']);
										Wo_DeleteFromToS3($file['filename']);
									}
									if (!empty($file)) {
										$media_album = Wo_RegisterAlbumMedia($id, $file['filename']);
										$post_data['multi_image'] = 0;
										$post_data['multi_image_post'] = 1;
										$post_data['album_name'] = '';
										$post_data['postFile'] = $file['filename'];
										$post_data['postFileName'] = $file['name'];
										$post_data['active'] = 1;
										$new_id = Wo_RegisterPost($post_data);
										$media_album = Wo_RegisterAlbumMedia($new_id, $file['filename'], $id);
										$data['image-uploaded'] = 'uploaded' . $new_id . "media_album" . $media_album;
									}
									$data['i-' . $i] = 'uploaded';
								}
							}
						}

						$data['status'] = 200;
						$data['message'] = $wo['lang']['funding_draft_created'];
						$data['url'] = Wo_SeoLink('index.php?link1=my_funding').'?query=drafts';
					} else {
						$data['status'] = 400;
						$message = $error_icon . $wo['lang']['please_check_details'] . "Database Error";
						$data['message'] = $message;
					}
				} else {
					$data['status'] = 400;
					$message = $error_icon . $wo['lang']['file_not_supported'] . "image" . "<br>";
					$data['message'] = $message;
				}
			} else {
				$data['post data'] = $_POST;
				$data['files'] = $_FILES;
				$data['message'] = $error_icon . $wo['lang']['please_check_details'];
			}
		}
	}

	if ($s == 'update_fund_as_draft') {
		//echo "<pre>";print_r($_POST);die;
		if ($wo['config']['who_upload'] == 'pro' && $wo['user']['is_pro'] == 0 && !Wo_IsAdmin() && !empty($_FILES['image'])) {
			$data['message'] = $error_icon . $wo['lang']['free_plan_upload_pro'];
		} 
		else 
		{
			if (!empty($_POST['id']) && !empty($_POST['title'])  && !empty($_POST['description'])  && !empty($_POST['amount']) && is_numeric($_POST['amount']) && $_POST['amount'] > 0) 
			{
				

				// Tagging code will be used for challenge not for campgian sameer
				$post_text = "";
				$beneficiary = "";
				$fund_id = "";
				$funding_admins = "";
				$mention_regex = '/@([A-Za-z0-9_]+)/i';

				$fundraising_for=$_POST['fundraising_for'];
				$bank_name='';
				$bank_account_name='';
				$bank_account_no='';

				if($fundraising_for=='2')
				{
					$beneficiary = Wo_Secure($_POST['beneficiary2']);
				}
				else
				{
					$flag='0';
					if(empty($_POST['bank_name']))
					{
						$flag ='1';
						$message = $error_icon . $wo['lang']['bank_name_required'];
					}
					if(empty($_POST['bank_account_name']))
					{
						$flag ='1';
						$message = $error_icon . $wo['lang']['bank_account_name_required'];
					}
					if(empty($_POST['bank_account_no']))
					{
						$flag ='1';
						$message = $error_icon . $wo['lang']['bank_account_no_required'];
					}
					if($flag=='1')
					{
						$data['status'] = 400;
						$data['message'] = $message;
						echo json_encode($data);
						exit();
					}
					$bank_name=$_POST['bank_name'];
					$bank_account_name=$_POST['bank_account_name'];
					$bank_account_no=$_POST['bank_account_no'];

					$beneficiary = Wo_Secure($_POST['beneficiary1']);
				}
				if (empty($beneficiary)) {
					$data['status'] = 400;
					$message = $error_icon . "Beneficiary is required" . "<br>";
					$data['message'] = $message;
					echo json_encode($data);
					exit();
				} 
				else 
				{
					//$beneficiary = Wo_Secure($_POST['beneficiary']);
					//$fundraising_for=$_POST['fundraising_for'];
					if($fundraising_for=='2')
					{
						preg_match_all($mention_regex, $beneficiary, $matches);
						foreach ($matches[1] as $match) {
							$match         = Wo_Secure($match);
							$match_user    = Wo_PageData(Wo_PageIdFromPagename($match));
							$match_search  = '@' . $match;
							$match_replace = '@[' . $match_user['page_id'] . ']';
							if (isset($match_user['page_id'])) {
								$beneficiary  = str_replace($match_search, $match_replace, $beneficiary);
								$mentions[] = $match_user['page_id'];
							}
						}
					}
					else
					{
						preg_match_all($mention_regex, $beneficiary, $matches);
						foreach ($matches[1] as $match) {
							$match         = Wo_Secure($match);
							$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
							$match_search  = '@' . $match;
							$match_replace = '@[' . $match_user['user_id'] . ']';
							if (isset($match_user['user_id'])) {
								$beneficiary  = str_replace($match_search, $match_replace, $beneficiary);
								$mentions[] = $match_user['user_id'];
							}
						}
					}
					$data['beneficiary'] =sizeof($mentions);
					if (sizeof($mentions) != 1) {
						$data['status'] = 400;
						$message = $error_icon . "beneficiary must be 1 person " . "<br>";
						$data['message'] = $message;
						$data['beneficiary'] =sizeof($mentions);
						echo json_encode($data);
						exit();
					}
				}
				if(isset($_POST['funding_admins']) && !empty($_POST['funding_admins'])){	
					$funding_admins = Wo_Secure($_POST['funding_admins']);
					preg_match_all($mention_regex, $funding_admins, $matches);
					foreach ($matches[1] as $match) {
						$match         = Wo_Secure($match);
						$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
						$match_search  = '@' . $match;
						$match_replace = '@[' . $match_user['user_id'] . ']';
						if (isset($match_user['user_id'])) {
							$funding_admins  = str_replace($match_search, $match_replace, $funding_admins);
							$fundingAdmins[] = $match_user['user_id'];
						}
					}
				}
				$categories=array();
				if (isset($_POST['postTextCategory']) && !empty($_POST['postTextCategory'])) 
				{
					$categories = $_POST['postTextCategory'];
				}

				// Tagging code end;
				$media = array();
				if (!empty($_FILES['image'])) {
					$fileInfo      = array(
						'file' => $_FILES["image"]["tmp_name"],
						'name' => $_FILES['image']['name'],
						'size' => $_FILES["image"]["size"],
						'type' => $_FILES["image"]["type"],
						'types' => 'jpeg,jpg,png,bmp'
					);
					$media  = Wo_ShareFile($fileInfo);
				}

				$insert_array = array(
					'title' => Wo_Secure($_POST['title']),
					'description'   => $_POST['description'],
					'fundraising_for'   => $fundraising_for,
					'amount'   => Wo_Secure($_POST['amount']),
					'time'   => time(),
					'beneficiary' => $beneficiary,
					'funding_admins' => $funding_admins,
					'user_id'  => $wo['user']['id'],
					'category'=>json_encode($categories),
					'bank_name'   => $bank_name,
					'bank_account_name'   => $bank_account_name,
					'bank_account_no'   => $bank_account_no
					
				);

				$due_date = Wo_Secure($_POST['due_date']);
				if ($due_date) {
					$insert_array['due_date'] = $due_date;
				}

				if(!empty($media['filename'])){
					$insert_array['image'] = $media['filename'];
				}

				if (!empty($_POST['id'])) {
					$fund_id = $_POST['id'];
					$updated= $db->where('id',$_POST['id'])->update(T_FUNDING, $insert_array);

					if (!empty($updated)) {
						//multiple options uploads
						// $postPhotos = "";
						// $postVideo = "";
						$mediaFilename = '';
						$post_photo    = '';
						$mediaName     = '';
						$video_thumb   = '';
						$album_name = '';
						$blur = 0;

						if (!empty($_POST['album_name'])) {
							$album_name = $_POST['album_name'];
						}
						if (!isset($_FILES['postPhotos']['name'])) {
							$album_name = '';
						}
						$multi = 0;

						if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) 
						{
							$multi = 1;
							$data['multi'] = $multi;
						}
						if (isset($_FILES['postPhotos']['name'])) {
							$allowed = array(
								'gif',
								'png',
								'jpg',
								'jpeg'
							);
							for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
								$new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
								if (!in_array(strtolower($new_string['extension']), $allowed)) {
									$errors[] = $error_icon . $wo['lang']['please_check_details'];
								}
							}
						}
						if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
							$headers = get_headers($_POST['postSticker'], 1);
							if (strpos($headers['Content-Type'], 'image/') !== false) {
								$post_data['postSticker'] = $_POST['postSticker'];
							} else {
								$invalid_file = 2;
							}
						} else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
							if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
								$post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
							}
						}

						if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
							if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postVideo"]["tmp_name"],
									'name' => $_FILES['postVideo']['name'],
									'size' => $_FILES["postVideo"]["size"],
									'type' => $_FILES["postVideo"]["type"],
									'types' => 'mp4,m4v,webm,flv,mov,mpeg'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilename = $media['filename'];
									$mediaName     = $media['name'];
									$img_types     = array(
										'image/png',
										'image/jpeg',
										'image/jpg',
										'image/gif'
									);
									if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
										$fileInfo = array(
											'file' => $_FILES["video_thumb"]["tmp_name"],
											'name' => $_FILES['video_thumb']['name'],
											'size' => $_FILES["video_thumb"]["size"],
											'type' => $_FILES["video_thumb"]["type"],
											'types' => 'jpeg,png,jpg,gif',
											'crop' => array(
												'width' => 525,
												'height' => 295
											)
										);
										$media    = Wo_ShareFile($fileInfo);
										if (!empty($media)) {
											$video_thumb = $media['filename'];
										}
									}
								}
							}
						}
						if (isset($_FILES['postMusic']['name'])) {
							if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postMusic"]["tmp_name"],
									'name' => $_FILES['postMusic']['name'],
									'size' => $_FILES["postMusic"]["size"],
									'type' => $_FILES["postMusic"]["type"],
									'types' => 'mp3,wav'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilenameMusic = $media['filename'];
									$mediaNameMusic     = $media['name'];
								}
							}
						}
						// $post_data = array(
						// 	'user_id' => Wo_Secure($wo['user']['user_id']),
						// 	'fund_draft_id' => $fund_id,
						// 	'time' => time(),
						// 	'postRecord' => Wo_Secure($_POST['postRecord']),
						// 	'multi_image' => Wo_Secure($multi),
						// 	'multi_image_post' => 0,
						// 	'postText' => Wo_Secure($post_text),
						// 	'album_name' => Wo_Secure($album_name),
						// 	'postFile' => Wo_Secure($mediaFilename, 0),
						// 	'postFileName' => Wo_Secure($mediaName),
						// 	'postFileThumb' => Wo_Secure($video_thumb),
						// 	'blur' => $blur,
						// 	'postPrivacy' => 0,
						// );
						$post = $db->where('fund_id',$fund_id)->getOne(T_POSTS);
						$posts = $db->where('fund_id',$fund_id)->get(T_POSTS);
						$postMusic = $db->where('is_music',1)->where('fund_id',$id)->getOne(T_POSTS);

						// $id = Wo_RegisterPost($post_data);
						// $data['before'] = $post;

						$post->postText = Wo_Secure($post_text);
						if($multi == 1){
							$post->multi_image = Wo_Secure($multi);
						}
						if(!empty($mediaFilename)){


							$post->postFile = Wo_Secure($mediaFilename, 0);



						}
						if(!empty($mediaName)){
							$post->postFileName = Wo_Secure($mediaName);
						}
						if(!empty($video_thumb)){
							$post->postFileThumb = Wo_Secure($video_thumb);
						}
						if($blue == 1){
							$post->blur = $blur;
						}
						if(isset($_POST['postRecord'])){
							$post->postRecord=Wo_Secure($_POST['postRecord']);
						}
						$post_data = json_decode(json_encode($post), true);
						$updatedPost = $db->where('id', $post_data['post_id'])->update(T_POSTS, $post_data);
						
						
						$id = $post_data['post_id'];
						unset($post_data['id']);
						unset($post_data['post_id']);
						unset($post_data['parent_id']);
				
						if($id && isset($mediaFilenameMusic) && isset($mediaNameMusic) && isset($_FILES['postMusic']['name'])  ){

							if(!$postMusic){
									
								$post_data['postFile'] = Wo_Secure($mediaFilenameMusic, 0);
								$post_data['postFileName'] = Wo_Secure($mediaNameMusic);
								$post_data['is_music'] = 1;

								Wo_RegisterPost($post_data);
								unset($post_data['is_music']);

							}else{
								$post_music_data = json_decode(json_encode($postMusic), true);
								$post_music_data['postFile'] = Wo_Secure($mediaFilenameMusic, 0);
								$post_music_data['postFileName'] = Wo_Secure($mediaNameMusic);
								$updatedPostMusic = $db->where('id', $postMusic->post_id)->update(T_POSTS, $post_music_data);

							}
						}

						if (isset($mentions) && is_array($mentions) && $fundraising_for !='2') {
							foreach ($mentions as $mention) {
								if (empty($wo['no_mention']) || (!empty($wo['no_mention']) && !in_array($mention, $wo['no_mention']))) {
									$notification_data_array = array(
										'recipient_id' => $mention,
										'type' => 'post_mention',
										'page_id' => 0,
										'post_id' => $id,
										'url' => 'index.php?link1=show_fund&id=' . $insert_array['hashed_id']
									);
									Wo_RegisterNotification($notification_data_array);
								}
							}
						}
						if (isset($_FILES['postPhotos']['name'])) {
							if (count($_FILES['postPhotos']['name']) > 0) {
							
								$data['imageUpload'] = 'photos > 0';
								for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
									$fileInfo = array(
										'file' => $_FILES["postPhotos"]["tmp_name"][$i],
										'name' => $_FILES['postPhotos']['name'][$i],
										'size' => $_FILES["postPhotos"]["size"][$i],
										'type' => $_FILES["postPhotos"]["type"][$i],
										'types' => 'jpg,png,jpeg,gif'
									);
									$file     = Wo_ShareFile($fileInfo, 1);
									$image_file = Wo_GetMedia($file['filename']);
									if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
										$blur = 1;
									} elseif ($wo['config']['adult_images'] == 1 && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
										$invalid_file = 3;
										$errors[] = $error_icon . $wo['lang']['adult_image_file'];
										Wo_DeletePost($id);
										@unlink($file['filename']);
										Wo_DeleteFromToS3($file['filename']);
									}
									if (!empty($file)) {
										$media_album = Wo_RegisterAlbumMedia($id, $file['filename']);
										$post_data['multi_image'] = 0;
										$post_data['multi_image_post'] = 1;
										$post_data['album_name'] = '';
										$post_data['postFile'] = $file['filename'];
										$post_data['postFileName'] = $file['name'];
										$post_data['active'] = 1;
										$new_id = Wo_RegisterPost($post_data);
										$media_album = Wo_RegisterAlbumMedia($new_id, $file['filename'], $id);
										$data['image-uploaded'] = 'uploaded' . $new_id . "media_album" . $media_album;
									}
									$data['i-' . $i] = 'uploaded';
								}
							}
						}

						$data['status'] = 200;
						$data['message'] = $wo['lang']['funding_draft_created'];
						$data['url'] = Wo_SeoLink('index.php?link1=my_funding').'?query=drafts';
					} else {
						$data['status'] = 400;
						$message = $error_icon . $wo['lang']['please_check_details'] . "Database Error";
						$data['message'] = $message;
					}
				} else {
					$data['status'] = 400;
					$message = $error_icon . $wo['lang']['file_not_supported'] . "image" . "<br>";
					$data['message'] = $message;
				}
			} else {
				$data['post data'] = $_POST;
				$data['files'] = $_FILES;
				$data['message'] = $error_icon . $wo['lang']['please_check_details'];
			}
		}
		
	}

	// sameer challenged
	if ($s == 'insert_challenge') {
		
		$sqlConnect;
		if ($wo['config']['who_upload'] == 'pro' && $wo['user']['is_pro'] == 0 && !Wo_IsAdmin() && !empty($_FILES['image'])) {
			$data['message'] = $error_icon . $wo['lang']['free_plan_upload_pro'];
		} 
		else 
		{
			if (!empty($_POST['title'])  && !empty($_POST['description'])) 
			{
				$media = array();
				$multi = 0;
				if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) {
					if (count($_FILES['postPhotos']['name']) == 1) {
						if ($_FILES['postPhotos']['size'][0] > $wo['config']['maxUpload']) {
							$invalid_file = 1;
							$data['invalid_file'] = $invalid_file;
						} else if (Wo_IsFileAllowed($_FILES['postPhotos']['name'][0]) == false) {
							$invalid_file = 2;
							$data['invalid_file'] = $invalid_file;
						} else {

							$fileInfo = array(
								'file' => $_FILES["postPhotos"]["tmp_name"][0],
								'name' => $_FILES['postPhotos']['name'][0],
								'size' => $_FILES["postPhotos"]["size"][0],
								'type' => $_FILES["postPhotos"]["type"][0]
							);
							$media    = Wo_ShareFile($fileInfo);
							if (!empty($media)) {
								$image_file = Wo_GetMedia($media['filename']);
								$upload = true;
								if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
									$blur = 1;
								} elseif ($wo['config']['adult_images'] == 1  && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
									$invalid_file = 3;
									$upload = false;
									@unlink($media['filename']);
									Wo_DeleteFromToS3($media['filename']);
								}
								$mediaFilename = $media['filename'];
								$mediaName     = $media['name'];
							}
						}
					} 
					else {
						$multi = 1;
						$data['multi'] = $multi;
					}
				}
				if (isset($_FILES['postPhotos']['name'])) {
					$allowed = array(
						'gif',
						'png',
						'jpg',
						'jpeg'
					);
					for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
						$new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
						if (!in_array(strtolower($new_string['extension']), $allowed)) {
							$errors[] = $error_icon . $wo['lang']['please_check_details'];
						}
					}
				}
				if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
					$headers = get_headers($_POST['postSticker'], 1);
					if (strpos($headers['Content-Type'], 'image/') !== false) {
						$post_data['postSticker'] = $_POST['postSticker'];
					} else {
						$invalid_file = 2;
					}
				} else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
					if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
						$post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
					}
				}
				$videothumb='';
				if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
					if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
						$invalid_file = 1;
					} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
						$invalid_file = 2;
					} else {
						$fileInfo = array(
							'file' => $_FILES["postVideo"]["tmp_name"],
							'name' => $_FILES['postVideo']['name'],
							'size' => $_FILES["postVideo"]["size"],
							'type' => $_FILES["postVideo"]["type"],
							'types' => 'mp4,m4v,webm,flv,mov,mpeg'
						);
						$media    = Wo_ShareFile($fileInfo);
						if (!empty($media)) {
							$mediaFilename = $media['filename'];
							$mediaName     = $media['name'];
							$img_types     = array(
								'image/png',
								'image/jpeg',
								'image/jpg',
								'image/gif'
							);
							if (!file_exists('upload/videos/' . date('Y'))) {
						        @mkdir('upload/videos/' . date('Y'), 0777, true);
						    }
						    if (!file_exists('upload/videos/' . date('Y') . '/' . date('m'))) {
						        @mkdir('upload/videos/' . date('Y') . '/' . date('m'), 0777, true);
						    }
						    $dir         = "upload/videos/" . date('Y') . '/' . date('m');
    						$videothumb    = $dir . '/' . Wo_GenerateKey() . '_' . date('d') . '_' . md5(time()) . "_thumbnail.jpg";

							//$videothumb='upload/photos/thumbnail/'.md5(time()).'.jpg';
							//$thumb=$wo['config']['site_url'].'/'.$mediaFilename;
							$base_url=$wo['config']['site_url'];
							if ($wo['config']['amazone_s3'] == 1)  
							{
                                                            $base_url = $wo['config']['s3_site_url'];
                                                        }

							$thumb=$base_url.'/'.$mediaFilename;
							$ffmpeg = \FFMpeg\FFMpeg::create([
                                    'ffmpeg.binaries'  => FFMPEG_FFMPEG,
                                    'ffprobe.binaries' => FFMPEG_FFPROBE 
                                      ]);
					        $video = $ffmpeg->open($thumb);
					       	$frame=$video->frame(FFMpeg\Coordinate\TimeCode::fromSeconds(5));
					        $frame->save($videothumb);
					        if (($wo['config']['amazone_s3'] == 1 || $wo['config']['ftp_upload'] == 1 || $wo['config']['spaces'] == 1 || $wo['config']['cloud_upload'] == 1) && !empty($videothumb)) {
				                $upload_s3 = Wo_UploadToS3($videothumb);
				            }
							/*if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
								$fileInfo = array(
									'file' => $_FILES["video_thumb"]["tmp_name"],
									'name' => $_FILES['video_thumb']['name'],
									'size' => $_FILES["video_thumb"]["size"],
									'type' => $_FILES["video_thumb"]["type"],
									'types' => 'jpeg,png,jpg,gif',
									'crop' => array(
										'width' => 525,
										'height' => 295
									)
								);
								$media    = Wo_ShareFile($fileInfo);
								
								if (!empty($media)) 
								{
									$video_thumb = $media['filename'];
								}
							}*/
						}
					}
				}
				if (isset($_FILES['postMusic']['name'])  && empty($mediaFilename)) {
					if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
						$invalid_file = 1;
					} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
						$invalid_file = 2;
					} else {
						$fileInfo = array(
							'file' => $_FILES["postMusic"]["tmp_name"],
							'name' => $_FILES['postMusic']['name'],
							'size' => $_FILES["postMusic"]["size"],
							'type' => $_FILES["postMusic"]["type"],
							'types' => 'mp3,wav'
						);
						$media    = Wo_ShareFile($fileInfo);
						if (!empty($media)) {
							$mediaFilename = $media['filename'];
							$mediaName    = $media['name'];
						}
					}
				}

				
				if (!empty($media) && !empty($media['filename'])) 
				{
					if(isset($_POST['challenge_id']) && !empty($_POST['challenge_id']))
					{
						$challenge_id = Wo_Secure($_POST['challenge_id']);
						// update challenge;
						$challenge_data = $db->where('id',$challenge_id)->getOne(T_CHALLENGE);
						// $challenge_data = json_decode(json_encode($challenge_data));
						$challenge_query     = "UPDATE " . T_CHALLENGE . " SET `last_trend_time` = " . time() . ", `trend_use_num` = " . ($challenge_data->trend_use_num + 1) . " WHERE `id` = " . $challenge_id;
                		$challenge_sql_query = mysqli_query($sqlConnect, $challenge_query);

					}
					else
					{
				
						// insert Challenge 
						$week = date('Y-m-d', strtotime(date('Y-m-d') . " +1week"));
						$insert_challenge = array(
							'hash' => md5(Wo_Secure($_POST['title'])),
							'title' => Wo_Secure($_POST['title']),
							'last_trend_time' => time(),
							'trend_use_num' => 1,
							'expire_date' => $week
						); 
					
						$challenge_id = $db->insert(T_CHALLENGE,$insert_challenge);
					}
					
					$insert_array = array(
						'funding_id' => Wo_Secure($_POST['funding_id']),
						'challenge_id' => Wo_Secure($challenge_id),
						'invited_by' => 0,
						'description'   => Wo_Secure($_POST['description']),
						//'invites' => Wo_Secure($post_text),
						'time'   => time(),
						'file' => $media['name'],
						'fileName' => $media['filename'],
						'fileThumb' => $videothumb,
						'user_id'  => $wo['user']['id'],
						'status' => '1',
						'hashed_id' => Wo_GenerateKey(15, 15)
					);
					// 'postRecord' => Wo_Secure($_POST['postRecord']),
			
					$challenge_fund_id = $db->insert(T_CHALLENGE_FUNDING, $insert_array);
			
					if(!empty($challenge_fund_id))
					{
						if(empty($_POST['challenge_id']))
						{
							Wo_sendMailToCampaignOrganizer($challenge_fund_id);
						}
						
						$mediaFilename = '';
						$post_photo    = '';
						$mediaName     = '';
						$video_thumb   = '';
						$album_name = '';
						$blur = 0;
						$page_id='';
						$campaign_id=$_POST['funding_id'];
						$campaign = $db->where('id',$campaign_id)->getOne(T_FUNDING);
						if(!empty($campaign) && $campaign->fundraising_for=='2')
						{
							$page = Wo_GetTaggedPages($campaign->beneficiary);
							$page_id=$page[0]['id'];
						}
						if (!empty($_POST['album_name'])) {
							$album_name = $_POST['album_name'];
						}
						if (!isset($_FILES['postPhotos']['name'])) {
							$album_name = '';
						}
						$multi = 0;

						if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) 
						{
							if (count($_FILES['postPhotos']['name']) == 1) {
								if ($_FILES['postPhotos']['size'][0] > $wo['config']['maxUpload']) {
									$invalid_file = 1;
									$data['invalid_file'] = $invalid_file;
								} else if (Wo_IsFileAllowed($_FILES['postPhotos']['name'][0]) == false) {
									$invalid_file = 2;
									$data['invalid_file'] = $invalid_file;
								} else {

									$fileInfo = array(
										'file' => $_FILES["postPhotos"]["tmp_name"][0],
										'name' => $_FILES['postPhotos']['name'][0],
										'size' => $_FILES["postPhotos"]["size"][0],
										'type' => $_FILES["postPhotos"]["type"][0]
									);
									$media    = Wo_ShareFile($fileInfo);
									if (!empty($media)) {
										$image_file = Wo_GetMedia($media['filename']);
										$upload = true;
										if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
											$blur = 1;
										} elseif ($wo['config']['adult_images'] == 1  && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
											$invalid_file = 3;
											$upload = false;
											@unlink($media['filename']);
											Wo_DeleteFromToS3($media['filename']);
										}
										$mediaFilename = $media['filename'];
										$mediaName     = $media['name'];
									}
								}
							} else {
								$multi = 1;
								$data['multi'] = $multi;
							}
						}
						if (isset($_FILES['postPhotos']['name'])) 
						{
							$allowed = array(
								'gif',
								'png',
								'jpg',
								'jpeg'
							);
							for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
								$new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
								if (!in_array(strtolower($new_string['extension']), $allowed)) {
									$errors[] = $error_icon . $wo['lang']['please_check_details'];
								}
							}
						}
						if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
							$headers = get_headers($_POST['postSticker'], 1);
							if (strpos($headers['Content-Type'], 'image/') !== false) {
								$post_data['postSticker'] = $_POST['postSticker'];
							} else {
								$invalid_file = 2;
							}
						} else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
							if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
								$post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
							}
						}

						if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
							if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postVideo"]["tmp_name"],
									'name' => $_FILES['postVideo']['name'],
									'size' => $_FILES["postVideo"]["size"],
									'type' => $_FILES["postVideo"]["type"],
									'types' => 'mp4,m4v,webm,flv,mov,mpeg'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilename = $media['filename'];
									$mediaName     = $media['name'];
									$img_types     = array(
										'image/png',
										'image/jpeg',
										'image/jpg',
										'image/gif'
									);
									if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
										$fileInfo = array(
											'file' => $_FILES["video_thumb"]["tmp_name"],
											'name' => $_FILES['video_thumb']['name'],
											'size' => $_FILES["video_thumb"]["size"],
											'type' => $_FILES["video_thumb"]["type"],
											'types' => 'jpeg,png,jpg,gif',
											'crop' => array(
												'width' => 525,
												'height' => 295
											)
										);
										$media    = Wo_ShareFile($fileInfo);
										if (!empty($media)) {
											$video_thumb = $media['filename'];
										}
									}
								}
							}
						}
						if (isset($_FILES['postMusic']['name']) && empty($mediaFilename)) {
							if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postMusic"]["tmp_name"],
									'name' => $_FILES['postMusic']['name'],
									'size' => $_FILES["postMusic"]["size"],
									'type' => $_FILES["postMusic"]["type"],
									'types' => 'mp3,wav'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilename = $media['filename'];
									$mediaName     = $media['name'];
								}
							}
						}
						$post_data = array( 
							'user_id' => Wo_Secure($wo['user']['user_id']),
							//'page_id' => $page_id,
							'challenge_fund_id' => $challenge_fund_id,
							'time' => time(),
							'postRecord' => Wo_Secure($_POST['postRecord']),
							'multi_image' => Wo_Secure($multi),
							'multi_image_post' => 0,
							'postText' => '   ',
							'album_name' => Wo_Secure($album_name),
							'postFile' => Wo_Secure($mediaFilename, 0),
							'postFileName' => Wo_Secure($mediaName),
							'postFileThumb' => Wo_Secure($video_thumb),
							'blur' => $blur,
							'postPrivacy' => 0,
						);

						$id = Wo_RegisterPost($post_data);
						$db->where('id',$id)->update(T_POSTS, array('page_id' =>$page_id));
						unset($post_data['id']);
						unset($post_data['post_id']);
						unset($post_data['parent_id']);
						// if($id && isset($mediaFilenameMusic) && isset($mediaNameMusic) && isset($_FILES['postMusic']['name']) ){

						// 	$post_data['postFile'] = Wo_Secure($mediaFilenameMusic, 0);
						// 	$post_data['postFileName'] = Wo_Secure($mediaNameMusic);
						// 	$post_data['is_music'] = 1;
						// 	Wo_RegisterPost($post_data);
						// }
						if(!empty($id)){
							if (isset($_FILES['postPhotos']['name'])) {
								if (count($_FILES['postPhotos']['name']) > 0) {
									$data['imageUpload'] = 'photos > 0';
	
									for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
										$fileInfo = array(
											'file' => $_FILES["postPhotos"]["tmp_name"][$i],
											'name' => $_FILES['postPhotos']['name'][$i],
											'size' => $_FILES["postPhotos"]["size"][$i],
											'type' => $_FILES["postPhotos"]["type"][$i],
											'types' => 'jpg,png,jpeg,gif'
										);
										$file     = Wo_ShareFile($fileInfo, 1);
										$image_file = Wo_GetMedia($file['filename']);
										if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
											$blur = 1;
										} elseif ($wo['config']['adult_images'] == 1 && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
											$invalid_file = 3;
											$errors[] = $error_icon . $wo['lang']['adult_image_file'];
											Wo_DeletePost($id);
											@unlink($file['filename']);
											Wo_DeleteFromToS3($file['filename']);
										}
										if (!empty($file)) {
											$media_album = Wo_RegisterAlbumMedia($id, $file['filename']);
											$post_data['multi_image'] = 0;
											$post_data['multi_image_post'] = 1;
											$post_data['album_name'] = '';
											$post_data['postFile'] = $file['filename'];
											$post_data['postFileName'] = $file['name'];
											$post_data['active'] = 1;
											$new_id = Wo_RegisterPost($post_data);
											$media_album = Wo_RegisterAlbumMedia($new_id, $file['filename'], $id);
											$data['image-uploaded'] = 'uploaded' . $new_id . "media_album" . $media_album;
										}
										$data['i-' . $i] = 'uploaded';
									}
								}
							}
							$data['status'] = 200;
							$data['message'] = $wo['lang']['challenge_created'];
							//$data['url'] = Wo_SeoLink('index.php?link1=my_challenges');
							$data['challenge_link'] = $wo['config']['site_url'] . '/show_challenge/' . $insert_array['hashed_id'];
							$data['challenge_id'] =$challenge_fund_id;
						}else{
							$data['status'] = 400;
							$message = $error_icon . $wo['lang']['please_check_details'] . "Post insert error";
							$data['message'] = $message;
						}
						
					}else{
						$data['status'] = 400;
						$message = $error_icon . $wo['lang']['please_check_details'] . "Database Error";
						$data['message'] = $message;
					}
				}else{
					$data['status'] = 400;
					$message = $error_icon . $wo['lang']['please_check_details'] . " Regarding Media";
					$data['message'] = $message;
				}
			} else {
				$data['post data'] = $_POST;
				$data['files'] = $_FILES;
				$data['message'] = $error_icon . $wo['lang']['please_check_details'];
			}
		}
	}
	if ($s == 'challenge_tag_friends') 
	{
		// Tagging code will be used for challenge not for campaygn
		//echo "<pre>";print_r($_POST);print_r($_GET);die;
		$challenge_fund_id=$_POST['new_challengefund_id'];
		$challengedetail=$db->where('id',$challenge_fund_id)->getOne(T_CHALLENGE_FUNDING);
		$tagfriends = "";
		$mention_regex = '/@([A-Za-z0-9_]+)/i';
		$mentions=array();
		$mention_regex = '/@([A-Za-z0-9_]+)/i';
		if(empty($_POST['tagfriends']))
		{
			// $data['status'] = 400;
			// $message = $error_icon.$wo['lang']['please_tag_friends']."<br>";						
			// $data['message'] =$message;
			// echo json_encode($data);	
			// exit();
		}
		else
		{
			$tagfriends = Wo_Secure($_POST['tagfriends']);
			preg_match_all($mention_regex, $tagfriends, $matches);
			foreach ($matches[1] as $match) {
				$match         = Wo_Secure($match);
				// $match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
				$match_user['user_id']    = Wo_UserIdFromUsername($match);
				$match_search  = '@' . $match;
				$match_replace = '@[' . $match_user['user_id'] . ']';
				// if (isset($match_user['user_id'])) {
					$tagfriends  = str_replace($match_search, $match_replace, $tagfriends);
					$mentions[] = $match_user['user_id'];
				// }
			}
		
			// if(sizeof($mentions) < 3){
			// 	$data['status'] = 400;
			// 	$message =$error_icon.$wo['lang']['please_tag_friends']."<br>";						
			// 	$data['message'] = $message;
			// 	echo json_encode($data);	
			// 	exit();
			// }
		}
		// Tagging code end;
		

		//echo "<pre>";print_r($mentions);print_r($_POST);die;
		if(sizeof($mentions) > 0)
		{
			$user_message=$_POST['message'];
			$is_modal=$_POST['is_modal'];
			$taggedUsers = Wo_GetTaggedUsers($tagfriends);
			$tagged_users_challenge_fund_id = array();

			$db->where('id',$challenge_fund_id)->update(T_CHALLENGE_FUNDING,array('invites' => Wo_Secure($tagfriends)));
				foreach($taggedUsers as $user)
				{
					$invites_insert_array = array(
						'funding_id' => Wo_Secure($challengedetail->funding_id),
						'challenge_id' => Wo_Secure($challengedetail->challenge_id),
						'invited_by' => $wo['user']['id'],
						'description'   => Wo_Secure($challengedetail->description),
						'invites' => '',
						'time'   => time(),
						'file' => '',
						'fileName' => '',
						'user_id'  => $user['id'],
						//'status' => '0',
						'challenge_fund_id'=>$challenge_fund_id,
						'hashed_id' => Wo_GenerateKey(15, 15)
					);
					$iddd = $db->insert(T_CHALLENGE_FUNDING,$invites_insert_array);
					$tagged_users_challenge_fund_id[] = array('user_id' => $user['id'],'cha_fund_id' => $iddd);
				}
			
			$data['mentions'] = array();
			
			if (isset($mentions) && is_array($mentions)) 
			{
				//foreach($tagged_users_challenge_fund_id as $val)
				//{	
					foreach ($mentions as $mention) 
					{
						if (empty($wo['no_mention']) || (!empty($wo['no_mention']) && !in_array($mention, $wo['no_mention']))) {
							//if($mention == $val['user_id']){
								$notification_data_array = array(
									'recipient_id' => $mention,
									'type' => 'challenge_invitation',
									'page_id' => 0,
									'text' => $user_message,
									'challenge_funding_id' => $challengedetail->id,
									'url' => 'show_challenge/'.$challengedetail->hashed_id
								);
								//Wo_RegisterNotification($notification_data_array);
								$id=Wo_RegisterNotification($notification_data_array);
								$db->where('id',$id)->update(T_NOTIFICATION,array('url' => $config['site_url'] . "/show_challenge/" . $challengedetail->hashed_id.'?notify='.$id));
							//}
						
						}
					}
				//}
				
			}
		}
		$data['status'] = 200;
		if($is_modal =='1')
		{
			$data['message'] = $wo['lang']['challenge_invitation_sent'];
		}
		else
		{
			$data['message'] = $wo['lang']['challenge_created'];
		}
		
		//$data['url'] = Wo_SeoLink('index.php?link1=my_challenges');
		$data['url'] = $wo['config']['site_url'] . '/challenge_thankyou/' . $challengedetail->hashed_id;

	}
	// insert accepted challenge
	if ($s == 'insert_accepted_challenge') {
		$sqlConnect;
		if ($wo['config']['who_upload'] == 'pro' && $wo['user']['is_pro'] == 0 && !Wo_IsAdmin() && !empty($_FILES['image'])) {
			$data['message'] = $error_icon . $wo['lang']['free_plan_upload_pro'];
		} else {
			if (!empty($_POST['challenge_id']) && !empty($_POST['challenge_fund_id'])) {

				
				// Tagging code end;
				$media = array();
				$multi = 0;
				if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) {
					if (count($_FILES['postPhotos']['name']) == 1) {
						if ($_FILES['postPhotos']['size'][0] > $wo['config']['maxUpload']) {
							$invalid_file = 1;
							$data['invalid_file'] = $invalid_file;
						} else if (Wo_IsFileAllowed($_FILES['postPhotos']['name'][0]) == false) {
							$invalid_file = 2;
							$data['invalid_file'] = $invalid_file;
						} else {

							$fileInfo = array(
								'file' => $_FILES["postPhotos"]["tmp_name"][0],
								'name' => $_FILES['postPhotos']['name'][0],
								'size' => $_FILES["postPhotos"]["size"][0],
								'type' => $_FILES["postPhotos"]["type"][0]
							);
							$media    = Wo_ShareFile($fileInfo);
							if (!empty($media)) {
								$image_file = Wo_GetMedia($media['filename']);
								$upload = true;
								if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
									$blur = 1;
								} elseif ($wo['config']['adult_images'] == 1  && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
									$invalid_file = 3;
									$upload = false;
									@unlink($media['filename']);
									Wo_DeleteFromToS3($media['filename']);
								}
								$mediaFilename = $media['filename'];
								$mediaName     = $media['name'];
							}
						}
					} else {
						$multi = 1;
						$data['multi'] = $multi;
					}
				}
				if (isset($_FILES['postPhotos']['name'])) {
					$allowed = array(
						'gif',
						'png',
						'jpg',
						'jpeg'
					);
					for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
						$new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
						if (!in_array(strtolower($new_string['extension']), $allowed)) {
							$errors[] = $error_icon . $wo['lang']['please_check_details'];
						}
					}
				}
				if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
					$headers = get_headers($_POST['postSticker'], 1);
					if (strpos($headers['Content-Type'], 'image/') !== false) {
						$post_data['postSticker'] = $_POST['postSticker'];
					} else {
						$invalid_file = 2;
					}
				} else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
					if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
						$post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
					}
				}
				if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
					if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
						$invalid_file = 1;
					} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
						$invalid_file = 2;
					} else {
						$fileInfo = array(
							'file' => $_FILES["postVideo"]["tmp_name"],
							'name' => $_FILES['postVideo']['name'],
							'size' => $_FILES["postVideo"]["size"],
							'type' => $_FILES["postVideo"]["type"],
							'types' => 'mp4,m4v,webm,flv,mov,mpeg'
						);
						$media    = Wo_ShareFile($fileInfo);
						if (!empty($media)) {
							$mediaFilename = $media['filename'];
							$mediaName     = $media['name'];
							$img_types     = array(
								'image/png',
								'image/jpeg',
								'image/jpg',
								'image/gif'
							);
							if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
								$fileInfo = array(
									'file' => $_FILES["video_thumb"]["tmp_name"],
									'name' => $_FILES['video_thumb']['name'],
									'size' => $_FILES["video_thumb"]["size"],
									'type' => $_FILES["video_thumb"]["type"],
									'types' => 'jpeg,png,jpg,gif',
									'crop' => array(
										'width' => 525,
										'height' => 295
									)
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$video_thumb = $media['filename'];
								}
							}
						}
					}
				}
				if (isset($_FILES['postMusic']['name']) && empty($mediaFilename)) {
					if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
						$invalid_file = 1;
					} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
						$invalid_file = 2;
					} else {
						$fileInfo = array(
							'file' => $_FILES["postMusic"]["tmp_name"],
							'name' => $_FILES['postMusic']['name'],
							'size' => $_FILES["postMusic"]["size"],
							'type' => $_FILES["postMusic"]["type"],
							'types' => 'mp3,wav'
						);
						$media    = Wo_ShareFile($fileInfo);
						if (!empty($media)) {
							$mediaFilename = $media['filename'];
							$mediaName     = $media['name'];
						}
					}
				}

				if (!empty($media) && !empty($media['filename'])) {
					if(isset($_POST['challenge_id']) && !empty($_POST['challenge_id'])){
					
						$challenge_id = Wo_Secure($_POST['challenge_id']);
						// update challenge;
						$challenge_data = $db->where('id',$challenge_id)->getOne(T_CHALLENGE);
						// $challenge_data = json_decode(json_encode($challenge_data));
						
						$challenge_query     = "UPDATE " . T_CHALLENGE . " SET `last_trend_time` = " . time() . ", `trend_use_num` = '" . ($challenge_data->trend_use_num + 1) . "' WHERE `id` = '" . $challenge_id ."'";
                		$challenge_sql_query = mysqli_query($sqlConnect, $challenge_query);
				
						
					
					}else{
						// insert Challenge 
						$week = date('Y-m-d', strtotime(date('Y-m-d') . " +1week"));
						$insert_challenge = array(
							'hash' => md5(Wo_Secure($_POST['title'])),
							'title' => Wo_Secure($_POST['title']),
							'last_trend_time' => time(),
							'trend_use_num' => 1,
							'expire_date' => $week
						); 
						$challenge_id = $db->insert(T_CHALLENGE,$insert_challenge);
					}

		

					if(isset($_POST['challenge_fund_id']) && !empty($_POST['challenge_fund_id'])){
			
						$challenge_fund_id = $_POST['challenge_fund_id'];
						$challenge_fund = $db->where('id',$challenge_fund_id)->getOne(T_CHALLENGE_FUNDING);
						$challenge_fund->invites = Wo_Secure($post_text);
						$challenge_fund->time = time();
						if(!empty($media)){
							$challenge_fund->file =  $media['name'];							
							$challenge_fund->fileName = $media['filename'];
						}
						$challenge_fund->status = 1;
						$challenge_fund = (Array)$challenge_fund;
						
						$result = $db->where('id',$challenge_fund_id)->update(T_CHALLENGE_FUNDING, $challenge_fund);
						if(!$result){
							$challenge_fund_id = false;
						}
					}else{
						$data['status'] = 400;
						$message = $error_icon . $wo['lang']['please_check_details'] . " Challenge Funding Id id not found";
						$data['message'] = $message;
					}
					
					// 'postRecord' => Wo_Secure($_POST['postRecord']),
					//Challenge Funding Updated for user invited
					if(!empty($challenge_fund_id))
					{

						
						$mediaFilename = '';
						$post_photo    = '';
						$mediaName     = '';
						$video_thumb   = '';
						$album_name = '';
						$blur = 0;

						if (!empty($_POST['album_name'])) {
							$album_name = $_POST['album_name'];
						}
						if (!isset($_FILES['postPhotos']['name'])) {
							$album_name = '';
						}
						$multi = 0;

						if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) {
							if (count($_FILES['postPhotos']['name']) == 1) {
								if ($_FILES['postPhotos']['size'][0] > $wo['config']['maxUpload']) {
									$invalid_file = 1;
									$data['invalid_file'] = $invalid_file;
								} else if (Wo_IsFileAllowed($_FILES['postPhotos']['name'][0]) == false) {
									$invalid_file = 2;
									$data['invalid_file'] = $invalid_file;
								} else {

									$fileInfo = array(
										'file' => $_FILES["postPhotos"]["tmp_name"][0],
										'name' => $_FILES['postPhotos']['name'][0],
										'size' => $_FILES["postPhotos"]["size"][0],
										'type' => $_FILES["postPhotos"]["type"][0]
									);
									$media    = Wo_ShareFile($fileInfo);
									if (!empty($media)) {
										$image_file = Wo_GetMedia($media['filename']);
										$upload = true;
										if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
											$blur = 1;
										} elseif ($wo['config']['adult_images'] == 1  && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
											$invalid_file = 3;
											$upload = false;
											@unlink($media['filename']);
											Wo_DeleteFromToS3($media['filename']);
										}
										$mediaFilename = $media['filename'];
										$mediaName     = $media['name'];
									}
								}
							} else {
								$multi = 1;
								$data['multi'] = $multi;
							}
						}
						if (isset($_FILES['postPhotos']['name'])) {
							$allowed = array(
								'gif',
								'png',
								'jpg',
								'jpeg'
							);
							for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
								$new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
								if (!in_array(strtolower($new_string['extension']), $allowed)) {
									$errors[] = $error_icon . $wo['lang']['please_check_details'];
								}
							}
						}
						if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
							$headers = get_headers($_POST['postSticker'], 1);
							if (strpos($headers['Content-Type'], 'image/') !== false) {
								$post_data['postSticker'] = $_POST['postSticker'];
							} else {
								$invalid_file = 2;
							}
						} else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
							if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
								$post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
							}
						}

						if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
							if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postVideo"]["tmp_name"],
									'name' => $_FILES['postVideo']['name'],
									'size' => $_FILES["postVideo"]["size"],
									'type' => $_FILES["postVideo"]["type"],
									'types' => 'mp4,m4v,webm,flv,mov,mpeg'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilename = $media['filename'];
									$mediaName     = $media['name'];
									$img_types     = array(
										'image/png',
										'image/jpeg',
										'image/jpg',
										'image/gif'
									);
									if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
										$fileInfo = array(
											'file' => $_FILES["video_thumb"]["tmp_name"],
											'name' => $_FILES['video_thumb']['name'],
											'size' => $_FILES["video_thumb"]["size"],
											'type' => $_FILES["video_thumb"]["type"],
											'types' => 'jpeg,png,jpg,gif',
											'crop' => array(
												'width' => 525,
												'height' => 295
											)
										);
										$media    = Wo_ShareFile($fileInfo);
										if (!empty($media)) {
											$video_thumb = $media['filename'];
										}
									}
								}
							}
						}
						if (isset($_FILES['postMusic']['name']) && empty($mediaFilename)) {
							if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
								$invalid_file = 1;
							} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
								$invalid_file = 2;
							} else {
								$fileInfo = array(
									'file' => $_FILES["postMusic"]["tmp_name"],
									'name' => $_FILES['postMusic']['name'],
									'size' => $_FILES["postMusic"]["size"],
									'type' => $_FILES["postMusic"]["type"],
									'types' => 'mp3,wav'
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$mediaFilename = $media['filename'];
									$mediaName    = $media['name'];
								}
							}
						}
						$post_data = array(
							'user_id' => Wo_Secure($wo['user']['user_id']),
							'challenge_fund_id' => $challenge_fund_id,
							'time' => time(),
							'postRecord' => Wo_Secure($_POST['postRecord']),
							'multi_image' => Wo_Secure($multi),
							'multi_image_post' => 0,
							'postText' => '  ',
							'album_name' => Wo_Secure($album_name),
							'postFile' => Wo_Secure($mediaFilename, 0),
							'postFileName' => Wo_Secure($mediaName),
							'postFileThumb' => Wo_Secure($video_thumb),
							'blur' => $blur,
							'postPrivacy' => 0,
						);
						$id = Wo_RegisterPost($post_data);
						unset($post_data['id']);
						unset($post_data['post_id']);
						unset($post_data['parent_id']);
						// if($id && isset($mediaFilenameMusic) && isset($mediaNameMusic) && isset($_FILES['postMusic']['name']) ){

						// 	$post_data['postFile'] = Wo_Secure($mediaFilenameMusic, 0);
						// 	$post_data['postFileName'] = Wo_Secure($mediaNameMusic);
						// 	$post_data['is_music'] = 1;
						// 	Wo_RegisterPost($post_data);
						// }
						if(!empty($id)){
							if (isset($_FILES['postPhotos']['name'])) {
								if (count($_FILES['postPhotos']['name']) > 0) {
									$data['imageUpload'] = 'photos > 0';
	
									for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
										$fileInfo = array(
											'file' => $_FILES["postPhotos"]["tmp_name"][$i],
											'name' => $_FILES['postPhotos']['name'][$i],
											'size' => $_FILES["postPhotos"]["size"][$i],
											'type' => $_FILES["postPhotos"]["type"][$i],
											'types' => 'jpg,png,jpeg,gif'
										);
										$file     = Wo_ShareFile($fileInfo, 1);
										$image_file = Wo_GetMedia($file['filename']);
										if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
											$blur = 1;
										} elseif ($wo['config']['adult_images'] == 1 && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
											$invalid_file = 3;
											$errors[] = $error_icon . $wo['lang']['adult_image_file'];
											Wo_DeletePost($id);
											@unlink($file['filename']);
											Wo_DeleteFromToS3($file['filename']);
										}
										if (!empty($file)) {
											$media_album = Wo_RegisterAlbumMedia($id, $file['filename']);
											$post_data['multi_image'] = 0;
											$post_data['multi_image_post'] = 1;
											$post_data['album_name'] = '';
											$post_data['postFile'] = $file['filename'];
											$post_data['postFileName'] = $file['name'];
											$post_data['active'] = 1;
											$new_id = Wo_RegisterPost($post_data);
											$media_album = Wo_RegisterAlbumMedia($new_id, $file['filename'], $id);
											$data['image-uploaded'] = 'uploaded' . $new_id . "media_album" . $media_album;
										}
										$data['i-' . $i] = 'uploaded';
									}
								}
							}
							$data['status'] = 200;
							$data['message'] = $wo['lang']['challenge_created'];
							$data['url'] = Wo_SeoLink('index.php?link1=my_challenges');
						}else{
							$data['status'] = 400;
							$message = $error_icon . $wo['lang']['please_check_details'] . " Post Error";
							$data['message'] = $message;
							}
					

					
					}else{
						$data['status'] = 400;
						$message = $error_icon . $wo['lang']['please_check_details'] . " Database Error";
						$data['message'] = $message;
					}
				}else{
					$data['status'] = 400;
					$message = $error_icon . $wo['lang']['please_check_details'] . " Regarding Media";
					$data['message'] = $message;
				}
			} else {
				$data['post data'] = $_POST;
				$data['files'] = $_FILES;
				$data['message'] = $error_icon . $wo['lang']['please_check_details'];
			}
		}
	}
	if ($s == 'save_challenge_as_draft') {
		$sqlConnect;
		if ($wo['config']['who_upload'] == 'pro' && $wo['user']['is_pro'] == 0 && !Wo_IsAdmin() && !empty($_FILES['image'])) {
			$data['message'] = $error_icon . $wo['lang']['free_plan_upload_pro'];
		} else {
			if (!empty($_POST['title'])  && !empty($_POST['description'])) {

				// Tagging code will be used for challenge not for campgian sameer
				$post_text = "";
				$mention_regex = '/@([A-Za-z0-9_]+)/i';

				$mention_regex = '/@([A-Za-z0-9_]+)/i';
				if(empty($_POST['postText'])){
					// $data['status'] = 400;
					// $message = $error_icon."Tagging is required"."<br>";						
					// $data['message'] =$message;
					// echo json_encode($data);	
					// exit();
				}else{
					$post_text = Wo_Secure($_POST['postText']);
					preg_match_all($mention_regex, $post_text, $matches);
					foreach ($matches[1] as $match) {
						$match         = Wo_Secure($match);
						$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
						$match_search  = '@' . $match;
						$match_replace = '@[' . $match_user['user_id'] . ']';
						if (isset($match_user['user_id'])) {
							$post_text  = str_replace($match_search, $match_replace, $post_text);
							$mentions[] = $match_user['user_id'];
						}
					}
					if(sizeof($mentions) < 3){
						$data['status'] = 400;
						$message =$error_icon.$wo['lang']['please_tag_friends']."<br>";						
						$data['message'] = $message;
						echo json_encode($data);	
						exit();
					}
				}
				// Tagging code end;
				$media = array();
				if (!empty($_FILES['image'])) {
					$fileInfo      = array(
						'file' => $_FILES["image"]["tmp_name"],
						'name' => $_FILES['image']['name'],
						'size' => $_FILES["image"]["size"],
						'type' => $_FILES["image"]["type"],
						'types' => 'jpeg,jpg,png,bmp'
					);
					$media  = Wo_ShareFile($fileInfo);
				}
				if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
					if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
						$invalid_file = 1;
					} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
						$invalid_file = 2;
					} else {
						$fileInfo = array(
							'file' => $_FILES["postVideo"]["tmp_name"],
							'name' => $_FILES['postVideo']['name'],
							'size' => $_FILES["postVideo"]["size"],
							'type' => $_FILES["postVideo"]["type"],
							'types' => 'mp4,m4v,webm,flv,mov,mpeg'
						);
						$media    = Wo_ShareFile($fileInfo);
						if (!empty($media)) {
							$mediaFilename = $media['filename'];
							$mediaName     = $media['name'];
							$img_types     = array(
								'image/png',
								'image/jpeg',
								'image/jpg',
								'image/gif'
							);
							if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
								$fileInfo = array(
									'file' => $_FILES["video_thumb"]["tmp_name"],
									'name' => $_FILES['video_thumb']['name'],
									'size' => $_FILES["video_thumb"]["size"],
									'type' => $_FILES["video_thumb"]["type"],
									'types' => 'jpeg,png,jpg,gif',
									'crop' => array(
										'width' => 525,
										'height' => 295
									)
								);
								$media    = Wo_ShareFile($fileInfo);
								if (!empty($media)) {
									$video_thumb = $media['filename'];
								}
							}
						}
					}
				}
				if (isset($_FILES['postMusic']['name']) && empty($mediaFilename)) {
					if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
						$invalid_file = 1;
					} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
						$invalid_file = 2;
					} else {
						$fileInfo = array(
							'file' => $_FILES["postMusic"]["tmp_name"],
							'name' => $_FILES['postMusic']['name'],
							'size' => $_FILES["postMusic"]["size"],
							'type' => $_FILES["postMusic"]["type"],
							'types' => 'mp3,wav'
						);
						$media    = Wo_ShareFile($fileInfo);
						if (!empty($media)) {
							$mediaFilename = $media['filename'];
							$mediaName     = $media['name'];
						}
					}
				}
				if(isset($_POST['challenge_fund_id']) && !empty($_POST['challenge_fund_id'])){
					$challenge_fund_id = $_POST['challenge_fund_id'];
					$challenge_fund = $db->where('id',$challenge_fund_id)->getOne(T_CHALLENGE_FUNDING);
					$challenge_fund->description = Wo_Secure($_POST['description']);
					$challenge_fund->invites = Wo_Secure($post_text);
					$challenge_fund->time = time();
					if(!empty($media)){
						$challenge_fund->file =  $media['file'];							
						$challenge_fund->fileName = $media['fileName'];
					}
					$challenge_fund->status = 2;
					$challenge_fund = json_decode(json_encode($challenge_fund));
					$result = $db->update(T_CHALLENGE_FUNDING, $challenge_fund);
					if(!$result){
						$challenge_fund_id = false;
					}
				}else{
					$insert_array = array(
						'funding_id' => Wo_Secure($_POST['funding_id']),
						'challenge_id' => Wo_Secure($challenge_id),
						'invited_by' => 0,
						'description'   => Wo_Secure($_POST['description']),
						'invites' => Wo_Secure($post_text),
						'time'   => time(),
						'file' => $media['file'],
						'fileName' => $media['fileName'],
						'user_id'  => $wo['user']['id'],
						'status' => 2,
						'hashed_id' => Wo_GenerateKey(15, 15)
					);
					$challenge_fund_id = $db->insert(T_CHALLENGE_FUNDING, $insert_array);

				}
					// 'postRecord' => Wo_Secure($_POST['postRecord']),
				if(!empty($challenge_fund_id)){
					$data['status'] = 200;
					$data['message'] = $wo['lang']['challenge_created'];
					$data['url'] = Wo_SeoLink('index.php?link1=my_challenges');
				
				}else{
					$data['status'] = 400;
					$message = $error_icon . $wo['lang']['please_check_details'] . "Database Error";
					$data['message'] = $message;
				}
				
			} else {
				$data['post_data'] = $_POST;
				$data['files'] = $_FILES;
				$data['message'] = $error_icon . $wo['lang']['please_check_details'];
			}
		}
	}
	// Reject a challenge
	if ($s == 'challenge_reject') 
	{
		global $db;
		$data['data'] = $_POST;
		if (!empty($_POST['challenge_fund_id'])) 
		{
			$chall_fund_id = Wo_Secure($_POST['challenge_fund_id']);
			$challenge_fund = $db->where('id', $chall_fund_id)->getOne(T_CHALLENGE_FUNDING);
			if (!empty($challenge_fund) ) 
			{
				$db->where('id', $chall_fund_id)->update(T_CHALLENGE_FUNDING,['status' =>'5']);
				$db->where('challenge_fund_id', $id)->update(T_POSTS,['active' =>'0']);
				$message = 'Challenge rejected successfully';
				$data['url'] = Wo_SeoLink('index.php?link1=my_funding').'?query=published';
				$data['status'] = 200;
				$data['message'] = $message;
				$notification_data_array = array(
                    'recipient_id' => $challenge_fund->user_id,
                    'type' => 'challenge_rejected',
                    'page_id' => 0,
                    'text' => 'Your challenge has been rejected.',
                    'challenge_funding_id'=>$chall_fund_id,
                    'url' => $wo['config']['site_url'] . '/show_challenge/'.$challenge_fund->hashed_id
                );
				Wo_RegisterNotification($notification_data_array);
			} else {
				$data['message'] = $error_icon . $wo['lang']['please_check_details']."empty";
			}
		} else {
			$data['message'] = $error_icon . $wo['lang']['please_check_details'];
		}
	}
	// Reject a challenge
	if ($s == 'challenge_endorse') 
	{
		global $db;
		$data['data'] = $_POST;
		if (!empty($_POST['challenge_fund_id'])) 
		{
			$chall_fund_id = Wo_Secure($_POST['challenge_fund_id']);
			$comment = $_POST['comment'];
			$challenge_fund = $db->where('id', $chall_fund_id)->getOne(T_CHALLENGE_FUNDING);
			if (!empty($challenge_fund) ) 
			{
				$db->where('id', $chall_fund_id)->update(T_CHALLENGE_FUNDING,['endorsement_comment' =>$comment]);
				/*
		        *Notify challenge owner
		        */
		        $notification_data = array(
		                'recipient_id' => $challenge_fund->user_id,
		                'type' => 'challenge_endorsed',
		                'url' => $wo['config']['site_url'] . '/show_challenge/' . $challenge_fund->hashed_id

		            );
		        Wo_RegisterNotification($notification_data);
				$message = 'Challenge endorsed successfully';
				$data['status'] = 200;
				$data['message'] = $message;
				
			} else {
				$data['message'] = $error_icon . $wo['lang']['please_check_details']."empty";
			}
		} else {
			$data['message'] = $error_icon . $wo['lang']['please_check_details'];
		}
	}
	// unpublish toggle 
	if ($s == 'unpublish_fund') 
	{
		global $db;
		$data['data'] = $_POST;
		if (!empty($_POST['fund_id'])) {
			$id = Wo_Secure($_POST['fund_id']);

			$fund = $db->where('id', $id)->getOne(T_FUNDING);

			if (!empty($fund) ) {
				$message = '';
				// if($fund->status == '4' || $fund->status == '0'){
				// 	$fund->status = '1';
				// 	$db->where('id', $id)->update(T_FUNDING,array('status' =>'1'));
				// 	$db->where('fund_id', $id)->update(T_POSTS,array('active' =>'1'));
				// 	$message = 'Campaign Published';
				// }
				// else
				// {
					$fund->status = '0';
					$db->where('id', $id)->update(T_FUNDING,array('status' =>'0'));
					$db->where('fund_id', $id)->update(T_POSTS,array('active' =>'0'));
					$message = 'Campaign Published';
					Wo_NewCampaignPublish($id);
				//}
				$fund_data = (Array)$fund;
				$data['fund'] = $fund_data;
				//$data['url'] = Wo_SeoLink('index.php?link1=my_funding').'?query=published';
				$data['url'] = 'index.php?link1=campaign_thankyou&id='.$fund->hashed_id;
				
				$data['status'] = 200;
				$data['message'] = $message;
			} else {
				$data['message'] = $error_icon . $wo['lang']['please_check_details']."empty";
			}
		} else {
			$data['message'] = $error_icon . $wo['lang']['please_check_details'];
		}
	}

	if ($s == 'delete_fund') {
		if (!empty($_GET['id'])) {
			$id = Wo_Secure($_GET['id']);
			$fund = $db->where('id', $id)->getOne(T_FUNDING);
			if (!empty($fund) || ($wo['user']['user_id'] != $fund->user_id && Wo_IsAdmin() == false)) {

				@Wo_DeleteFromToS3($fund->image);

				if (file_exists($fund->image)) {
					try {
						unlink($fund->image);
					} catch (Exception $e) {
					}
				}

				$db->where('id', $id)->delete(T_FUNDING);
				$raise = $db->where('funding_id', $id)->get(T_FUNDING_RAISE);
				$db->where('funding_id', $id)->delete(T_FUNDING_RAISE);
				$db->where('funding_id', $id)->delete(T_CHALLENGE_FUNDING);
				$posts = $db->where('fund_id', $id)->get(T_POSTS);
				if (!empty($posts)) {
					foreach ($posts as $key => $value) {
						$db->where('parent_id', $value->id)->delete(T_POSTS);
					}
				}

				$db->where('fund_id', $id)->delete(T_POSTS);
				foreach ($raise as $key => $value) {
					$raise_posts = $db->where('fund_raise_id', $value->id)->get(T_POSTS);
					if (!empty($raise_posts)) {
						foreach ($posts as $key => $value1) {
							$db->where('parent_id', $value1->id)->delete(T_POSTS);
						}
					}
					$db->where('fund_raise_id', $value->id)->delete(T_POSTS);
				}


				$data['status'] = 200;
			} else {
				$data['message'] = $error_icon . $wo['lang']['please_check_details'];
			}
		} else {
			$data['message'] = $error_icon . $wo['lang']['please_check_details'];
		}
	}
	
	if ($s == 'delete_challenge') {
		if (!empty($_GET['id'])) {
			$id = Wo_Secure($_GET['id']);
			$challenge_fund = $db->where('id', $id)->getOne(T_CHALLENGE_FUNDING);
			if (!empty($challenge_fund) || ($wo['user']['user_id'] != $challenge_fund->user_id && Wo_IsAdmin() == false)) {

				@Wo_DeleteFromToS3($fund->file);

				if (file_exists($fund->file)) {
					try {
						unlink($fund->file);
					} catch (Exception $e) {
					}
				}

				$db->where('id', $id)->delete(T_CHALLENGE_FUNDING);
				$posts = $db->where('challenge_fund_id', $id)->get(T_POSTS);
				if (!empty($posts)) {
					foreach ($posts as $key => $value) {
						$db->where('parent_id', $value->id)->delete(T_POSTS);
					}
				}

				$db->where('challenge_fund_id', $id)->delete(T_POSTS);
				$data['status'] = 200;
			} else {
				$data['message'] = $error_icon . $wo['lang']['please_check_details'];
			}
		} else {
			$data['message'] = $error_icon . $wo['lang']['please_check_details'];
		}
	}
	if ($s == 'delete_draft_challenge') {
		if (!empty($_GET['id'])) {
			$id = Wo_Secure($_GET['id']);
			$fund = $db->where('id', $id)->getOne(T_CHALLENGE_FUNDING);
			if (!empty($fund) || ($wo['user']['user_id'] != $fund->user_id && Wo_IsAdmin() == false)) {

				@Wo_DeleteFromToS3($fund->file);

				if (file_exists($fund->file)) {
					try {
						unlink($fund->file);
					} catch (Exception $e) {
					}
				}

				$db->where('id', $id)->delete(T_CHALLENGE_FUNDING);
				$posts = $db->where('challenge_fund_id', $id)->get(T_POSTS);
				if (!empty($posts)) {
					foreach ($posts as $key => $value) {
						$db->where('parent_id', $value->id)->delete(T_POSTS);
					}
				}

				$db->where('challenge_fund_id', $id)->delete(T_POSTS);

				$data['status'] = 200;
			} else {
				$data['message'] = $error_icon . $wo['lang']['please_check_details'];
			}
		} else {
			$data['message'] = $error_icon . $wo['lang']['please_check_details'];
		}
	}
	// 
	if ($s == 'delete_draft_fund') {
		if (!empty($_GET['id'])) {
			$id = Wo_Secure($_GET['id']);
			$fund = $db->where('id', $id)->getOne(T_FUNDING);
			if (!empty($fund) || ($wo['user']['user_id'] != $fund->user_id && Wo_IsAdmin() == false)) {

				@Wo_DeleteFromToS3($fund->image);

				if (file_exists($fund->image)) {
					try {
						unlink($fund->image);
					} catch (Exception $e) {
					}
				}

				$db->where('id', $id)->delete(T_FUNDING);
				$raise = $db->where('funding_id', $id)->get(T_FUNDING_RAISE);
				$db->where('funding_id', $id)->delete(T_FUNDING_RAISE);
				$posts = $db->where('fund_id', $id)->get(T_POSTS);
				if (!empty($posts)) {
					foreach ($posts as $key => $value) {
						$db->where('parent_id', $value->id)->delete(T_POSTS);
					}
				}

				$db->where('fund_id', $id)->delete(T_POSTS);
				foreach ($raise as $key => $value) {
					$raise_posts = $db->where('fund_raise_id', $value->id)->get(T_POSTS);
					if (!empty($raise_posts)) {
						foreach ($posts as $key => $value1) {
							$db->where('parent_id', $value1->id)->delete(T_POSTS);
						}
					}
					$db->where('fund_raise_id', $value->id)->delete(T_POSTS);
				}

				$data['status'] = 200;
			} else {
				$data['message'] = $error_icon . $wo['lang']['please_check_details'];
			}
		} else {
			$data['message'] = $error_icon . $wo['lang']['please_check_details'];
		}
	}
	if ($s == 'edit_funding') {
		$data['status'] = 400;
		//echo "<pre>";print_r($_POST);die;
		if(!isset($_POST['type']) && empty($_POST['type']))
		{
			if (!empty($_POST['title'])  && !empty($_POST['description']) && !empty($_POST['amount']) && is_numeric($_POST['amount']) && $_POST['amount'] > 0 ) 
			{
				$flag='0';
				
				$id = Wo_Secure($_POST['id']);
				$fund = $db->where('id', $id)->getOne(T_FUNDING);
				$post = $db->where('fund_id',$id)->getOne(T_POSTS);
				$postMusic = $db->where('is_music',1)->where('fund_id',$id)->getOne(T_POSTS);

				$posts = $db->where('fund_id',$id)->get(T_POSTS);
				$mention_regex = '/@([A-Za-z0-9_]+)/i';
				// if (!empty($fund) || ($wo['user']['user_id'] != $fund->user_id && Wo_IsAdmin() == false)) {
				$fundraising_for=$_POST['fundraising_for'];
				$bank_name='';
				$bank_account_name='';
				$bank_account_no='';

				if($fundraising_for=='2')
				{
					$beneficiary = Wo_Secure($_POST['beneficiary2']);
				}
				elseif($fundraising_for=='1')
				{
					if(empty($_POST['bank_name']))
					{
						$flag ='1';
						$message = $error_icon . $wo['lang']['bank_name_required'];
					}
					if(empty($_POST['bank_account_name']))
					{
						$flag ='1';
						$message = $error_icon . $wo['lang']['bank_account_name_required'];
					}
					if(empty($_POST['bank_account_no']))
					{
						$flag ='1';
						$message = $error_icon . $wo['lang']['bank_account_no_required'];
					}
					if($flag=='1')
					{
						$data['status'] = 400;
						$data['message'] = $message;
						echo json_encode($data);
						exit();
					}
					$bank_name=$_POST['bank_name'];
					$bank_account_name=$_POST['bank_account_name'];
					$bank_account_no=$_POST['bank_account_no'];

					$beneficiary = Wo_Secure($_POST['beneficiary1']);
				}
				if (!empty($fund)) 
				{
					if (empty($beneficiary)) {
						$data['status'] = 400;
						$message = $error_icon . "Beneficiary is required" . "<br>";
						$data['message'] = $message;
						echo json_encode($data);
						exit();
					} 
					else 
					{
						
						
						if($fundraising_for=='2')
						{
							preg_match_all($mention_regex, $beneficiary, $matches);
							foreach ($matches[1] as $match) {
								$match         = Wo_Secure($match);
								$match_user    = Wo_PageData(Wo_PageIdFromPagename($match));
								$match_search  = '@' . $match;
								$match_replace = '@[' . $match_user['page_id'] . ']';
								if (isset($match_user['page_id'])) {
									$beneficiary  = str_replace($match_search, $match_replace, $beneficiary);
									$mentions[] = $match_user['page_id'];
								}
							}
						}
						else
						{
							preg_match_all($mention_regex, $beneficiary, $matches);
							foreach ($matches[1] as $match) {
								$match         = Wo_Secure($match);
								$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
								$match_search  = '@' . $match;
								$match_replace = '@[' . $match_user['user_id'] . ']';
								if (isset($match_user['user_id'])) {
									$beneficiary  = str_replace($match_search, $match_replace, $beneficiary);
									$mentions[] = $match_user['user_id'];
								}
							}
						}
						$data['beneficiaryText'] = $beneficiary;
						if (sizeof($mentions) != 1) {
							$data['status'] = 400;
							$message = $error_icon . "beneficiary must be 1 person " . "<br>";
							$data['message'] = $message;
							$data['beneficiary'] =sizeof($mentions);
							echo json_encode($data);
							exit();
						}
					}
					if(isset($_POST['funding_admins']) && !empty($_POST['funding_admins'])){	
						$funding_admins = Wo_Secure($_POST['funding_admins']);
						preg_match_all($mention_regex, $funding_admins, $matches);
						foreach ($matches[1] as $match) {
							$match         = Wo_Secure($match);
							$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
							$match_search  = '@' . $match;
							$match_replace = '@[' . $match_user['user_id'] . ']';
							if (isset($match_user['user_id'])) {
								$funding_admins  = str_replace($match_search, $match_replace, $funding_admins);
								$fundingAdmins[] = $match_user['user_id'];
							}
						}
					}
					if (isset($_POST['postTextCategory']) && !empty($_POST['postTextCategory'])) {
						/*$text = $_POST['postTextCategory'];

						$hashtag_regex = '/(#\[([0-9]+)\])/i';
						preg_match_all($hashtag_regex, $text, $matches);
						$match_i = 0;
						foreach ($matches[1] as $match) {
							$hashkey  = $matches[2][$match_i];
							if (!empty($hashkey)) {
								$db->where('id', $hashkey)->update(T_HASHTAGS, array('trend_use_num' => $db->dec(1)));
							}
							$match_i++;
						}
						$post_text = $post_text . " " . $text;*/
						$categories=json_encode($_POST['postTextCategory']);
					}

					$media = array();
					if (!empty($_FILES['image'])) {
						$fileInfo      = array(
							'file' => $_FILES["image"]["tmp_name"],
							'name' => $_FILES['image']['name'],
							'size' => $_FILES["image"]["size"],
							'type' => $_FILES["image"]["type"],
							'types' => 'jpeg,jpg,png,bmp'
						);
						$media  = Wo_ShareFile($fileInfo);
					}

					$fund->title =Wo_Secure($_POST['title']);
					$fund->description =$_POST['description'];
					$fund->amount =Wo_Secure($_POST['amount']);
					$fund->beneficiary =$beneficiary;
					$fund->funding_admins =$funding_admins;
					$fund->category =$categories;
					$fund->fundraising_for= $fundraising_for;
					$fund->status= '9';
					$fund->bank_name= $bank_name;
					$fund->bank_account_name= $bank_account_name;
					$fund->bank_account_no= $bank_account_no;

					if(!empty($media) && !empty($media['filename'])){
						$fund->image =$media['filename'];
					}

					$due_date = Wo_Secure($_POST['due_date']);
					if ($due_date) {
						$fund->due_date = $due_date;
					}
					$fundData = json_decode(json_encode($fund), true);

					$updatedFund = $db->where('id', $id)->update(T_FUNDING, $fundData);
					if($updatedFund)
					{
							//multiple options uploads
							// $postPhotos = "";
							// $postVideo = "";
							$mediaFilename = '';
							$post_photo    = '';
							$mediaName     = '';
							$video_thumb   = '';
							$album_name = '';
							$blur = 0;

							if (!empty($_POST['album_name'])) {
								$album_name = $_POST['album_name'];
							}
							if (!isset($_FILES['postPhotos']['name'])) {
								$album_name = '';
							}
							$multi = 0;

							if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) {
								// if (count($_FILES['postPhotos']['name']) == 1) {
								// 	if(!empty($post->postFile)){
								// 		if ($_FILES['postPhotos']['size'][0] > $wo['config']['maxUpload']) {
								// 			$invalid_file = 1;
								// 		} else if (Wo_IsFileAllowed($_FILES['postPhotos']['name'][0]) == false) {
								// 			$invalid_file = 2;
								// 		} else {

								// 			$fileInfo = array(
								// 				'file' => $_FILES["postPhotos"]["tmp_name"][0],
								// 				'name' => $_FILES['postPhotos']['name'][0],
								// 				'size' => $_FILES["postPhotos"]["size"][0],
								// 				'type' => $_FILES["postPhotos"]["type"][0]
								// 			);
								// 			$media    = Wo_ShareFile($fileInfo);
								// 			if (!empty($media)) {
								// 				$image_file = Wo_GetMedia($media['filename']);
								// 				$upload = true;
								// 				if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
								// 					$blur = 1;
								// 				} elseif ($wo['config']['adult_images'] == 1  && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
								// 					$invalid_file = 3;
								// 					$upload = false;
								// 					@unlink($media['filename']);
								// 					Wo_DeleteFromToS3($media['filename']);
								// 				}
								// 				$mediaFilename = $media['filename'];
								// 				$mediaName     = $media['name'];
								// 			}
								// 		}
								// 	}else{
								// 		$multi = 1;
								// 		$data['multi'] = $multi;
								// 	}
								// } else {
									$multi = 1;
									$data['multi'] = $multi;
								// }
							}
							if (isset($_FILES['postPhotos']['name'])) {
								$allowed = array(
									'gif',
									'png',
									'jpg',
									'jpeg'
								);
								for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
									$new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
									if (!in_array(strtolower($new_string['extension']), $allowed)) {
										$errors[] = $error_icon . $wo['lang']['please_check_details'];
									}
								}
							}
							if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
								$headers = get_headers($_POST['postSticker'], 1);
								if (strpos($headers['Content-Type'], 'image/') !== false) {
									$post_data['postSticker'] = $_POST['postSticker'];
								} else {
									$invalid_file = 2;
								}
							} else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
								if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
									$post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
								}
							}

							if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
								if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
									$invalid_file = 1;
								} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
									$invalid_file = 2;
								} else {
									$fileInfo = array(
										'file' => $_FILES["postVideo"]["tmp_name"],
										'name' => $_FILES['postVideo']['name'],
										'size' => $_FILES["postVideo"]["size"],
										'type' => $_FILES["postVideo"]["type"],
										'types' => 'mp4,m4v,webm,flv,mov,mpeg'
									);
									$media    = Wo_ShareFile($fileInfo);
									if (!empty($media)) {
										$mediaFilename = $media['filename'];
										$mediaName     = $media['name'];
										$img_types     = array(
											'image/png',
											'image/jpeg',
											'image/jpg',
											'image/gif'
										);
										
										if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
											$fileInfo = array(
												'file' => $_FILES["video_thumb"]["tmp_name"],
												'name' => $_FILES['video_thumb']['name'],
												'size' => $_FILES["video_thumb"]["size"],
												'type' => $_FILES["video_thumb"]["type"],
												'types' => 'jpeg,png,jpg,gif',
												'crop' => array(
													'width' => 525,
													'height' => 295
												)
											);
											$media   = Wo_ShareFile($fileInfo);
											if (!empty($media)) {
												$video_thumb = $media['filename'];
											}
										}
									}
								}
							}
						
							
							if (isset($_FILES['postMusic']['name'])) {
								if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
									$invalid_file = 1;
								} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
									$invalid_file = 2;
								} else {
									$fileInfo = array(
										'file' => $_FILES["postMusic"]["tmp_name"],
										'name' => $_FILES['postMusic']['name'],
										'size' => $_FILES["postMusic"]["size"],
										'type' => $_FILES["postMusic"]["type"],
										'types' => 'mp3,wav'
									);
									$media    = Wo_ShareFile($fileInfo);
									if (!empty($media)) {
										$mediaFilenameMusic = $media['filename'];
										$mediaNameMusic     = $media['name'];
									}
								}
							}
							
							$post->postText = Wo_Secure($post_text);
							if($multi == 1){
								$post->multi_image = Wo_Secure($multi);
							}
						
							if(!empty($mediaFilename)){
								$post->postFile = Wo_Secure($mediaFilename, 0);
							}
							if(!empty($mediaName)){
								$post->postFileName = Wo_Secure($mediaName);
							}
							if(!empty($video_thumb)){
								$post->postFileThumb = Wo_Secure($video_thumb);
							}
							
							if($blue == 1){
								$post->blur = $blur;
							}
							if(isset($_POST['postRecord'])){
								$post->postRecord=Wo_Secure($_POST['postRecord']);
							}
							$post->fund_id = $id;
						
							$post_data = json_decode(json_encode($post), true);
							$updatedPost = $db->where('id', $post_data['post_id'])->update(T_POSTS, $post_data);
							$id = $post_data['post_id'];
							unset($post_data['id']);
							unset($post_data['post_id']);
							unset($post_data['parent_id']);
							// $data['post_data'] = $post_data;
							// $data['update'] = $updatedPost;
							// $data['id'] = $id;
							// echo json_encode($data);
							// exit();
					
							if($id && isset($mediaFilenameMusic) && isset($mediaNameMusic) && isset($_FILES['postMusic']['name'])  ){
								if(!$postMusic){
									$post_data['postFile'] = Wo_Secure($mediaFilenameMusic, 0);
									$post_data['postFileName'] = Wo_Secure($mediaNameMusic);
									$post_data['is_music'] = 1;
									Wo_RegisterPost($post_data);
									unset($post_data['is_music']);

								}else{
									$post_music_data = json_decode(json_encode($postMusic), true);
									$post_music_data['postFile'] = Wo_Secure($mediaFilenameMusic, 0);
									$post_music_data['postFileName'] = Wo_Secure($mediaNameMusic);
									$updatedPostMusic = $db->where('id', $postMusic->post_id)->update(T_POSTS, $post_music_data);

								}
						
							}
							if (isset($_FILES['postPhotos']['name'])) {						
								if (count($_FILES['postPhotos']['name']) > 0) {
									// foreach($posts as $imgpost){
									// 	if($imgpost->multi_image != 1){
									// 		Wo_DeletePost($imgpost->id);
									// 	}
									// }
									// $data['imageUpload'] = 'photos > 0';
									for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
										$fileInfo = array(
											'file' => $_FILES["postPhotos"]["tmp_name"][$i],
											'name' => $_FILES['postPhotos']['name'][$i],
											'size' => $_FILES["postPhotos"]["size"][$i],
											'type' => $_FILES["postPhotos"]["type"][$i],
											'types' => 'jpg,png,jpeg,gif'
										);
										$file     = Wo_ShareFile($fileInfo, 1);
										$image_file = Wo_GetMedia($file['filename']);
										if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
											$blur = 1;
										} elseif ($wo['config']['adult_images'] == 1 && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
											$invalid_file = 3;
											$errors[] = $error_icon . $wo['lang']['adult_image_file'];
											Wo_DeletePost($id);
											@unlink($file['filename']);
											Wo_DeleteFromToS3($file['filename']);
										}
										if (!empty($file)) {
											$media_album = Wo_RegisterAlbumMedia($id, $file['filename']);
											$post_data['multi_image'] = 0;
											$post_data['multi_image_post'] = 1;
											$post_data['album_name'] = '';
											$post_data['postFile'] = $file['filename'];
											$post_data['postFileName'] = $file['name'];
											$post_data['active'] = 1;
											$new_id = Wo_RegisterPost($post_data);
											$media_album = Wo_RegisterAlbumMedia($new_id, $file['filename'], $id);
											$data['image-uploaded'.$i] = 'uploaded' . $new_id . "media_album" . $media_album;
										}
										$data['i-' . $i] = 'uploaded';
									}
								}
							}

						$data['status'] = 200;
						$data['message'] = $wo['lang']['funding_edited'];
						//$data['url'] = $wo['config']['site_url'] . '/show_fund/' . $fund->hashed_id;
                                                
                        if($fund->user_id == $wo['user']['id']){
                            $data['url'] = Wo_SeoLink('index.php?link1=my_funding').'?query=published';
                        }else{
                            $data['url'] = Wo_SeoLink('index.php?link1=my_funding').'?query=assigned';
                        }
					} 
				} else {
					$data['message'] = $error_icon . $wo['lang']['please_check_details']."yes";
				}
			} else {
				$data['message'] = $error_icon . $wo['lang']['please_check_details']."no";
			}
		}
		else
		{
			// publishing saved draft funding here sameer
			if (!empty($_POST['title'])  && !empty($_POST['description']) && !empty($_POST['amount']) && is_numeric($_POST['amount']) && $_POST['amount'] > 0) {
				$id = Wo_Secure($_POST['id']);
				$fund = $db->where('id', $id)->getOne(T_FUNDING_DRAFT);
				$post = $db->where('fund_draft_id',$id)->getOne(T_POSTS);
				$postMusic = $db->where('is_music',1)->where('fund_draft_id',$id)->getOne(T_POSTS);
				$mention_regex = '/@([A-Za-z0-9_]+)/i';
				// if (!empty($fund) || ($wo['user']['user_id'] != $fund->user_id && Wo_IsAdmin() == false)) {
				$fundraising_for=$_POST['fundraising_for'];
				if($fundraising_for=='2')
				{
					$beneficiary = Wo_Secure($_POST['beneficiary2']);
				}
				else
				{
					$beneficiary = Wo_Secure($_POST['beneficiary1']);
				}
				if (!empty($fund)) {
					if (empty($beneficiary)) {
						$data['status'] = 400;
						$message = $error_icon . "Beneficiary is required" . "<br>";
						$data['message'] = $message;
						echo json_encode($data);
						exit();
					} 
					else 
					{
						if($fundraising_for=='2')
						{
							preg_match_all($mention_regex, $beneficiary, $matches);
							foreach ($matches[1] as $match) {
								$match         = Wo_Secure($match);
								$match_user    = Wo_PageData(Wo_PageIdFromPagename($match));
								$match_search  = '@' . $match;
								$match_replace = '@[' . $match_user['page_id'] . ']';
								if (isset($match_user['page_id'])) {
									$beneficiary  = str_replace($match_search, $match_replace, $beneficiary);
									$mentions[] = $match_user['page_id'];
								}
							}
						}
						else
						{
							preg_match_all($mention_regex, $beneficiary, $matches);
							foreach ($matches[1] as $match) {
								$match         = Wo_Secure($match);
								$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
								$match_search  = '@' . $match;
								$match_replace = '@[' . $match_user['user_id'] . ']';
								if (isset($match_user['user_id'])) {
									$beneficiary  = str_replace($match_search, $match_replace, $beneficiary);
									$mentions[] = $match_user['user_id'];
								}
							}
						}
						$data['beneficiaryText'] = $beneficiary;
						if (sizeof($mentions) != 1) {
							$data['status'] = 400;
							$message = $error_icon . "beneficiary must be 1 person " . "<br>";
							$data['message'] = $message;
							$data['beneficiary'] =sizeof($mentions);
							echo json_encode($data);
							exit();
						}
					}
					$funding_admins = "";
					if(isset($_POST['funding_admins']) && !empty($_POST['funding_admins'])){	
						$funding_admins = Wo_Secure($_POST['funding_admins']);
						preg_match_all($mention_regex, $funding_admins, $matches);
						foreach ($matches[1] as $match) {
							$match         = Wo_Secure($match);
							$match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
							$match_search  = '@' . $match;
							$match_replace = '@[' . $match_user['user_id'] . ']';
							if (isset($match_user['user_id'])) {
								$funding_admins  = str_replace($match_search, $match_replace, $funding_admins);
								$fundingAdmins[] = $match_user['user_id'];
							}
						}
					}
					if (isset($_POST['postTextCategory']) && !empty($_POST['postTextCategory'])) {
						/*$text = $_POST['postTextCategory'];

						$hashtag_regex = '/(#\[([0-9]+)\])/i';
						preg_match_all($hashtag_regex, $text, $matches);
						$match_i = 0;
						foreach ($matches[1] as $match) {
							$hashkey  = $matches[2][$match_i];
							if (!empty($hashkey)) {
								$db->where('id', $hashkey)->update(T_HASHTAGS, array('trend_use_num' => $db->dec(1)));
							}
							$match_i++;
						}
						$post_text = $post_text . " " . $text;*/
						$categories=json_encode($_POST['postTextCategory']);
					}

					$media = array();
					if (!empty($_FILES['image'])) {
						$fileInfo      = array(
							'file' => $_FILES["image"]["tmp_name"],
							'name' => $_FILES['image']['name'],
							'size' => $_FILES["image"]["size"],
							'type' => $_FILES["image"]["type"],
							'types' => 'jpeg,jpg,png,bmp'
						);
						$media  = Wo_ShareFile($fileInfo);
					}
					// $fund = array(
					// 	'title' => Wo_Secure($_POST['title']),
					// 	'description'   => Wo_Secure($_POST['description']),
					// 	'amount'   => Wo_Secure($_POST['amount']),
					// 	'time'   => time(),
					// 	'image' => $media['filename'],
					// 	'video' => "",
					// 	'beneficiary' => $beneficiary,
					// 	'funding_admins' => $funding_admins,
					// 	'user_id'  => $wo['user']['id'],
					// 	'hashed_id' => Wo_GenerateKey(15, 15)
					// );
					
					$fund->time = time();
					$fund->user_id  = $wo['user']['id'];
					$fund->hashed_id =  Wo_GenerateKey(15, 15);
					$fund->title =Wo_Secure($_POST['title']);
					$fund->description =$_POST['description'];
					$fund->amount =Wo_Secure($_POST['amount']);
					$fund->beneficiary =$beneficiary;
					$fund->funding_admins =$funding_admins;
					$fund->category =$categories;
					$fund->fundraising_for= $fundraising_for;
			
					if(!empty($media) && !empty($media['filename'])){
						$fund->image =$media['filename'];
					}

					$due_date = Wo_Secure($_POST['due_date']);
					if (!empty($due_date)) {
						$fund->due_date = $due_date;
					}
					$fundData = json_decode(json_encode($fund), true);
					
					
					unset($fundData['id']);
					unset($fundData['type']);
					unset($fundData['status']);
					
					$fund_id = $db->insert(T_FUNDING, $fundData);
					// $data['update'] = $fund_id;
					// $data['funddata'] = $fundData;
					// echo json_encode($data);
					// exit();
					$draftFundId = $fund->id;
					$isdraftUpdated = $db->where('id',$fund->id)->update(T_FUNDING_DRAFT,['status' => 0]);

					// $updatedFund = $db->where('id', $id)->update(T_FUNDING, $fundData);
					if($fund_id && $isdraftUpdated){
							//multiple options uploads
							// $postPhotos = "";
							// $postVideo = "";
							$mediaFilename = '';
							$post_photo    = '';
							$mediaName     = '';
							$video_thumb   = '';
							$album_name = '';
							$blur = 0;
							$post_data = json_decode(json_encode($post), true);

							if (!empty($_POST['album_name'])) {
								$album_name = $_POST['album_name'];
							}
							if (!isset($_FILES['postPhotos']['name'])) {
								$album_name = '';
							}
							$multi = 0;

							if (isset($_FILES['postPhotos']['name']) && empty($mediaFilename) && empty($_POST['album_name'])) {
								// if (count($_FILES['postPhotos']['name']) == 1) {
								// 	if ($_FILES['postPhotos']['size'][0] > $wo['config']['maxUpload']) {
								// 		$invalid_file = 1;
								// 	} else if (Wo_IsFileAllowed($_FILES['postPhotos']['name'][0]) == false) {
								// 		$invalid_file = 2;
								// 	} else {

								// 		$fileInfo = array(
								// 			'file' => $_FILES["postPhotos"]["tmp_name"][0],
								// 			'name' => $_FILES['postPhotos']['name'][0],
								// 			'size' => $_FILES["postPhotos"]["size"][0],
								// 			'type' => $_FILES["postPhotos"]["type"][0]
								// 		);
								// 		$media    = Wo_ShareFile($fileInfo);
								// 		if (!empty($media)) {
								// 			$image_file = Wo_GetMedia($media['filename']);
								// 			$upload = true;
								// 			if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
								// 				$blur = 1;
								// 			} elseif ($wo['config']['adult_images'] == 1  && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
								// 				$invalid_file = 3;
								// 				$upload = false;
								// 				@unlink($media['filename']);
								// 				Wo_DeleteFromToS3($media['filename']);
								// 			}
								// 			$mediaFilename = $media['filename'];
								// 			$mediaName     = $media['name'];
								// 		}
								// 	}
								// } else {
									$multi = 1;
									$data['multi'] = $multi;
								// }
							}
							if (isset($_FILES['postPhotos']['name'])) {
								$allowed = array(
									'gif',
									'png',
									'jpg',
									'jpeg'
								);
								for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
									$new_string = pathinfo($_FILES['postPhotos']['name'][$i]);
									if (!in_array(strtolower($new_string['extension']), $allowed)) {
										$errors[] = $error_icon . $wo['lang']['please_check_details'];
									}
								}
							}
							if (isset($_POST['postSticker']) && Wo_IsUrl($_POST['postSticker']) && empty($_FILES) && empty($_POST['postRecord'])) {
								$headers = get_headers($_POST['postSticker'], 1);
								if (strpos($headers['Content-Type'], 'image/') !== false) {
									$post_data['postSticker'] = $_POST['postSticker'];
								} else {
									$invalid_file = 2;
								}
							} else if (empty($_FILES['postPhotos']) && preg_match_all('/https?:\/\/(?:[^\s]+)\.(?:png|jpg|gif|jpeg)/', $post_data['postText'], $matches)) {
								if (!empty($matches[0][0]) && Wo_IsUrl($matches[0][0])) {
									$post_data['postPhoto'] = @Wo_ImportImageFromUrl($matches[0][0]);
								}
							}

							if (isset($_FILES['postVideo']['name']) && empty($mediaFilename)) {
								if ($_FILES['postVideo']['size'] > $wo['config']['maxUpload']) {
									$invalid_file = 1;
								} else if (Wo_IsFileAllowed($_FILES['postVideo']['name']) == false) {
									$invalid_file = 2;
								} else {
									$fileInfo = array(
										'file' => $_FILES["postVideo"]["tmp_name"],
										'name' => $_FILES['postVideo']['name'],
										'size' => $_FILES["postVideo"]["size"],
										'type' => $_FILES["postVideo"]["type"],
										'types' => 'mp4,m4v,webm,flv,mov,mpeg'
									);
									$media    = Wo_ShareFile($fileInfo);
									if (!empty($media)) {
										$mediaFilename = $media['filename'];
										$mediaName     = $media['name'];
										$img_types     = array(
											'image/png',
											'image/jpeg',
											'image/jpg',
											'image/gif'
										);
										if (!empty($_FILES['video_thumb']) && in_array($_FILES["video_thumb"]["type"], $img_types)) {
											$fileInfo = array(
												'file' => $_FILES["video_thumb"]["tmp_name"],
												'name' => $_FILES['video_thumb']['name'],
												'size' => $_FILES["video_thumb"]["size"],
												'type' => $_FILES["video_thumb"]["type"],
												'types' => 'jpeg,png,jpg,gif',
												'crop' => array(
													'width' => 525,
													'height' => 295
												)
											);
											$media   = Wo_ShareFile($fileInfo);
											if (!empty($media)) {
												$video_thumb = $media['filename'];
											}
										}
									}
								}
							}
							if (isset($_FILES['postMusic']['name']) ) {
								if ($_FILES['postMusic']['size'] > $wo['config']['maxUpload']) {
									$invalid_file = 1;
								} else if (Wo_IsFileAllowed($_FILES['postMusic']['name']) == false) {
									$invalid_file = 2;
								} else {
									$fileInfo = array(
										'file' => $_FILES["postMusic"]["tmp_name"],
										'name' => $_FILES['postMusic']['name'],
										'size' => $_FILES["postMusic"]["size"],
										'type' => $_FILES["postMusic"]["type"],
										'types' => 'mp3,wav'
									);
									$media    = Wo_ShareFile($fileInfo);
									if (!empty($media)) {
										$mediaFilenameMusic = $media['filename'];
										$mediaNameMusic    = $media['name'];
									}
								}
							}

							$post->fund_id = $fund_id;
							$post->postText = Wo_Secure($post_text);
							if($multi == 1){
								$post->multi_image = Wo_Secure($multi);
							}
							if(!empty($mediaFileame)){
								$post->postFile = Wo_Secure($mediaFilename, 0);
							}
							if(!empty($mediaName)){
								$post->postFileName = Wo_Secure($mediaName);
							}
							if(!empty($video_thumb)){
								$post->postFileThumb = Wo_Secure($video_thumb);
							}
							if($blue == 1){
								$post->blur = $blur;
							}
							if(isset($_POST['postRecord'])){
								$post->postRecord=Wo_Secure($_POST['postRecord']);
							}

							$post->fund_draft_id = 0;
							$post_data = json_decode(json_encode($post), true);
							$updatedPost = $db->where('id', $post->post_id)->update(T_POSTS, $post_data);
							// $data['update'] = $updatedPost;
							// $data['post_id'] = $post->post_id;
							// $data['postd'] = $post;
							// echo json_encode($data);
							// exit();
							$id = $post->id;
							unset($post_data['id']);
							unset($post_data['post_id']);
							unset($post_data['parent_id']);

							if($id && isset($mediaFilenameMusic) && isset($mediaNameMusic) && isset($_FILES['postMusic']['name']) ){
								if(!$postMusic){
									
									$post_data['postFile'] = Wo_Secure($mediaFilenameMusic, 0);
									$post_data['postFileName'] = Wo_Secure($mediaNameMusic);
									$post_data['is_music'] = 1;

									Wo_RegisterPost($post_data);
									unset($post_data['is_music']);

								}else{
									$postMusic->fund_draft_id = 0;
									$postMusic->fund_id = $fund_id;
									$post_music_data = json_decode(json_encode($postMusic), true);
									$post_music_data['postFile'] = Wo_Secure($mediaFilenameMusic, 0);
									$post_music_data['postFileName'] = Wo_Secure($mediaNameMusic);
									$updatedPostMusic = $db->where('id', $postMusic->post_id)->update(T_POSTS, $post_music_data);

								}
							}

							if (isset($mentions) && is_array($mentions)) {
								foreach ($mentions as $mention) {
									if (empty($wo['no_mention']) || (!empty($wo['no_mention']) && !in_array($mention, $wo['no_mention']))) {
										$notification_data_array = array(
											'recipient_id' => $mention,
											'type' => 'post_mention',
											'page_id' => 0,
											'post_id' => $id,
											'url' => 'index.php?link1=show_fund&id=' . $insert_array['hashed_id']
										);
										Wo_RegisterNotification($notification_data_array);
									}
								}
							}
							if (isset($_FILES['postPhotos']['name'])) {						
								if (count($_FILES['postPhotos']['name']) > 0) {
									// foreach($posts as $imgpost){
									// 	if($imgpost->multi_image != 1){
									// 		Wo_DeletePost($imgpost->id);
									// 	}
									// }
									$data['imageUpload'] = 'photos > 0';
									for ($i = 0; $i < count($_FILES['postPhotos']['name']); $i++) {
										$fileInfo = array(
											'file' => $_FILES["postPhotos"]["tmp_name"][$i],
											'name' => $_FILES['postPhotos']['name'][$i],
											'size' => $_FILES["postPhotos"]["size"][$i],
											'type' => $_FILES["postPhotos"]["type"][$i],
											'types' => 'jpg,png,jpeg,gif'
										);
										$file     = Wo_ShareFile($fileInfo, 1);
										$image_file = Wo_GetMedia($file['filename']);
										if ($wo['config']['adult_images'] == 1  && !detect_safe_search($image_file) && $wo['config']['adult_images_action'] == 1) {
											$blur = 1;
										} elseif ($wo['config']['adult_images'] == 1 && detect_safe_search($image_file) == false && $wo['config']['adult_images_action'] == 0) {
											$invalid_file = 3;
											$errors[] = $error_icon . $wo['lang']['adult_image_file'];
											Wo_DeletePost($id);
											@unlink($file['filename']);
											Wo_DeleteFromToS3($file['filename']);
										}
										if (!empty($file)) {
											$media_album = Wo_RegisterAlbumMedia($id, $file['filename']);
											$post_data['multi_image'] = 0;
											$post_data['multi_image_post'] = 1;
											$post_data['album_name'] = '';
											$post_data['postFile'] = $file['filename'];
											$post_data['postFileName'] = $file['name'];
											$post_data['active'] = 1;
											$new_id = Wo_RegisterPost($post_data);
											$media_album = Wo_RegisterAlbumMedia($new_id, $file['filename'], $id);
											$data['image-uploaded'] = 'uploaded' . $new_id . "media_album" . $media_album;
										}
										$data['i-' . $i] = 'uploaded';
									}
								}
							}

							// $posts = $db->where('fund_draft_id',$id)->get(T_POSTS);

							// foreach($posts as $post){
							// 	$post->fund_draft_id = 0;
							// 	$post->fund_id = $fund_id;
							// 	$post_data1 = json_decode(json_encode($post), true);
							// 	$db->where('id', $post->post_id)->update(T_POSTS, $post_data1);
							// }
						$update_data = array('fund_draft_id' => 0,'fund_id' => $fund_id);
						$data['updated'] = $db->where('fund_draft_id', $draftFundId)->update(T_POSTS, $update_data);


						$data['status'] = 200;
						$data['message'] = $wo['lang']['funding_edited'];
						$data['url'] = $wo['config']['site_url'] . '/show_fund/' . $fund->hashed_id;
					} 
				} else {
					$data['message'] = $error_icon . $wo['lang']['please_check_details'];
				}
			} else {
				$data['message'] = $error_icon . $wo['lang']['please_check_details'];
			}
			// publishing saved draft funding here sameer end
		}
	}

	if ($s == 'load_user_fund') {

		if (!empty($_GET['offset']) && is_numeric($_GET['offset']) && $_GET['offset'] > 0) {
			$id = Wo_Secure($_GET['offset']);
			$user_id = $wo['user']['id'];
			if (!empty($_GET['user_id']) && is_numeric($_GET['user_id']) && $_GET['user_id'] > 0) {
				$user_id = Wo_Secure($_GET['user_id']);
			}

			$funding = GetFundingByUserId($user_id, 9, $id,$_GET['query']);

			$html = '';
			if (!empty($funding)) {
				foreach ($funding as $key => $wo['fund']) {
					if($_GET['type'] == 1){
						if(isset($wo['fund']['type'])){
							$html .= Wo_LoadPage('my_funding/list');						
						}
					}else{
						if(!isset($wo['fund']['type'])){
							$html .= Wo_LoadPage('my_funding/list');						
						}
					}
				}
			}
			$data['status'] = 200;
			$data['html'] = $html;
		}
	}
	
	// load user fund
	if ($s == 'load_user_challenge_fund') {

		if (!empty($_GET['offset']) && is_numeric($_GET['offset']) && $_GET['offset'] > 0) {
			$id = Wo_Secure($_GET['offset']);
			$user_id = $wo['user']['id'];
			if (!empty($_GET['user_id']) && is_numeric($_GET['user_id']) && $_GET['user_id'] > 0) {
				$user_id = Wo_Secure($_GET['user_id']);
			}

			$funding = GetChallengesByUserId($user_id, 9, $id);

			$html = '';
			if (!empty($funding)) {
				foreach ($funding as $key => $wo['challenge_fund']) {
					$html .= Wo_LoadPage('my_challenges/list');
				}
			}
			$data['status'] = 200;
			$data['html'] = $html;
		}
	}
	if ($s == 'load_fund') {

		if (!empty($_GET['offset']) && is_numeric($_GET['offset']) && $_GET['offset'] > 0) {
			$id = Wo_Secure($_GET['offset']);

			$funding = GetFunding(10, $id);

			$html = '';
			if (!empty($funding)) {
				foreach ($funding as $key => $wo['fund']) {
					$html .= Wo_LoadPage('funding/list');
				}
			}
			$data['status'] = 200;
			$data['html'] = $html;
		}
	}

	
	// function by load challenges
	if ($s == 'load_challenge_fund') {

		if (!empty($_GET['offset']) && is_numeric($_GET['offset']) && $_GET['offset'] > 0) {
			$id = Wo_Secure($_GET['offset']);

			$funding = GetChallenges(10, $id);

			$html = '';
			if (!empty($funding)) {
				foreach ($funding as $key => $wo['challenge_fund']) {
					$html .= Wo_LoadPage('challenges/list');
				}
			}
			$data['status'] = 200;
			$data['html'] = $html;
		}
	}
	//get funding details
	if ($s == 'get_funding_by_id') {

		if (!empty($_POST['campaign_id']) && is_numeric($_POST['campaign_id']) && $_POST['campaign_id'] > 0) {
			$id = Wo_Secure($_POST['campaign_id']);
			
			$fund = GetFundingById($id);
			$html = '';
			if (!empty($fund)) {
	
					
					$beneficiary =   Wo_Markup($fund['beneficiary'],false,false,true,0,0,0,true);
					$html .= '
                                            <p>' . $wo['lang']['campaign_chosen'] . '</p>
                                            <input type="hidden" name="fund_campaign_id" value="'.$fund['id'].'" />
                                            <div class="col-md-8 mx-auto">
                                            <div class="wow_content fundings">
                                                <div class="avatar">
                                                    <img src="'.$fund['image'].'" alt="' . $fund['title'] . '">                                                    
                                                </div>
                                                <div class="fundings_desc">
                                                    <h3>' . $fund['title'] . '</h3>
                                                        
                                                    <div class="campaign_users_container">
                                                        <!-- // organiser -->
                                                        <div class="show-fund-info-user">
                                                                <div class="postMeta--author-avatar large">
                                                                        <img src="' . $fund['user_data']['avatar'] . '" alt="Organiser Image">
                                                                </div>
                                                                <div class="postMeta--author-text">
                                                                        <span class="user-popover" data-id="' . $fund['user_data']['id'] . '" data-type="user">
                                                                            <a class="bold" href="javascript:void(0);">' . $fund['user_data']['name'] . '</a>
                                                                        </span>
                                                                        <br>
                                                                        <span class="time">'.$wo['lang']['organiser'].'</span>
                                                                </div>

                                                        </div>
                                                        <!-- // beneficiary -->
                                                        <div class="show-fund-info-user">
                                                                <div class="postMeta--author-avatar large">
                                                                        <img src="' . $fund['beneficiary_data']['avatar'] . '" alt="Beneficiary Image">
                                                                </div>
                                                                <div class="postMeta--author-text">
                                                                        <span class="user-popover" data-id="' . $fund['beneficiary_data']['id'] . '" data-type="user">
                                                                                <a class="bold" href="javascript:void(0);">' . $fund['beneficiary_data']['name'] . '</a>
                                                                        </span>
                                                                        <br>
                                                                        <span class="time">'.$wo['lang']['beneficiary'].'</span>
                                                                </div>
                                                        </div>
                                                    </div>
                                                    <div class="campaign_progress">
                                                        <div>
                                                                <span class="pull-right">' . (empty($fund['due_date']) ? '' : ('<time>' . Wo_Time_Elapsed_String(strtotime($fund['due_date']),"due_date") . '</time>')) . '</span>
                                                                <p><span class="big-text bold">' . $wo['config']['currency_symbol_array'][$wo['config']['currency']]. number_format($fund['raised']) . '</span> of ' . $wo['config']['currency_symbol_array'][$wo['config']['currency']] .number_format($fund['amount']) . ' ' . $wo['lang']['raised_of'] . '</p>
                                                        </div>
                                                        <div class="fund_raise_bar">
                                                            <div class="progress mb-2">
                                                                <div class="progress-bar" role="progressbar" style="width: ' . $fund['bar'] . '%" aria-valuenow="' . $fund['bar'] . '" aria-valuemin="0" aria-valuemax="' . $fund['amount'] . '"></div>
                                                            </div>
                                                        </div>     
                                                    </div>
                                                </div>                                                
                                            </div>
                                            </div>
                                            <div class="text-center"><a id="restart_button" href="'.$wo['site_url'].'/create_challenge" class="btn btn-mat btn-main">'.$wo['lang']['choose_another_campaign'].'</a></div>
                                        ';
					
					$data['message'] = "Data Found";
					$data['status'] = 200;
					$data['html'] = $html;
			}else{
				$data['message'] = "No Data Found";
				$data['status'] = 400;
			}

		}
	}
	// challlenges
	if ($s == 'get_all_challenges') {
		$challenges = getAllChallenges();
		$titles = array();	
		foreach($challenges as $challenge){
			$titles[] = array(
				'value'=>$challenge->title,
				'id'=>$challenge->id
			);
		}
					
		$data['message'] = "Data Found";
		$data['status'] = 200;
		$data['challenges'] = Array($challenges);
		$data['title'] = $titles;
		
	}
	//search funding
	if ($s == 'search_funding'){
		$data['search'] = $_GET['search']; 
		$data['message'] = "Data Found";
		$data['status'] = 200;
		$data['html'] = Wo_LoadPage("create_challenge/show-search-campaigns");
	}

	if ($s == 'get_payment_donate_method') {
		if (!empty($_GET['amount']) && is_numeric($_GET['amount']) && $_GET['amount'] > 0 && !empty($_GET['fund_id']) && is_numeric($_GET['fund_id']) && $_GET['fund_id'] > 0) {
			$amount = Wo_Secure($_GET['amount']);
			$id = Wo_Secure($_GET['fund_id']);
			$challenge_id = Wo_Secure($_GET['challenge_id']);
			$show = Wo_Secure($_GET['show']);
			$receipt = Wo_Secure($_GET['receipt']);
			$fund = GetFundingById($id);
			$accepted_amount=$fund['amount'] + ($fund['amount']*20/100);
			$left = $accepted_amount - $fund['raised'];
			//echo $left;die;
			if (!empty($fund)) {
				//if ($amount <= $left) {
					$html = '';
					$wo['donate_fund_id'] = $id;
					$wo['donate_fund_amount'] = $amount;
					$wo['donate_challenge_id'] = $challenge_id;
					$wo['display_name'] = $show;
					$wo['receipt'] = $receipt;
					$html = Wo_LoadPage('modals/donate_payment_methods');
					$data['status'] = 200;
					$data['html'] = $html;
				/*} else {
					$data['message'] = $error_icon . str_replace('{{money}}', $wo['config']['currency_symbol_array'][$wo['config']['currency']] . $left, $wo['lang']['you_cant_pay']);
				}*/
			} else {
				$data['message'] = $error_icon . $wo['lang']['please_check_details'];
			}
		} else {
			$data['message'] = $error_icon . $wo['lang']['please_check_details'];
		}
	}

	if ($s == 'get_paypal_url' && $wo['config']['paypal'] != 'no') {
		if (!empty($_POST['amount']) && is_numeric($_POST['amount']) && $_POST['amount'] > 0 && !empty($_POST['fund_id']) && is_numeric($_POST['fund_id']) && $_POST['fund_id'] > 0) {
			$price = Wo_Secure($_POST['amount']);
			$id = Wo_Secure($_POST['fund_id']);
			$challenge_id = Wo_Secure($_POST['challenge_id']);
			$display_name = Wo_Secure($_POST['display_name']);
			$receipt = Wo_Secure($_POST['receipt_required']);
			$fund = $db->where('id', $id)->getOne(T_FUNDING);
			if (!empty($fund)) {

				include_once('assets/includes/paypal_config.php');
				$product = "Doante to " . mb_substr($fund->title, 0, 100, "UTF-8");

				$payer = new Payer();
				$payer->setPaymentMethod('paypal');
				$item = new Item();
				$item->setName($product)->setQuantity(1)->setPrice($price)->setCurrency($wo['config']['paypal_currency']);
				$itemList = new ItemList();
				$itemList->setItems(array(
					$item
				));
				$details = new Details();
				$details->setSubtotal($price);
				$amount = new Amount();
				$amount->setCurrency($wo['config']['paypal_currency'])->setTotal($price)->setDetails($details);
				$transaction = new Transaction();
				$transaction->setAmount($amount)->setItemList($itemList)->setDescription($product)->setInvoiceNumber(uniqid());
				$redirectUrls = new RedirectUrls();


				$redirectUrls->setReturnUrl($wo['config']['site_url'] . "/requests.php?f=funding&s=paypal_paid&challenge_id=".$challenge_id."&fund_id=" . $id . "&amount=" . $price ."&show=".$display_name."&receipt=".$receipt)->setCancelUrl($wo['config']['site_url']);

				$payment = new Payment();
				$payment->setIntent('sale')->setPayer($payer)->setRedirectUrls($redirectUrls)->setTransactions(array(
					$transaction
				));
				try {
					$payment->create($paypal);
				} catch (Exception $e) {
					$data = array(
						'type' => 'ERROR',
						'details' => json_decode($e->getData())
					);
					if (empty($data['details'])) {
						$data['details'] = json_decode($e->getCode());
					}
					return $data;
				}
				$data = array(
					'status' => 200,
					'url' => $payment->getApprovalLink()
				);
			} else {
				$data['message'] = $error_icon . $wo['lang']['please_check_details'];
			}
		} else {
			$data['message'] = $error_icon . $wo['lang']['please_check_details'];
		}
	}

	if ($s == 'paypal_paid' && $wo['config']['paypal'] != 'no' && !empty($_GET['fund_id']) && !empty($_GET['amount'])) {
		if (!isset($_GET['paymentId'], $_GET['PayerID'])) {
			header("Location: " . Wo_SeoLink('index.php?link1=oops'));
			exit();
		}

		$fund_id = Wo_Secure($_GET['fund_id']);
		$amount = Wo_Secure($_GET['amount']);
		$challenge_id = Wo_Secure($_GET['challenge_id']);
		$show = Wo_Secure($_GET['show']);
		$receipt = Wo_Secure($_GET['receipt']);
		$fund = $db->where('id', $fund_id)->getOne(T_FUNDING);

		if (!empty($fund) && !empty($fund_id) && !empty($amount)) 
		{
			$paymentId = $_GET['paymentId'];
			$PayerID = $_GET['PayerID'];

			include_once  'assets/includes/paypal_config.php';
			$payment = new Payment();
			$payment = Payment::get($paymentId, $paypal);
			$execute = new PaymentExecution();
			$execute->setPayerId($PayerID);
			try {
				$notes = "Donated to " . mb_substr($fund->title, 0, 100, "UTF-8");
				$result = $payment->execute($execute, $paypal);

				//$create_payment_log = mysqli_query($sqlConnect, "INSERT INTO " . T_PAYMENT_TRANSACTIONS . " (`userid`, `kind`, `amount`, `notes`) VALUES ({$wo['user']['user_id']}, 'DONATE', {$amount}, '{$notes}')");
				$payment_log_id=$db->insert(T_PAYMENT_TRANSACTIONS, array(
                    'userid' => $wo['user']['user_id'],
                    'payment_id' => $paymentId,
                    'payment_gateway'=>'paypal',
                    'kind' => 'DONATE',
                    'amount' => $amount,
                    'notes' => $notes,
                    
                ));

				$admin_com = 0;
				if (!empty($wo['config']['donate_percentage']) && is_numeric($wo['config']['donate_percentage']) && $wo['config']['donate_percentage'] > 0) 
				{
					$admin_com = ($wo['config']['donate_percentage'] * $amount) / 100;
					$amount = $amount - $admin_com;
				}
				$user_data = Wo_UserData($fund->user_id);
				$db->where('user_id', $fund->user_id)
				->update(T_USERS, array('balance' => $user_data['balance'] + $amount));
				$fund_raise_id = $db->insert(T_FUNDING_RAISE, array(
					'user_id' => $wo['user']['user_id'],
					'payment_log_id' => $payment_log_id,
					'funding_id' => $fund_id,
					'challenge_id' => $challenge_id,
					'amount' => $amount,
					'time' => time(),
					'show' => $show,
					'receipt' => $receipt
				));
				Wo_checkRaisedStatus($fund_id);
				wo_assignFlagsToUser($fund_id,$fund_raise_id);
				$post_data = array();
				$post_data['user_id']=Wo_Secure($wo['user']['user_id']);
				$post_data['fund_raise_id']=$fund_raise_id;
				$post_data['time']=time();
				$post_data['multi_image_post']=0;
				if($show=='0')
				{
					$post_data['postPrivacy']='4';
				}
				
				$id = Wo_RegisterPost($post_data);

				$notification_data_array = array(
					'recipient_id' => $fund->user_id,
					'type' => 'fund_donate',
					'url' => 'index.php?link1=show_fund&id=' . $fund->hashed_id
				);
				Wo_RegisterNotification($notification_data_array);

				//header("Location: " . $config['site_url'] . "/show_fund/" . $fund->hashed_id);
				header("Location: " . $config['site_url'] . "/payment_thankyou/" . $fund_raise_id);
				exit();
			} catch (Exception $e) {
				header("Location: " . Wo_SeoLink('index.php?link1=oops'));
				exit();
			}
		} else {
			header("Location: " . Wo_SeoLink('index.php?link1=oops'));
			exit();
		}
	}
	if ($s == 'stripe_new')
	{
		include_once('assets/includes/stripe_config.php');
		$fund_id = Wo_Secure($_POST['fund_id']);
		$amount = Wo_Secure($_POST['amount']);
		$challenge_id = Wo_Secure($_POST['challenge_id']);
		$show = Wo_Secure($_POST['display_name']);
		$receipt = Wo_Secure($_POST['receipt_required']);
                $payment_method = Wo_Secure($_POST['payment_method']);
		$fund = $db->where('id', $fund_id)->getOne(T_FUNDING);                

		$success_url=$wo['config']['site_url'] . "/requests.php?f=funding&s=stripe_paid_success&session_id={CHECKOUT_SESSION_ID}";
		//$success_url=$wo['config']['site_url'] . "/requests.php?f=funding&s=stripe_paid_success&session_id={CHECKOUT_SESSION_ID}";
		$metadata=array('challenge_id'=>$challenge_id,
					'campaign_id'=>$fund_id,
					'amount'=>$amount,
					'show'=>$show,
					'receipt'=>$receipt);
		$cancel_url=$wo['config']['site_url'] . "/requests.php?f=funding&s=stripe_paid_cancel&session_id={CHECKOUT_SESSION_ID}";
		//echo "<pre>";print_r($_POST);die;
		$product = \Stripe\Product::create([
			  'name' => 'Donation on Little Flags to campaign "' . $fund->title . '"',
			  'images' => [Wo_GetMedia($fund->image)],
			]);
		// $customer = \Stripe\Customer::create([
		// 		'email' => $wo['user']['email'],
		// 		'id'=>$wo['user']['id']]);
                $session_data = [
			  'line_items' => [[
			    'price_data' => [
			      'product' => $product->id,
			      'unit_amount' => $amount*100,
			      'currency' => $wo['config']['stripe_currency'],
			    ],
			    'quantity' => 1,
			  ]],
			  'client_reference_id'=>$wo['user']['id'],
			  'customer_email'=>$wo['user']['email'],
			  'metadata'=>$metadata,
			  'mode' => 'payment',
			  'success_url' => $success_url,
			  'cancel_url' => $cancel_url,
			];
                if($payment_method == 'stripe'){
                    $session_data['payment_method_types'] = ['card'];
                }else{
                    $session_data['payment_method_types'] = ['alipay'];
                }
		$session = \Stripe\Checkout\Session::create($session_data);
		$data = array(
					'status' => 200,
					'session' => $session
				);
	}
	if ($s == 'stripe_paid_success')
	{
		include_once('assets/includes/stripe_config.php');
		$session_id =$_GET['session_id'];
		if(empty($_GET['session_id']))
		{
			header("Location: " . Wo_SeoLink('index.php?link1=oops'));
			exit();
		}
		//$stripepk=$wo['config']['stripe_secret'];
		//$stripe = new \Stripe\StripeClient($stripepk);
		//$result=$stripe->checkout->sessions->retrieve($session_id);
		$result = \Stripe\Checkout\Session::retrieve($session_id);
		//echo "<pre>";print_r($result);die;
		$redirect=1;
		Wo_completeYourOrder($result,$redirect);
		
	}
	if ($s == 'stripe_paid_cancel')
	{
		header("Location: " . Wo_SeoLink('index.php?link1=oops'));
		exit();
	}
	
	if ($s == 'stripe' && $wo['config']['credit_card'] != 'no' && !empty($_POST['fund_id']) && is_numeric($_POST['fund_id']) && $_POST['fund_id'] > 0 && !empty($_POST['amount']) && is_numeric($_POST['amount']) && $_POST['amount'] > 0) {

		include_once('assets/includes/stripe_config.php');

		$fund_id = Wo_Secure($_POST['fund_id']);
		$amount = Wo_Secure($_POST['amount']);
		$challenge_id = Wo_Secure($_POST['challenge_id']);
		$show = Wo_Secure($_POST['display_name']);
		$receipt = Wo_Secure($_POST['receipt_required']);
		$fund = $db->where('id', $fund_id)->getOne(T_FUNDING);

		if (empty($_POST['stripeToken']) || empty($fund)) {
			header("Location: " . Wo_SeoLink('index.php?link1=oops'));
			exit();
		}
		$token = $_POST['stripeToken'];
		try {
			$customer = \Stripe\Customer::create(array(
				'source' => $token
			));

			$final_amount = $amount * 100;
			$charge   = \Stripe\Charge::create(array(
				'customer' => $customer->id,
				'amount' => $final_amount,
				'currency' => $wo['config']['stripe_currency']
			));
			if ($charge) {

				$notes = "Donated to " . mb_substr($fund->title, 0, 100, "UTF-8");

				$create_payment_log = mysqli_query($sqlConnect, "INSERT INTO " . T_PAYMENT_TRANSACTIONS . " (`userid`, `kind`, `amount`, `notes`) VALUES ({$wo['user']['user_id']}, 'DONATE', {$amount}, '{$notes}')");

				$admin_com = 0;
				if (!empty($wo['config']['donate_percentage']) && is_numeric($wo['config']['donate_percentage']) && $wo['config']['donate_percentage'] > 0) {
					$admin_com = ($wo['config']['donate_percentage'] * $amount) / 100;
					$amount = $amount - $admin_com;
				}
				$user_data = Wo_UserData($fund->user_id);
				$db->where('user_id', $fund->user_id)->update(T_USERS, array('balance' => $user_data['balance'] + $amount));
				$fund_raise_id = $db->insert(T_FUNDING_RAISE, array(
					'user_id' => $wo['user']['user_id'],
					'funding_id' => $fund_id,
					'amount' => $amount,
					'time' => time(),
					'challenge_id' => $challenge_id,
					'show' => $show,
					'receipt' => $receipt
				));

				$post_data = array(
					'user_id' => Wo_Secure($wo['user']['user_id']),
					'fund_raise_id' => $fund_raise_id,
                                        'time' => time(),
					'multi_image_post' => 0
				);
				Wo_checkRaisedStatus($fund_id);
				wo_assignFlagsToUser($fund_id,$fund_raise_id);
				$id = Wo_RegisterPost($post_data);

				$notification_data_array = array(
					'recipient_id' => $fund->user_id,
					'type' => 'fund_donate',
					'url' => 'index.php?link1=show_fund&id=' . $fund->hashed_id
				);
				Wo_RegisterNotification($notification_data_array);

				$data = array(
					'status' => 200,
					'location' => $config['site_url'] . "/payment_thankyou/" . $fund_raise_id
				);
			}
		} catch (Exception $e) {
			$data = array(
				'status' => 400,
				'error' => $e->getMessage()
			);
			header("Content-type: application/json");
			echo json_encode($data);
			exit();
		}
	}

	if ($s == 'bank') {
		//echo "<pre>";print_r($_POST);die;
		$request   = array();
		$request[] = (empty($_FILES["thumbnail"]));
		if (in_array(true, $request) || empty($_POST['price']) || !is_numeric($_POST['price']) || $_POST['price'] < 1 || empty($_POST['fund_id']) || !is_numeric($_POST['fund_id']) || $_POST['fund_id'] < 1) {
			$error = $error_icon . $wo['lang']['please_check_details'];
		}
		$challenge_id = 0;
		if(isset($_POST['challenge_id']) && !empty($_POST['challenge_id'])){
			$challenge_id = $_POST['challenge_id'];
		}
		$show = 1;
		if(isset($_POST['show']) && !empty($_POST['show'])){
			$show = $_POST['show'];
		}
		$receipt = 0;
		if(isset($_POST['receipt']) && !empty($_POST['receipt'])){
			$receipt = $_POST['receipt'];
		}

		$fund_id = Wo_Secure($_POST['fund_id']);
		$amount = Wo_Secure($_POST['price']);
		$fund = $db->where('id', $fund_id)->getOne(T_FUNDING);

		if (empty($error) && !empty($fund)) {
			$description = "Doante to " . mb_substr($fund->title, 0, 100, "UTF-8");;
			$fileInfo      = array(
				'file' => $_FILES["thumbnail"]["tmp_name"],
				'name' => $_FILES['thumbnail']['name'],
				'size' => $_FILES["thumbnail"]["size"],
				'type' => $_FILES["thumbnail"]["type"],
				'types' => 'jpeg,jpg,png,bmp,gif'
			);
			$media         = Wo_ShareFile($fileInfo);
			$mediaFilename = $media['filename'];
			if (!empty($mediaFilename)) {
				$insert_id = Wo_InsertBankTrnsfer(array(
					'user_id' => $wo['user']['id'],
					'description' => $description,
					'price'       => $amount,
					'receipt_file' => $mediaFilename,
					'mode'         => 'donate',
					'fund_id' => $fund_id
				));
				if (!empty($insert_id)) {
					$fundraiseData = array(
						'funding_id'=>$fund_id,
						'user_id'=> $wo['user']['id'],
						'amount'=>$amount,
						'time' => time(),
						'challenge_id' => $challenge_id,
						'show' => $show,
						'status' => 0,
						'receipt' => $receipt
					);
					$fundraised_id = Wo_InsertFundRaiser($fundraiseData);
					
					if(!empty($fundraised_id)){
						$data = array(
							'message' => $success_icon . $wo['lang']['bank_transfer_request'],
							'status' => 200,
							'location' => $config['site_url'] . "/payment_thankyou/" . $fundraised_id
						);
					}
					
				}
			} else {
				$error = $error_icon . $wo['lang']['file_not_supported'];
				$data = array(
					'status' => 500,
					'message' => $error
				);
			}
		} else {
			$data = array(
				'status' => 500,
				'message' => $error
			);
		}
	}

	if ($s == '2checkout') {



		if (empty($_POST['card_number']) || empty($_POST['card_cvc']) || empty($_POST['card_month']) || empty($_POST['card_year']) || empty($_POST['token']) || empty($_POST['card_name']) || empty($_POST['card_address']) || empty($_POST['card_city']) || empty($_POST['card_state']) || empty($_POST['card_zip']) || empty($_POST['card_country']) || empty($_POST['card_email']) || empty($_POST['card_phone']) || empty($_POST['amount']) || empty($_POST['fund_id'])) {
			$data = array(
				'status' => 400,
				'error' => $wo['lang']['please_check_details']
			);
		} else {

			$fund_id = Wo_Secure($_POST['fund_id']);
			$amount = Wo_Secure($_POST['amount']);
			$fund = $db->where('id', $fund_id)->getOne(T_FUNDING);

			if (!empty($fund)) {
				require_once 'assets/libraries/2checkout/Twocheckout.php';
				Twocheckout::privateKey($wo['config']['checkout_private_key']);
				Twocheckout::sellerId($wo['config']['checkout_seller_id']);
				if ($wo['config']['checkout_mode'] == 'sandbox') {
					Twocheckout::sandbox(true);
				} else {
					Twocheckout::sandbox(false);
				}
				try {
					$amount1 = $_POST['amount'];
					$charge  = Twocheckout_Charge::auth(array(
						"merchantOrderId" => "123",
						"token" => $_POST['token'],
						"currency" => $wo['config']['2checkout_currency'],
						"total" => $amount1,
						"billingAddr" => array(
							"name" => $_POST['card_name'],
							"addrLine1" => $_POST['card_address'],
							"city" => $_POST['card_city'],
							"state" => $_POST['card_state'],
							"zipCode" => $_POST['card_zip'],
							"country" => $wo['countries_name'][$_POST['card_country']],
							"email" => $_POST['card_email'],
							"phoneNumber" => $_POST['card_phone']
						)
					));
					if ($charge['response']['responseCode'] == 'APPROVED') {
						Wo_UpdateUserData($wo['user']['id'], array(
							'address' => Wo_Secure($_POST['card_address']),
							'city' => Wo_Secure($_POST['card_city']),
							'state' => Wo_Secure($_POST['card_state']),
							'zip' => Wo_Secure($_POST['card_zip'])
						));



						$notes = "Donated to " . mb_substr($fund->title, 0, 100, "UTF-8");

						$create_payment_log = mysqli_query($sqlConnect, "INSERT INTO " . T_PAYMENT_TRANSACTIONS . " (`userid`, `kind`, `amount`, `notes`) VALUES ({$wo['user']['user_id']}, 'DONATE', {$amount}, '{$notes}')");

						$admin_com = 0;
						if (!empty($wo['config']['donate_percentage']) && is_numeric($wo['config']['donate_percentage']) && $wo['config']['donate_percentage'] > 0) {
							$admin_com = ($wo['config']['donate_percentage'] * $amount) / 100;
							$amount = $amount - $admin_com;
						}
						$user_data = Wo_UserData($fund->user_id);
						$db->where('user_id', $fund->user_id)->update(T_USERS, array('balance' => $user_data['balance'] + $amount));
						$fund_raise_id = $db->insert(T_FUNDING_RAISE, array(
							'user_id' => $wo['user']['user_id'],
							'funding_id' => $fund_id,
							'amount' => $amount,
							'time' => time()
						));
						$post_data = array(
							'user_id' => Wo_Secure($wo['user']['user_id']),
							'fund_raise_id' => $fund_raise_id,
                                                        'time' => time(),
							'multi_image_post' => 0
						);
						Wo_checkRaisedStatus($fund_id);
						wo_assignFlagsToUser($fund_id,$fund_raise_id);
						$id = Wo_RegisterPost($post_data);

						$notification_data_array = array(
							'recipient_id' => $fund->user_id,
							'type' => 'fund_donate',
							'url' => 'index.php?link1=show_fund&id=' . $fund->hashed_id
						);
						Wo_RegisterNotification($notification_data_array);

						$data = array(
							'status' => 200,
							'location' => $config['site_url'] . "/payment_thankyou/" . $fund_raise_id
						);
					} else {
						$data = array(
							'status' => 400,
							'error' => $wo['lang']['2checkout_declined']
						);
						header("Content-type: application/json");
						echo json_encode($data);
						exit();
					}
				} catch (Twocheckout_Error $e) {
					$data = array(
						'status' => 400,
						'error' => $e->getMessage()
					);
				}
			}
		}
	}
	// endorse funding
	if ($s == 'funding_endorse') {
		$data['status'] = 400;
		if (isset($_POST['fund_id']) && !empty($_POST['fund_id'])) {

			$isEndorseExits = $db->where('funding_id',$_POST['fund_id'])->where('user_id',$wo['user']['user_id'])->get(T_FUNDING_ENDORSE);	

			if(empty($isEndorseExits)){
				
				$isEndorseSaved = $db->insert(T_FUNDING_ENDORSE,array(
					'funding_id' => $_POST['fund_id'],
					'text' => $_POST['text'],
					'user_id' => $wo['user']['user_id'],
					'time' => time(),
				));
				//echo $isEndorseSaved ;die;
				if ($isEndorseSaved) 
				{
					$html='';
					$wo['endorse_list']='';
					$funding = $db->where('id',$isEndorseSaved)->getOne(T_FUNDING_ENDORSE);
				    if (!empty($funding)) 
				    {
				       	$new_data = $funding;
				        $new_data->user_data = Wo_UserData($funding->user_id);
				        $new_data = (Array) $new_data;
				        $wo['endorse_list'] = $new_data;
				        $html = Wo_LoadPage('show_fund/endorse-list');

				        $post_data = array(
		                    'user_id' => Wo_Secure($wo['user']['user_id']),
		                    'endorsed_id' => $isEndorseSaved,
		                    'time' => time(),
		                    'multi_image_post' => 0
		                );

		        		$id = Wo_RegisterPost($post_data);

				    }

					
					$data['html']=$html;
					$data['status'] = 200;
					$data['message'] = $success_icon . $wo['lang']['endorse_saved'];
				} else {
					$data['message'] = $error_icon . $wo['lang']['please_check_details'];
				}
			}else{
				$data['status'] = 200;
				$data['message'] = $success_icon . $wo['lang']['endorse_saved'];
			}
		} else {
			$data['message'] = $error_icon . $wo['lang']['please_check_details'];
		}
	}

	// withdraw funding request
	if ($s == 'funding_withdraw_request') {
		$data['status'] = 400;
		if (isset($_POST['fund_id']) && !empty($_POST['fund_id'])) 
		{

			$id = Wo_Secure($_POST['fund_id']);

			$fund = $db->where('id', $id)->getOne(T_FUNDING);	

			if(!empty($fund))
			{

				/*********** Zapier hook request ********************/
				$fund_raised=$db->where('funding_id',$fund->id)->where('status','1')->getValue(T_FUNDING_RAISE,"SUM(amount)");
				$organiser=Wo_UserData($fund->user_id);
				//echo "<pre>";print_r($fund_raised);die;
				$organizer_data=array();
				$beneficiary_data=array();
				$organizer_data['user_id']=$organiser['id'];
				$organizer_data['username']=$organiser['username'];
				$organizer_data['first_name']=$organiser['first_name'];
				$organizer_data['last_name']=$organiser['last_name'];
				$organizer_data['profile_url']=$wo['config']['site_url'].'/'.$organiser['username'];

				$beneficiary_users = Wo_GetTaggedUsers($fund->beneficiary);
                                $beneficiary = Wo_UserData($beneficiary_users[0]['id']);
                                //echo "<pre>";print_r($beneficiary);die;
                                $beneficiary_data['user_id']=$beneficiary['user_id'];
				$beneficiary_data['username']=$beneficiary['username'];
				$beneficiary_data['first_name']=$beneficiary['first_name'];
				$beneficiary_data['last_name']=$beneficiary['last_name'];
				$beneficiary_data['profile_url']=$wo['config']['site_url'].'/'.$beneficiary['username'];

				$request_data=array();
				$url='https://hooks.zapier.com/hooks/catch/6466381/ovuoacp/';
				$request_data['campaign_id']=$fund->id;
				$request_data['campaign_title']=$fund->title;
				$d = DateTime::createFromFormat('Y-m-d', $fund->due_date);
				$request_data['due_date']=$d->getTimestamp();
				$request_data['date']=time();
				$request_data['campaign_url']=$wo['config']['site_url'].'/show_fund/'.$fund->hashed_id;
				$request_data['withdraw_fund']=$fund_raised;
				$request_data['raised']=$fund_raised;
				$request_data['target']=$fund->amount;
				$request_data['organiser']=$organizer_data;
				$request_data['beneficiary']=$beneficiary_data;
				$postfield=json_encode($request_data);
				$ch = curl_init();

				//set the url, number of POST vars, POST data
				curl_setopt($ch,CURLOPT_URL, $url);
				curl_setopt($ch,CURLOPT_POST, true);
				curl_setopt($ch,CURLOPT_POSTFIELDS, $postfield);

				//So that curl_exec returns the contents of the cURL; rather than echoing it
				curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

				//execute post
				$result = curl_exec($ch);
				//echo $result;

				//echo "<pre>";print_r($result);
				$output=json_decode($result);
				//echo "<pre>";print_r($output);
				//echo $output['status'] .'---'.$output->status.'----'.$result['status'].'--'.$result->status;
				//die;
				if($output->status=='success'){
                                    $body= Wo_LoadPage('emails/withdraw-request');
				}else{
                                    $body= Wo_LoadPage('emails/withdraw-request-to-trello-failed');
                                }
                                
                                // update email content
                                $body= str_replace('{campaign_name}', $request_data['campaign_title'], $body);
                                $body= str_replace('{campaign_url}', $request_data['campaign_url'], $body);
                                $body= str_replace('{site_name}', $config['siteName'], $body);
                                $send_message_data = array(
                                    'from_email' => $wo['config']['siteEmail'],
                                    'from_name' => $wo['config']['siteName'],
                                    'to_email' => 'account@littleflags.org',
                                    'to_name' => 'Account Department',
                                    'subject' => $wo['lang']['campaign_fund_withdrawl_request'],
                                    'charSet' => 'utf-8',
                                    'message_body' => $body,
                                    'is_html' => true
                                );
                                $send              = Wo_SendMessage($send_message_data);
				
				$fund->status ='2';	
                                $fund->widthdraw_request_id =$output->request_id;
                                $fund_data = (Array)$fund;
                                $data['fund'] = $fund_data;

                                $db->where('id', $id)->update(T_FUNDING,$fund_data);
                                $data['status'] = 200;
                                $data['message'] = $wo['lang']['withdraw_funds_success'];
			}
			else
			{
				$data['status'] = 200;
				$data['message'] = $success_icon . $wo['lang']['no_funding_found'];
			}
		} 
		else 
		{
			$data['message'] = $error_icon . $wo['lang']['please_check_details'];
		}
	}
	/******** Create Beneficiary ***************/
	if ($s == 'create_beneficiary') {
		//echo "<pre>";print_r($_POST);die;
		if(empty($_POST['first_name']))
		{
			$data['message'] = $error_icon . $wo['lang']['first_name'].' is required';
		}
		else if(empty($_POST['last_name']))
		{
			$data['message'] = $error_icon . $wo['lang']['last_name'].' is required';
		}
		else if(empty($_POST['email']))
		{
			$data['message'] = $error_icon . $wo['lang']['email'].' is required';
		}
		else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) 
                {
                        $data['message'] = $error_icon . $wo['lang']['email_invalid_characters'];
                }
		else if (Wo_EmailExists($_POST['email']) === true) 
                {
                        $data['message'] = $error_icon . $wo['lang']['email_exists'];
                }
                else  if (preg_match_all('~@(.*?)(.*)~', $_POST['email'], $matches) && !empty($matches[2]) && !empty($matches[2][0]) && Wo_IsBanned($matches[2][0])) {
                    $data['message'] = $error_icon . $wo['lang']['email_provider_banned'];
                }
        // else if (Wo_CheckIfUserCanRegister($wo['config']['user_limit']) === false) {
        //     $data['message'] = $error_icon . $wo['lang']['limit_exceeded'];
        // }
		else if (empty($_POST['gender'])) 
		{
                        $data['message'] = $error_icon . $wo['lang']['gender'].' is required';
                }
                else
                {
                    $gender = 'male';
                    if (in_array($_POST['gender'], array_keys($wo['genders']))) {
                        $gender = $_POST['gender'];
	        }
	        /** first name,last name and username **/
	        $username='';
	        if (isset($_POST['first_name']) && !empty($_POST['first_name'])) {
	            $first_name = $_POST['first_name'];
	            $username = str_replace(' ', '_', $first_name);
	        }
	        if (isset($_POST['last_name']) && !empty($_POST['last_name'])) {
	            $last_name = $_POST['last_name'];
	            $last_name = str_replace(' ', '_', $last_name);
	            $username =$username.'_'.$last_name;
	        }
                $username = strtolower($username);
	        $is_exist = Wo_IsNameExist($username, 0);
	        if (in_array(true, $is_exist)) 
	        {
	            $six_digit_random_number = mt_rand(100000, 999999);
	            $username = $username . '_'.$six_digit_random_number;
	        }

	        $username=str_replace(".", "", $username);
	        $activate = ($wo['config']['emailValidation'] == '1') ? '0' : '1';

	        $my_passwords = randomPassword(8,1,"lower_case,upper_case,numbers");
	        //echo $my_passwords .'----'.$username;die;
	        $code = md5(rand(1111, 9999) . time());

	        $password=
	        $re_data  = array(
	            'email' => Wo_Secure($_POST['email'], 0),
	            'first_name' => Wo_Secure($_POST['first_name'], 0),
	            'last_name' => Wo_Secure($_POST['last_name'], 0),
	            'username' => Wo_Secure($username),
	            'password' => $my_passwords,
	            'email_code' => Wo_Secure($code, 0),
	            'src' => 'site',
	            'gender' => Wo_Secure($gender),
	            'lastseen' => time(),
	            'active' => Wo_Secure($activate),
	            'birthday' => '0000-00-00'
	        );
	        //echo "<pre>";print_r($re_data);die;
	        if ($gender == 'female') {
	            $re_data['avatar'] = "upload/photos/f-avatar.jpg";
	        }

        	$register = Wo_RegisterUser($re_data);
        	if ($register === true) 
        	{

        		$body              = Wo_LoadPage('emails/beneficiary-email');
        		$name=$_POST['first_name'] .' '.$_POST['last_name'];
        		
        		$tokens = array('{beneficiary_name}'=>$name,'{beneficiary_username}'=>$username,'{beneficiary_password}'=>$my_passwords);
    			$keys   = array_keys($tokens);
    			$values = array_values($tokens);
        		$body = str_replace($keys,$values,$body);
                $send_message_data = array(
                    'from_email' => $wo['config']['siteEmail'],
                    'from_name' => $wo['config']['siteName'],
                    'to_email' => $_POST['email'],
                    'to_name' => $username,
                    'subject' => $wo['lang']['account_activation'],
                    'charSet' => 'utf-8',
                    'message_body' => $body,
                    'is_html' => true
                );
                $data['status'] = 200;
                $send              = Wo_SendMessage($send_message_data);
                $data['message'] = $success_icon . $wo['lang']['successfully_joined_label'];
                $data['user_detail']=$username;
        	}

    	}
    	
    	
	}

	if ($s == 'load_category_fund') {
		//echo "<pre>";print_r($_GET);die;
		if (!empty($_GET['offset']) && is_numeric($_GET['offset']) && $_GET['offset'] > 0) {
			$id = Wo_Secure($_GET['offset']);
			$category_id = Wo_Secure($_GET['cat']);
			//$funding = GetFunding(10, $id);
			$funding =getCampaignsBycategory($category_id,10,$id);
			$html = '';
			if (!empty($funding)) {
				foreach ($funding as $key => $wo['fund']) {
					$html .= Wo_LoadPage('funding/list');
				}
			}
			$data['status'] = 200;
			$data['html'] = $html;
		}
	}

	/************ Invite Fundraiser *******************/
	if($s == 'invite_funding')
	{
		//echo "<pre>";print_r($_POST);die;
		if(empty($_POST['message']))
		{
			$data['message'] = $error_icon . $wo['lang']['message'].' is required';
		}
		else if(empty($_POST['mention_friends']))
		{
			$data['message'] = $error_icon . $wo['lang']['tag_at_least_one'].' is required';
		}
		else if(empty($_POST['fund_id']))
		{
			$data['message'] = $error_icon . 'Unable to get the campaign detail. Please try after some time!!';
		}
		else
		{
			$fund_id=$_POST['fund_id'];
			$message=$_POST['message'];
			$funding = $db->where('id',$fund_id)->getOne(T_FUNDING);
			$post_text = "";
			$mention_regex = '/@([A-Za-z0-9_]+)/i';
			$post_text = Wo_Secure($_POST['mention_friends']);
			preg_match_all($mention_regex, $post_text, $matches);
			foreach ($matches[1] as $match) {
				$match         = Wo_Secure($match);
				// $match_user    = Wo_UserData(Wo_UserIdFromUsername($match));
				$match_user['user_id']    = Wo_UserIdFromUsername($match);
				$match_search  = '@' . $match;
				$match_replace = '@[' . $match_user['user_id'] . ']';
				// if (isset($match_user['user_id'])) {
					$post_text  = str_replace($match_search, $match_replace, $post_text);
					$mentions[] = $match_user['user_id'];
				// }
			}
		
			if(sizeof($mentions) < 1)
			{
				$data['status'] = 400;
				$message =$error_icon.$wo['lang']['tag_at_least_one']."<br>";						
				$data['message'] = $message;
				echo json_encode($data);	
				exit();
			}
			if (isset($mentions) && is_array($mentions)) 
			{
				foreach ($mentions as $mention) 
				{
					if (empty($wo['no_mention']) || (!empty($wo['no_mention']) && !in_array($mention, $wo['no_mention'])))
					{
						$notification_data_array = array(
								'recipient_id' => $mention,
								'type' => 'campaign_invitation',
								'page_id' => 0,
								'text' => $message,
								'campaign_id' => $fund_id,
								'url' => $config['site_url'] . "/show_fund/" . $funding->hashed_id
							);
							$id=Wo_RegisterNotification($notification_data_array);
							$db->where('id',$id)
							->update(T_NOTIFICATION,array('url' => $config['site_url'] . "/show_fund/" . $funding->hashed_id.'?notify='.$id));
					}
				}
				$data = array(
							'status' => 200,
							'message' => 'You have successfully sent invitation to selected users',
							'url' => $config['site_url'] . "/show_fund/" . $funding->hashed_id
						);
				
			}
		}
	}

	

}
echo json_encode($data);
exit();
