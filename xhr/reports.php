<?php 
if ($f == 'reports') {
    if ($s == 'report_user' && isset($_POST['user']) && is_numeric($_POST['user']) && isset($_POST['text'])) {
        $user = Wo_Secure($_POST['user']);
        $text = Wo_Secure($_POST['text']);
        $code = Wo_ReportUser($user, $text);
        $data = array(
            'status' => 304
        );
        if ($code == 0) {
            $data['status'] = 200;
            $data['code']   = 0;
        } else if ($code == 1) {
            $data['status'] = 200;
            $data['code']   = 1;
        }
        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }
    // report campaign sameer
    if ($s == 'report_campaign' && isset($_POST['fund_id']) && is_numeric($_POST['fund_id']) && isset($_POST['text'])) {
        $fund_id = Wo_Secure($_POST['fund_id']);
        $text = Wo_Secure($_POST['text']);
        $code = Wo_ReportCampaign($fund_id, $text);
        // $data['status'] = 400;
        // $data['code']   = $code;
        // $data['message'] = 'issue';
        // echo json_encode($data);
        // exit();
        $data = array(
            'status' => 304
        );
        if ($code == 0) {
            $data['status'] = 200;
            $data['code']   = 0;
        } else if ($code == 1) {
            $data['status'] = 200;
            $data['code']   = 1;
        }
        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }

    if ($s == 'report_challenge' && isset($_POST['challenge_id']) && is_numeric($_POST['challenge_id']) && isset($_POST['text'])) {
        $challenge_id = Wo_Secure($_POST['challenge_id']);
        $text = Wo_Secure($_POST['text']);
        $code = Wo_ReportChallenge($challenge_id, $text);
        //echo $code;die;
        // $data['status'] = 400;
        // $data['code']   = $code;
        // $data['message'] = 'issue';
        // echo json_encode($data);
        // exit();
        $data = array(
            'status' => 304
        );
        if ($code == 0) {
            $data['status'] = 200;
            $data['code']   = 0;
        } else if ($code == 1) {
            $data['status'] = 200;
            $data['code']   = 1;
        }
        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }
    if ($s == 'report_page' && isset($_POST['page']) && is_numeric($_POST['page']) && isset($_POST['text'])) {
        $page = Wo_Secure($_POST['page']);
        $text = Wo_Secure($_POST['text']);
        $code = Wo_ReportPage($page, $text);
        $data = array(
            'status' => 304
        );
        if ($code == 0) {
            $data['status'] = 200;
            $data['code']   = 0;
        } else if ($code == 1) {
            $data['status'] = 200;
            $data['code']   = 1;
        }
        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }
    if ($s == 'report_group' && isset($_POST['group']) && is_numeric($_POST['group']) && isset($_POST['text'])) {
        $group = Wo_Secure($_POST['group']);
        $text  = Wo_Secure($_POST['text']);
        $code  = Wo_ReportGroup($group, $text);
        $data  = array(
            'status' => 304
        );
        if ($code == 0) {
            $data['status'] = 200;
            $data['code']   = 0;
        } else if ($code == 1) {
            $data['status'] = 200;
            $data['code']   = 1;
        }
        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }
     if ($s == 'endorse_user' && isset($_POST['user']) && is_numeric($_POST['user']) && isset($_POST['text'])) {
        $user = Wo_Secure($_POST['user']);
        $text = Wo_Secure($_POST['text']);
        $code = Wo_EndorseUser($user, $text);
        $data = array(
            'status' => 304
        );
        if ($code == 0) {
            $data['status'] = 200;
            $data['code']   = 0;
        } else if ($code == 1) {
            $data['status'] = 200;
            $data['code']   = 1;
        }
        else
        {
            return $code;
        }
        header("Content-type: application/json");
        echo json_encode($data);
        exit();
    }
}
