<?php 
if ($f == 'cancel_user_verification') {
    if (!empty($_GET['id'])) {
        if (Wo_RemoveVerificationRequest($_GET['id'], 'User') === true) {
            $data = array(
                'status' => 200,
                'html' => '<div class="alert alert-success">' . $success_icon . 'Your verification request has been cancelled.</div>',
                'url' => $wo['config']['site_url'] . '/setting/verification'
            );
        }else{
            $data = array(
                'status' => 500,
                'html' => '<div class="alert alert-danger">' . $error_icon . 'There was an issue cancelling your verification request. Please try again later.</div>',
                'url' => ''
            );
        }
    }
    header("Content-type: application/json");
    echo json_encode($data);
    exit();
}
