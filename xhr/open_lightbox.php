<?php 
if ($f == 'open_lightbox') {

    header("Content-type: application/json");

    $html = '';
    if (!empty($_GET['post_id'])) {
        $wo['story'] = Wo_PostData($_GET['post_id']);
        if (!empty($wo['story']['challenge_fund_id'])) {
            $html = Wo_LoadPage('lightbox_challenge/content');
        }else{
            $html = Wo_LoadPage('lightbox/content');
        }
    }
    $data = array(
        'status' => 200,
        'html' => $html
    );

    echo json_encode($data);
    exit();
}
