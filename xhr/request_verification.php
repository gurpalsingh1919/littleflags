<?php 
if ($f == 'request_verification') {
    if (!empty($_GET['id']) && !empty($_GET['type'])) {
        //if (Wo_RequestVerification($_GET['id'], $_GET['type']) === true) {
        $request_id = Wo_RequestVerification($_GET['id'], $_GET['type']);
        if ($request_id !== false) {
            if($_GET['type'] == 'Page'){
                // get page info
                $page = Wo_PageData($_GET['id']);
                
                /***** Send mail to little flags **/
                $body   = Wo_LoadPage('emails/page-verification-request');
                $tokens = array(
                    '{request_id}'=>$request_id,
                    '{page_id}'=>$_GET['id'],
                    '{page_name}'=>$page['name'],
                    '{page_url}'=>$page['url'],
                    '{page_username}'=>$page['username']);
                $keys   = array_keys($tokens);
                $values = array_values($tokens);
                $body   = str_replace($keys,$values,$body);
                $send_message_data = array(
                    'from_email' => $wo['config']['noreplyEmail'],
                    'from_name' => $wo['config']['siteName'],
                    'to_email' => $wo['config']['siteEmail'],
                    'to_name' => 'Little Flags Account',
                    'subject' => $wo['lang']['new_page_verification_request'],
                    'charSet' => 'utf-8',
                    'message_body' => $body,
                    'is_html' => true
                );
                Wo_SendMessage($send_message_data);

                // set up card on Trello
                $request_data=array();
                $url='https://hooks.zapier.com/hooks/catch/6466381/bbaucmt/';
                $request_data['request_id']=$request_id;
                $request_data['page_id']=$_GET['id'];
                $request_data['page_name']=$page['name'];
                $request_data['page_url']=$page['url'];
                $request_data['page_username']=$page['username'];
                
                $postfield=json_encode($request_data);
                $ch = curl_init();

                //set the url, number of POST vars, POST data
                curl_setopt($ch,CURLOPT_URL, $url);
                curl_setopt($ch,CURLOPT_POST, true);
                curl_setopt($ch,CURLOPT_POSTFIELDS, $postfield);

                //So that curl_exec returns the contents of the cURL; rather than echoing it
                curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

                //execute post
                $result = curl_exec($ch);

                $output=json_decode($result);

                if($output->status!='success'){ 
                    // alert admin
                    $body2   = Wo_LoadPage('emails/page-to-trello-failed');
                    $tokens2 = array(
                        '{request_id}'=>$request_id,
                        '{page_id}'=>$_GET['id'],
                        '{page_name}'=>$page['name'],
                        '{page_url}'=>$page['url'],
                        '{page_username}'=>$page['username']);
                    $keys2   = array_keys($tokens2);
                    $values2 = array_values($tokens2);
                    $body2   = str_replace($keys2,$values2,$body2);
                    $send_message_data2 = array(
                        'from_email' => $wo['config']['noreplyEmail'],
                        'from_name' => $wo['config']['siteName'],
                        'to_email' => $wo['config']['siteEmail'],
                        'to_name' => 'Little Flags Account',
                        'subject' => 'Failed to create page on Trello board "Page Approval Pipeline"',
                        'charSet' => 'utf-8',
                        'message_body' => $body2,
                        'is_html' => true
                    );
                    Wo_SendMessage($send_message_data2);
                } 
            }
            $data = array(
                'status' => 200,
                'html' => Wo_GetVerificationButton($_GET['id'], $_GET['type'])
            );
        }
    }
    header("Content-type: application/json");
    echo json_encode($data);
    exit();
}
