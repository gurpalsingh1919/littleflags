<?php 
if ($f == 'verificate-user') {
    $data  = array(
        'status' => 304,
        'message' => ($error_icon . $wo['lang']['please_check_details']),
        'url' => ''
    );
    $error = false;
    
    if (!$error) {
        $response = Wo_InitIDVerification($wo['user']['id']);
        if($response != false){
            $data['status'] = $response['status'];
            $data['message'] = $response['message'];
            $data['result'] = $response['result'];
            
            if($response['status'] == 200){
                $data['url']     = $wo['config']['site_url'] . '/setting/verification';
            }
        }
    }
    
    header("Content-type: application/json");
    echo json_encode($data);
    exit();
}
