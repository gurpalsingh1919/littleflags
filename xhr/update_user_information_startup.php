<?php 
if ($f == 'update_user_information_startup' && Wo_CheckSession($hash_id) === true) {
    if (isset($_POST['user_id']) && is_numeric($_POST['user_id']) && $_POST['user_id'] > 0) {
        $Userdata = Wo_UserData($_POST['user_id']);
        if (!empty($Userdata['user_id'])) {
            $age_data = '00-00-0000';
            if (!empty($_POST['birthday']) && preg_match('@^\s*(3[01]|[12][0-9]|0?[1-9])\-(1[012]|0?[1-9])\-((?:19|20)\d{2})\s*$@', $_POST['birthday'])) {
               $newDate = date("Y-m-d", strtotime($_POST['birthday']));
               $age_data = $newDate;
            }
            else{
                if (!empty($_POST['age_year']) || !empty($_POST['age_day']) || !empty($_POST['age_month'])) {
                    if (empty($_POST['age_year']) || empty($_POST['age_day']) || empty($_POST['age_month'])) {
                        $errors[] = $error_icon . $wo['lang']['please_choose_correct_date'];
                    } else {
                        $age_data = $_POST['age_year'] . '-' . $_POST['age_month'] . '-' . $_POST['age_day'];
                    }
                }
            } 
           // echo "<pre>";print_r($_POST['interesting_cause']);
           // echo json_encode($_POST['interesting_cause']);
           // die;
            $Update_data = array(
                'first_name' => $_POST['first_name'],
                'last_name' => $_POST['last_name'],
                'country_id' => $_POST['country'],
                'interesting_cause' => json_encode($_POST['interesting_cause']),
                'birthday' => $age_data,
                'start_up_info' => '1'
            );
            $user_id = $db->where('user_id', $_POST['user_id'])->update(T_USERS, $Update_data);
            //if (Wo_UpdateUserData($_POST['user_id'], $Update_data)) {
                $data = array(
                    'status' => 200
                );
           // }
        }
    }
    header("Content-type: application/json");
    echo json_encode($data);
    exit();
}
