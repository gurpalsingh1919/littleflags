$(function() {

    // Basic editors
    // ------------------------------

    // Default initialization
    $('.summernote').summernote({
        height: 300
    });


    // Control editor height
    $('.summernote-height').summernote({
        height: 600
    });


    // Click to edit
    // ------------------------------

    // Edit
    $('#edit').on('click', function() {
        $('.click2edit').summernote({focus: true});
    })

    // Save
    $('.save').on('click', function() {
        var aHTML = $('.click2edit').summernote('code'); //save HTML If you need(aHTML: array).
        console.log(aHTML);
        $('#description').val(aHTML);
        $('.click2edit').summernote('destroy');
    })
    // var content=$('#description').val();
    // console.log(content);
   // var content=$(".summernote").summernote("code",content);

});
