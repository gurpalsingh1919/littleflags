// const staticCacheName = "site-static-v1",
//     assets = [
//         '/pwa.js'
       
//     ];
// self.addEventListener("install", e => {
//     e.waitUntil(caches.open(staticCacheName).then(e => {
//         console.log("caching shell assets"),
//         e.addAll(assets)
//     }))
// }),
// self.addEventListener("activate", e => {
//     e.waitUntil(
//         caches.keys().then(e => Promise.all(e.filter(e => e !== staticCacheName).map(e => caches.delete(e))))
//     )
// }),
// self.addEventListener("fetch", e => {
//     e.respondWith(caches.match(e.request).then(t => t || fetch(e.request)))
// });
const OFFLINE_VERSION = 0.50;
const CACHE_NAME = 'offline';
const OFFLINE_URL = 'offline.html';
self.addEventListener('install', (event) => {
    console.log('[Service Worker] running ...');
    event.waitUntil((async () => {
        const cache = await caches.open(CACHE_NAME);
        await cache.add(new Request(OFFLINE_URL, {cache: 'reload'}));
    })());
});
self.addEventListener('activate', (event) => {
    event.waitUntil((async () => {
        if ('navigationPreload' in self.registration) {
            await self
                .registration
                .navigationPreload
                .enable();
        }
    })());
    self
        .clients
        .claim();
});
self.addEventListener('fetch', (event) => {
    if (event.request.mode === 'navigate') {
        event.respondWith((async () => {
            try {
                const preloadResponse = await event.preloadResponse;
                if (preloadResponse) {
                    return preloadResponse;
                }
                const networkResponse = await fetch(event.request);
                return networkResponse;
            } catch (error) {
                console.log('Fetch failed; returning offline page instead.', error);
                const cache = await caches.open(CACHE_NAME);
                const cachedResponse = await cache.match(OFFLINE_URL);
                return cachedResponse;
            }
        })());
    }
});