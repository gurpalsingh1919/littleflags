"serviceWorker" in navigator && window.addEventListener("load", function () {
    navigator
        .serviceWorker
        .register("./serviceWorker.js")
        .then(e => console.log("service worker registered"))
        .catch(e => console.log("service worker not registered", e))
    });
    
    $(document).ready(function(){      
      'use strict'	
      
      var pwaCookie = true; // if set to false, the PWA prompt will appear even if the user selects "maybe later"
      var pwaNoCache = true; // always keep the cache clear to serve the freshest possible content
      
      
  
          
      //Creating Cookie System for PWA Hide
      function createCookie(e, t, n) {if (n) {var o = new Date;o.setTime(o.getTime() + 48 * n * 60 * 60 * 1e3);var r = "; expires=" + o.toGMTString()} else var r = "";document.cookie = e + "=" + t + r + "; path=/"}
      function readCookie(e) {for (var t = e + "=", n = document.cookie.split(";"), o = 0; o < n.length; o++) {for (var r = n[o];" " == r.charAt(0);) r = r.substring(1, r.length);if (0 == r.indexOf(t)) return r.substring(t.length, r.length)}return null}
      function eraseCookie(e) {createCookie(e, "", -1)}
      
     
      //Detecting Mobile Operating Systems
      var isMobile = {
          Android: function() {return navigator.userAgent.match(/Android/i);},
          iOS: function() {return navigator.userAgent.match(/iPhone|iPad|iPod/i);},
          any: function() {return (isMobile.Android() || isMobile.iOS() || isMobile.Windows());}
      };
      var isInWebAppiOS = (window.navigator.standalone == true);
      var isInWebAppChrome = (window.matchMedia('(display-mode: standalone)').matches);
      
      //Firing PWA prompts for specific versions and when not on home screen.    
      if (isMobile.Android()) {
          console.log('Android Detected');
          (function() {
            'use strict';
            var a = function(b) {
                var c;
                var d = function() {
                    c && (c.prompt(),
                    c.userChoice.then(function(i) {
                        c = null,
                        b.classList.remove('available')
                    }).catch(function() {
                        c = null,
                        b.classList.remove('available')
                    }))
                };
                window.addEventListener('beforeinstallprompt', function(h) {
                    return c = h,
                    b.classList.add('available'),
                    !1
                }),
                window.addEventListener('appinstalled', function() {
          
                    c = null,
                    b.classList.remove('available')
                }),
                b.addEventListener('click', d.bind(this)),
                b.addEventListener('touchend', d.bind(this))
            };
            window.addEventListener('load', function() {
                var b = document.body.querySelector('.usePWA')
                  , c = new a(b)
            })
          }
          )();
          

      }  

      if (isMobile.iOS()) {
        if(!isInWebAppiOS){
            //console.log('iOS Detected');
            if($('#menu-install-pwa-ios, .add-to-home').length){
                if (!readCookie('Azures_pwa_rejected_install')) {
                    //console.log('Triggering PWA / Add to Home Screen Menu for iOS');
                    setTimeout(function(){
                        $('.add-to-home').addClass('add-to-home-visible add-to-home-ios');
                        $('#menu-install-pwa-ios, .menu-hider').addClass('menu-active');
                    },4500);
                };
            } else {
                //console.log('The div #menu-install-pwa-ios was not found. Please add this div to show the install window');
            }
        }
    }     
      
      //Creating Update Modal
      function updateModal(){
          var body = $('body');
          var updateModal = $('#menu-update');
          var mt = new Date();
          var menuUpdate = mt.getHours() + ":" + mt.getMinutes() + ":" + mt.getSeconds();
          if(!updateModal.length){
             body.append('<div id="menu-update"></div>');
             setTimeout(function(){
                 body.find('#menu-update').load('menu-update.html?ver='+menuUpdate);   
             },250);
          }
      };
      
      
      
      if(pwaCookie == false){
          eraseCookie('Azures_pwa_rejected_install');
      }
      
      //Reload To Clear Button    
      $('body').on('click', '.page-update, .reloadme', function() {
          location.reload();
      });
      
   
      if(pwaNoCache == true){
          caches.delete('workbox-runtime').then(function() { 
          });
          localStorage.clear();
          sessionStorage.clear()
          caches.keys().then(cacheNames => {
            cacheNames.forEach(cacheName => {
              caches.delete(cacheName);
            });
          });
      }
      
   
  }); 

  